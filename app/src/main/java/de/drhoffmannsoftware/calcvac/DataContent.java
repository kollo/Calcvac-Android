package de.drhoffmannsoftware.calcvac;

/* DataContent.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.graphics.Color;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;

public class DataContent {
    private final static String TAG="DataContent";
    public final static int MAX_ANZDATA=100000;
    public int anzdata;

    /* Datenspuren haben gemeinsame X-Basis,
     * unterschiedliche namen, farbe*/

    ArrayList<DataTrace> p_traces;
    ArrayList<DataTrace> q_traces;
    DataTrace x_trace;
    public ArrayList<Label> labels;
    public ArrayList<Twiss> lattice;

    public double pmin,pmax;
    public double qmin,qmax;

    public DataContent(int maxanzdata) {
    	    x_trace=new DataTrace(maxanzdata);
    	    clear();
    }
    public DataContent() {
    	    x_trace=new DataTrace();
    	    clear();
    }
    public void clear() {
    	    labels=new ArrayList<Label>();
    	    lattice=new ArrayList<Twiss>();
    	    p_traces = new ArrayList();
    	    q_traces = new ArrayList();
    	    anzdata=0;
    	    x_trace.clear();
    }
	void calculate_total_pressure() {
		int i,j;
		int anzdata;
		DataTrace tot=new DataTrace();
		tot.name="Total Pressure";
		tot.color=Color.RED;
		if(p_traces.size()>0){
			anzdata=p_traces.get(0).anzdata;
			tot.anzdata=anzdata;
			for(i=0;i<anzdata;i++) {
				tot.y[i]=0;
				for(j=0;j<p_traces.size();j++) tot.y[i]+=p_traces.get(j).y[i];
			}
			if(p_traces.get(0).links.size()>0) {
				Link link;
				for(i=0;i<p_traces.get(0).links.size();i++) {
					link = new Link();
					link.name1 = p_traces.get(0).links.get(i).name1;
					link.name2 = p_traces.get(0).links.get(i).name2;
					link.idx = p_traces.get(0).links.get(i).idx;
					link.pos1 = p_traces.get(0).links.get(i).pos1;
					link.pos2 = p_traces.get(0).links.get(i).pos2;
					link.pressure=0;
					link.flow=0;
					for(j=0;j<p_traces.size();j++) {
						link.pressure+=p_traces.get(j).links.get(i).pressure;
						link.flow+=p_traces.get(j).links.get(i).flow;
					}
					tot.links.add(link);
				}
			}
		}
		tot.calc_minmax();
		p_traces.add(tot);
	}
	void calculate_total_flow() {
		int i,j;
		int anzdata;
		DataTrace tot=new DataTrace();
		tot.name="Total Flow";
		tot.color=Color.GREEN;
		if(q_traces.size()>0) {
			anzdata=q_traces.get(0).anzdata;
			tot.anzdata=anzdata;
			for(i=0;i<anzdata;i++) {
				tot.y[i]=0;
				for(j=0;j<q_traces.size();j++) tot.y[i]+=q_traces.get(j).y[i];
			}

		if(q_traces.get(0).links.size()>0) {
			Link link;
			for(i=0;i<q_traces.get(0).links.size();i++) {
				link = new Link();
				link.name1 = q_traces.get(0).links.get(i).name1;
				link.name2 = q_traces.get(0).links.get(i).name2;
				link.idx = q_traces.get(0).links.get(i).idx;
				link.pos1 = q_traces.get(0).links.get(i).pos1;
				link.pos2 = q_traces.get(0).links.get(i).pos2;
				link.pressure=0;
				link.flow=0;
				for(j=0;j<q_traces.size();j++) {
					link.pressure+=q_traces.get(j).links.get(i).pressure;
					link.flow+=q_traces.get(j).links.get(i).flow;
				}
				tot.links.add(link);
			}
		}
		}
		tot.calc_minmax();
		q_traces.add(tot);
	}

	void arrange_colors() {
	    Log.d(TAG,"Arrange colors...");
	    if(p_traces.size()>0) {
	        for(int i=0;i<p_traces.size();i++) {
	            p_traces.get(i).color=Color.argb(130,255,(150*i)/p_traces.size(),0);
            }
            p_traces.get(p_traces.size()-1).color=Color.argb(255,255,0,0);
        }
        if(q_traces.size()>0) {
            for(int i=0;i<q_traces.size();i++) {
                q_traces.get(i).color=Color.argb(130,0,255,(150*i)/p_traces.size());
            }
            q_traces.get(q_traces.size()-1).color=Color.argb(255,0,255,0);
        }
    }

	void calc_qminmax() {
		qmax=-9999;
		qmin=9999;
		double val;
		int i=q_traces.size();
		while(--i>=0) {
			qmax=Math.max(qmax,q_traces.get(i).ymax);
			qmin=Math.min(qmin,q_traces.get(i).ymin);
			if(q_traces.get(i).links.size()>0) {
				for(int j=0;j<q_traces.get(i).links.size();j++) {
					val=q_traces.get(i).links.get(j).flow;
					if(val>qmax) qmax=val;
					if(val<qmin) qmin=val;
				}
			}
		}
	}
	void calc_pminmax() {
		pmax=-9999;
		pmin=9999;
		double val;
		int i=p_traces.size();
		while(--i>=0) {
            pmax = Math.max(pmax, p_traces.get(i).ymax);
            pmin = Math.min(pmin, p_traces.get(i).ymin);

            if (p_traces.get(i).links.size() > 0) {
                for (int j = 0; j < p_traces.get(i).links.size(); j++) {
                    val = p_traces.get(i).links.get(j).pressure;
                    if (val > pmax) pmax = val;
                    if (val < pmin) pmin = val;
                }
            }
        }
	}


	public int loadtwissfile(String filename) {
  	    lattice = new ArrayList<Twiss>();
  	    Twiss twiss;
  	    Link link;
      Log.d(TAG,"read twiss file: "+filename);
      InputStream in = null;
      try {
		File file= new File(filename);
		in = new FileInputStream(file);

		if(in!=null)   {
			// prepare the file for reading
			InputStreamReader inputreader = new InputStreamReader(in);
			BufferedReader reader = new BufferedReader(inputreader);
			String line;
			try {line = reader.readLine();} catch (IOException e) {reader.close();return(0);}
			while (line!=null) {
				// do something with the settings from the file
				line=line.trim();
				line=line.replaceAll("\\s+"," ");
				if(!line.startsWith("#")) {
					twiss= new Twiss();
					String[] sep=line.split(" ");
					try {
						twiss.name=sep[0];
						if(sep.length>1) twiss.pos=Double.parseDouble(sep[1]);
						if(sep.length>2) twiss.len=Double.parseDouble(sep[2]);
						if(sep.length>3) twiss.pressure=Double.parseDouble(sep[3]);
						if(sep.length>4) twiss.flow=Double.parseDouble(sep[4]);
						if(sep.length>5) twiss.conductance=Double.parseDouble(sep[5]);
						if(sep.length>6) twiss.orate=Double.parseDouble(sep[6]);
						if(sep.length>7) twiss.pspeed=Double.parseDouble(sep[7]);
						if(sep.length>8) twiss.pavg=Double.parseDouble(sep[8]);
						if(sep.length>9) twiss.area=Double.parseDouble(sep[9]);

					} catch (NumberFormatException e) {Log.e(TAG,"could not parse line. <"+line+">"); }
					lattice.add(twiss);
					if(twiss.len==0 && twiss.area==0) {
						Label e=new Label();
						e.name=twiss.name;
						labels.add(e);
					}
/*
				} else if(line.startsWith("#*")) {
					link= new Link();
					String[] sep=line.split(" ");
					try {
						if(sep.length>1) link.idx=Integer.parseInt(sep[1]);
						if(sep.length>2) link.name1=sep[2];
						if(sep.length>3) link.pos1=Double.parseDouble(sep[3]);
						if(sep.length>4) link.name2=sep[4];
						if(sep.length>5) link.pos2=Double.parseDouble(sep[5]);
						if(sep.length>6) link.flow=Double.parseDouble(sep[6]);
						if(sep.length>7) link.pressure=Double.parseDouble(sep[7]);

					} catch (NumberFormatException e) {Log.e(TAG,"could not parse line. <"+line+">"); }
					links.add(link);
					*/
				}
				try {line = reader.readLine();} catch (IOException e) {reader.close();return(0);}
			}
			reader.close();
		}
		// close the file again
		try {in.close();} catch (IOException e) {return(0);}
	  }	catch (FileNotFoundException e) {
			Log.e(TAG,"ERROR: filenotfound ");
			return(0);
		} catch (IOException e) {
			Log.e(TAG,"ERROR: IO error ");
			return(0);
		} finally {
			if (in != null) {
				try {in.close();} catch (IOException e) {return(0);}
			}
		}
        return(lattice.size());
	}

	public int loaddatafile(String filename) {
		String nam=filename;
		Log.d(TAG,"read file: "+filename);
		InputStream in = null;
		String sep[]=nam.split("/");
		if(sep.length>0) nam=sep[sep.length-1];
		sep=nam.split("\\.");
		DataTrace trace_p=new DataTrace();
		DataTrace trace_q=new DataTrace();

		if(sep.length>=3) {
			int mol;
		try {mol=Integer.parseInt(sep[sep.length-3]);} catch(NumberFormatException nfe) {mol=0;}
			trace_p.name=Gas.moles[mol];
			trace_q.name=Gas.moles[mol];
		}
		else {
			trace_p.name=nam;
			trace_q.name=nam;
		}
		trace_p.color=Color.RED;
		trace_q.color=Color.GREEN;
		x_trace.clear();

		try {
			File file= new File(filename);
			in = new FileInputStream(file);
			if(in!=null) {
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				BufferedReader reader = new BufferedReader(inputreader);
				String line;
				try {line = reader.readLine();} catch (IOException e) {reader.close();return(0);}
				while (line!=null) {
					line=line.trim();
					line=line.replaceAll("\\s+"," ");
					// do something with the settings from the file
					if(!line.startsWith("#") && !line.startsWith("%")) {
						double xx,y=0,y2=0,y3=0;
						sep=line.split(" ");
						try {
							xx=Double.parseDouble(sep[0]);
							try {
								if(sep.length>1) {
									y=Double.parseDouble(sep[1]);
								}
							} catch (NumberFormatException e) {y=0;}
							try {
								if(sep.length>2) {
									y2=Double.parseDouble(sep[2])*1e6;
								}
							} catch (NumberFormatException e) {y2=0;}
							trace_p.add(y);
							trace_q.add(y2);
							x_trace.add(xx);
						} catch (NumberFormatException e) {Log.e(TAG,"could not parse line. <"+line+">");}
					} else if(line.startsWith("#*")) {
						Link link= new Link();
						sep=line.split(" ");
						try {
							if(sep.length>1) link.idx=Integer.parseInt(sep[1]);
							if(sep.length>2) link.name1=sep[2];
							if(sep.length>3) link.pos1=Double.parseDouble(sep[3]);
							if(sep.length>4) link.name2=sep[4];
							if(sep.length>5) link.pos2=Double.parseDouble(sep[5]);
							if(sep.length>6) link.flow=Double.parseDouble(sep[6]);
							if(sep.length>7) link.pressure=Double.parseDouble(sep[7]);

						} catch (NumberFormatException e) {Log.e(TAG,"could not parse line. <"+line+">"); }
						trace_q.links.add(link);
						trace_p.links.add(link);
					}
					try {line = reader.readLine();} catch (IOException e) {reader.close();return(0);}
				}
				reader.close();
			}
			p_traces.add(trace_p);
			q_traces.add(trace_q);
			calc_pminmax();
			calc_qminmax();
			// close the file again
			try {in.close();} catch (IOException e) {return(0);}
		} catch (FileNotFoundException e) {
			Log.e(TAG,"ERROR: filenotfound ");
			return(0);
		} catch (IOException e) {
			Log.e(TAG,"ERROR: IO error ");
			return(0);
		} finally {
			if (in != null) {
				try {in.close();} catch (IOException e) {return(0);}
			}
		}
		return(x_trace.anzdata);
	}

    public String get_statistics() {
	String a=""+lattice.size()+" Components, ";
	a=a+anzdata+" data points P=["+pmin+":"+pmax+"] Q=["+qmin+":"+qmax+"], ";
	a=a+labels.size()+" labels, ";
	a=a+p_traces.size()+" pressure traces, ";
        a=a+q_traces.size()+" flow traces,  ";
	if(q_traces.size()>0) a=a+q_traces.get(0).links.size()+" links. ";
        return a;
    }
}
