package de.drhoffmannsoftware.calcvac;

/* VacFile.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;

public class VacFile {
	private final static String TAG="VacFile";
  	String mfilename;
  	public String mtitle;
  	public String museline;
  	public ArrayList<Section> sections;
	public ArrayList<String> vaclines;
	public ArrayList<String> puresections;
	public boolean isinffile=false;
	public ArrayList<Spectrum> materials;
	public ArrayList<Spectrum> pumps;
	public ArrayList<Link> links;
	public static ArrayList<Integer> mols_of_interest;

	VacFile(String filename) {
		if(!(filename.toLowerCase().endsWith(".vac") ||filename.toLowerCase().endsWith(".inf") )) filename=filename+".vac";
		mfilename=filename;
		create_dummy_file(mfilename);
		mols_of_interest=new ArrayList<Integer>();
		reload();
	}

	public void mols_of_interest_addfromspec(Spectrum a) {
		int i=a.spec.size();
		while(--i>=0) {
			mols_of_interest_addnew(a.spec.get(i).mol);
		}
	}

	public void mols_of_interest_addnew(int a) {
		if(!mols_of_interest.contains(a)) mols_of_interest.add(a);
	}


	public String info() {
		String t="Name: "+mfilename+"\n";
		t=t+"Title: "+mtitle+"\n";
		t=t+"Useline: "+museline+"\n";
		t=t+sections.size()+" Sections."+"\n";
		t=t+vaclines.size()+" Vaclines."+"\n";
		t=t+puresections.size()+" pure sections."+"\n";
		t=t+materials.size()+" materials."+"\n";
		t=t+pumps.size()+" pumps."+"\n";
		t=t+links.size()+" links."+"\n";
		return t;
	}

	public void reload() {
        mtitle=get_title();
        museline=get_useline();
        sections=new ArrayList<Section>();
        vaclines=new ArrayList<String>();
        puresections=new ArrayList<String>();
        materials=new ArrayList<Spectrum>();
		pumps=new ArrayList<Spectrum>();
		links=new ArrayList<Link>();
		mols_of_interest.clear();
		mols_of_interest.add(0); /* default */
        isinffile=mfilename.toLowerCase().endsWith(".inf");
        get_sections();
        get_vaclines();
        get_puresections();
        get_materials();
        get_pumps();
		get_links();
    }

	public boolean isline(String name) {
		for(int i=0;i<sections.size();i++) {
			if(sections.get(i).typ==1 && sections.get(i).name.contentEquals(name)) return(true);
		}
		return(false);
	}


	public int find_section(String name) {
		for(int i=0;i<sections.size();i++) {
			if(sections.get(i).typ!=1 && sections.get(i).name.contentEquals(name)) return(i);
		}
		return -1;
	}
	public int find_material(String name) {
		for(int i=0;i<materials.size();i++) {
			if(materials.get(i).name.contentEquals(name)) return(i);
		}
		return -1;
	}
	public int find_pump(String name) {
		for(int i=0;i<pumps.size();i++) {
			if(pumps.get(i).name.contentEquals(name)) return(i);
		}
		return -1;
	}
	public int find_subline(String name) {
		for(int i=0;i<sections.size();i++) {
			if(sections.get(i).typ==1 && sections.get(i).name.contentEquals(name)) return(i);
		}
		return -1;
	}

	public int get_vaclines() {
        vaclines.clear();
		int i;
		int cnt=0;
		for(i=0;i<sections.size();i++) {
			if(sections.get(i).typ==1) vaclines.add(sections.get(i).name);
		}
		return(cnt);
	}
	public int get_puresections() {
		puresections.clear();
		int i;
		int cnt=0;
		for(i=0;i<sections.size();i++) {
			if(sections.get(i).typ!=1 && sections.get(i).typ<4) puresections.add(sections.get(i).name);
		}
		return(cnt);
	}

	public void write_links() {
		/* This should remove all links*/
		filter_file(3,"LINK", "",false);

		if(links.size()>0) {
			String allinks="";
			int i;
			for(i=0;i<links.size();i++) {
				allinks=allinks+links.get(i).format(isinffile);
				if(i<links.size()-1) allinks=allinks+"\n";
			}
			filter_file(3, "LINK", allinks, true);
		}
	}

	public int get_links() {
		links.clear();
		int sectiontyp=0;
		InputStream in = null;
		try {
			File dirdata=new File(MainActivity.Basestoragedirectory+"/"+"calcvac/");
			File file= new File(dirdata,mfilename);
			in = new FileInputStream(file);
			if(in!=null) {
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				BufferedReader reader = new BufferedReader(inputreader);
				String line;
				try {
					line = reader.readLine();
				} catch (IOException e) {
					reader.close();
					return(-1);
				}
				while (line!=null) {
					// do something with the settings from the file
					line=line.trim();
					line=line.replaceAll("\\s+"," ");

					if(line.startsWith("[")) {
						if(line.equalsIgnoreCase("[general]")) sectiontyp=0;
						else if(line.equalsIgnoreCase("[pumping]")) sectiontyp=1;
						else if(line.equalsIgnoreCase("[outgasing]")) sectiontyp=2;
						else if(line.equalsIgnoreCase("[lattice]")) sectiontyp=3;
						else if(line.equalsIgnoreCase("[events]")) sectiontyp=4;
						else if(line.equalsIgnoreCase("[plot]")) sectiontyp=5;
						else if(line.equalsIgnoreCase("[measurements]")) sectiontyp=6;
						else if(line.equalsIgnoreCase("[ZEUS]")) ;
						else if(line.equalsIgnoreCase("[H1]")) ;
						else  sectiontyp=9;
					} else if(line.startsWith("#")) ;
					else if(line.startsWith("!")) ;
					else if((sectiontyp==3 ||isinffile) && line.toUpperCase().startsWith("LINK")) {
						Log.d(TAG,"matches: "+line);
						String[] sep=line.split(" ",2);
						if(sep[0].equalsIgnoreCase("LINK")) {
							line=sep[1].trim();
							sep=line.split(",");
							Link l=new Link();
							l.name1=sep[0].trim();
							l.name2=sep[1].trim();
							links.add(l);
						} else {
							sep=line.split(",");
							if(sep[0].trim().equalsIgnoreCase("LINK")) {
								Link l=new Link();
								l.name1=sep[1].trim();
								l.name2=sep[2].trim();
								links.add(l);
							}
						}
					} else Log.d(TAG,"LINE: "+line);
					try {line = reader.readLine();} catch (IOException e) {reader.close();return(-1);}
				}
				reader.close();
			}
		} catch (FileNotFoundException e) { Log.e(TAG,"ERROR: filenotfound ");return(-1);
		} catch (IOException e) { Log.e(TAG,"ERROR: IO error ");return(-1);
		} finally { if (in != null) { try {in.close();} catch (IOException e) {return(-1);} } }
       return(links.size());
	}


    public int get_materials() {
		materials.clear();

		int sectiontyp=0;
		InputStream in = null;
		try {
			File dirdata=new File(MainActivity.Basestoragedirectory+"/"+"calcvac/");
			File file= new File(dirdata,mfilename);
			in = new FileInputStream(file);
			if(in!=null) {
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				BufferedReader reader = new BufferedReader(inputreader);
				String line;
				try {
					line = reader.readLine();
				} catch (IOException e) {
					reader.close();
					return(-1);
				}
				while (line!=null) {
					// do something with the settings from the file
					line=line.trim();
					line=line.replaceAll("\\s+"," ");

					if(line.startsWith("[")) {
						if(line.equalsIgnoreCase("[general]")) sectiontyp=0;
						else if(line.equalsIgnoreCase("[pumping]")) sectiontyp=1;
						else if(line.equalsIgnoreCase("[outgasing]")) sectiontyp=2;
						else if(line.equalsIgnoreCase("[lattice]")) sectiontyp=3;
						else if(line.equalsIgnoreCase("[events]")) sectiontyp=4;
						else if(line.equalsIgnoreCase("[plot]")) sectiontyp=5;
						else if(line.equalsIgnoreCase("[measurements]")) sectiontyp=6;
						else if(line.equalsIgnoreCase("[ZEUS]")) ;
						else if(line.equalsIgnoreCase("[H1]")) ;
						else  sectiontyp=9;
					} else if(line.startsWith("#")) ;
					else if(line.startsWith("!")) ;
					else if((sectiontyp==2) && line.matches("\\S+:(?!=).*")) {
						Log.d(TAG,"matches: "+line);
						Spectrum spec=new Spectrum();
						spec.parsefromline(line);
						mols_of_interest_addfromspec(spec);
						materials.add(spec);
					} else Log.d(TAG,"LINE: "+line);
					try {line = reader.readLine();} catch (IOException e) {reader.close();return(-1);}
				}
				reader.close();
			}
		} catch (FileNotFoundException e) { Log.e(TAG,"ERROR: filenotfound ");return(-1);
		} catch (IOException e) { Log.e(TAG,"ERROR: IO error ");return(-1);
		} finally { if (in != null) { try {in.close();} catch (IOException e) {return(-1);} } }

		return(materials.size());
	}
	public int get_pumps() {
		pumps.clear();

		int sectiontyp=0;
		InputStream in = null;
		try {
			File dirdata=new File(MainActivity.Basestoragedirectory+"/"+"calcvac/");
			File file= new File(dirdata,mfilename);
			in = new FileInputStream(file);
			if(in!=null) {
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				BufferedReader reader = new BufferedReader(inputreader);
				String line;
				try {
					line = reader.readLine();
				} catch (IOException e) {
					reader.close();
					return(-1);
				}
				while (line!=null) {
					// do something with the settings from the file
					line=line.trim();
					line=line.replaceAll("\\s+"," ");

					if(line.startsWith("[")) {
						if(line.equalsIgnoreCase("[general]")) sectiontyp=0;
						else if(line.equalsIgnoreCase("[pumping]")) sectiontyp=1;
						else if(line.equalsIgnoreCase("[outgasing]")) sectiontyp=2;
						else if(line.equalsIgnoreCase("[lattice]")) sectiontyp=3;
						else if(line.equalsIgnoreCase("[events]")) sectiontyp=4;
						else if(line.equalsIgnoreCase("[plot]")) sectiontyp=5;
						else if(line.equalsIgnoreCase("[measurements]")) sectiontyp=6;
						else if(line.equalsIgnoreCase("[ZEUS]")) ;
						else if(line.equalsIgnoreCase("[H1]")) ;
						else  sectiontyp=9;
					} else if(line.startsWith("#")) ;
					else if(line.startsWith("!")) ;
					else if((sectiontyp==1) && line.matches("\\S+:(?!=).*")) {
						Log.d(TAG,"matches: "+line);
						Spectrum spec=new Spectrum();
						spec.parsefromline(line);
						mols_of_interest_addfromspec(spec);
						pumps.add(spec);
					} else Log.d(TAG,"LINE: "+line);
					try {line = reader.readLine();} catch (IOException e) {reader.close();return(-1);}
				}
				reader.close();
			}
		} catch (FileNotFoundException e) { Log.e(TAG,"ERROR: filenotfound ");return(-1);
		} catch (IOException e) { Log.e(TAG,"ERROR: IO error ");return(-1);
		} finally { if (in != null) { try {in.close();} catch (IOException e) {return(-1);} } }
		return(pumps.size());
	}



	public String rename_file(String newname) {
		if(!(newname.toLowerCase().endsWith(".vac") ||newname.toLowerCase().endsWith(".inf") )) newname=newname+".vac";
        File dirdata = new File(MainActivity.Basestoragedirectory + "/" + "calcvac/");
        File file = new File(dirdata, mfilename);
        File nfile=new File(dirdata, newname);
        file.renameTo(nfile);

		mfilename=newname;
		return newname;
	}

	public void set_title(String title) {
		filter_file(0,"TITLE=","TITLE=\""+title+"\"",true);
		mtitle=title;
	}
	public void set_useline(String useline) {
		if(!isinffile) filter_file(3,"USE ","USE "+useline,true);
		else filter_file(0,"USE,","USE, "+useline,true);
		museline=useline;
	}

	public void set_section(Section sec) {
		String t=sec.format();
		filter_file(3,sec.name+":",t,true);
	}
	public void set_material(Spectrum mat) {
		String t=mat.format();
		filter_file(2,mat.name+":",t,true);
	}
	public void set_pump(Spectrum p) {
		String t=p.format();
		filter_file(1,p.name+":",t,true);
	}


	public void delete(String elem) {
		Log.d(TAG,"Delete element "+elem);
		filter_file(3,elem+": ","",false);
	}
	public void delete_material(String elem) {
		Log.d(TAG,"Delete material "+elem);
		filter_file(2,elem+": ","",false);
	}
	public void delete_pump(String elem) {
		Log.d(TAG,"Delete pump "+elem);
		filter_file(1,elem+": ","",false);
	}


	void filter_file(int sectyp,String pattern, String replace,boolean addifnotfound) {
		int sectiontyp=0;
		boolean have_replaced=false;
		InputStream in = null;
		OutputStream out = null;
		try {
			File dirdata = new File(MainActivity.Basestoragedirectory + "/" + "calcvac/");
			File file = new File(dirdata, mfilename);
			File bakfile=new File(dirdata, mfilename+".bak");
			file.renameTo(bakfile);

			in = new FileInputStream(bakfile);
			out = new FileOutputStream(file);
			if (in != null && out!=null) {
				Log.d(TAG,"filter file: "+bakfile+" -->"+file);
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				BufferedReader reader = new BufferedReader(inputreader);
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);

				String line;
				try {
					line = reader.readLine();
				} catch (IOException e) {
					reader.close();
					return;
				}
				while (line!=null) {
					// do something with the settings from the file
					line=line.trim();
					line=line.replaceAll("\\s+"," ");

					if(line.startsWith("[")) {
						if(line.equalsIgnoreCase("[general]")) sectiontyp=0;
						else if(line.equalsIgnoreCase("[pumping]")) sectiontyp=1;
						else if(line.equalsIgnoreCase("[outgasing]")) sectiontyp=2;
						else if(line.equalsIgnoreCase("[lattice]")) sectiontyp=3;
						else if(line.equalsIgnoreCase("[events]")) sectiontyp=4;
						else if(line.equalsIgnoreCase("[plot]")) sectiontyp=5;
						else if(line.equalsIgnoreCase("[measurements]")) sectiontyp=6;
						else if(line.equalsIgnoreCase("[ZEUS]")) ;
						else if(line.equalsIgnoreCase("[H1]")) ;
						else  sectiontyp=9;
						Log.d(TAG,"SECTION: "+sectiontyp+"("+line+")");
						if(sectiontyp==sectyp+1 && !have_replaced && addifnotfound) {
							/* Here we have to add the new flag:
							   as the last line in the previous section.*/
							outputStreamWriter.write(replace+"\n");
						}
					} else if(line.startsWith("#")) ;
					else if(line.startsWith("!")) ;
					else if((sectiontyp==sectyp||isinffile) && line.startsWith(pattern)) {
						line=replace;
						have_replaced=true;
						Log.d(TAG,"***BINGO****");
					} else Log.d(TAG,"LINE: "+line);
					outputStreamWriter.write(line+"\n");

					try {line = reader.readLine();} catch (IOException e) {reader.close();return;}
				}
				/* If the option has not yet been inserted, append it to the file:*/
				if(!have_replaced && addifnotfound) {
					outputStreamWriter.write(replace+"\n");
				}
				reader.close();
				outputStreamWriter.close();
			}
		} catch (Exception e) {
			Log.e(TAG, e.toString()+e.getMessage());
		} finally {
			if (in != null) {
				try {in.close();} catch (IOException e) {}
			}
			if (out != null) {
				try {out.close();} catch (IOException e) {}
			}
		}
	}





    /* Liest wert des Parameters aus mit Komma separierter Liste aus VAR=VAL Zuordnungen.*/

	private String get_parameter(String line, String var) {
		String wert="";
		String[] sep2;
		String[] sep = line.split(",");
		int i=sep.length;
		while(i>0) {
			i--;
			sep2=sep[i].trim().split("=");
			if(sep2[0].equalsIgnoreCase(var)) wert = sep2[1];
		}
        return(wert);
	}




	public int vac2inf(int mol) {
		if(isinffile) return(-1);
		int sectiontyp=0;
		InputStream in = null;
		OutputStream out = null;
		try {
			File dirdata = new File(MainActivity.Basestoragedirectory + "/" + "calcvac/");
			File srcfile = new File(dirdata, mfilename);
			File dstfile=new File(dirdata, mfilename+"."+mol+".inf");


			in = new FileInputStream(srcfile);
			out = new FileOutputStream(dstfile);
			if (in != null && out!=null) {
				Log.d(TAG,"convert file: "+srcfile+" -->"+dstfile);
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				BufferedReader reader = new BufferedReader(inputreader);
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);
				Calendar cal = Calendar.getInstance();
				String line;
				try {
					line = reader.readLine();
				} catch (IOException e) {
					reader.close();
					return(-1);
				}
				outputStreamWriter.write("! Generated by calcvac (c) Markus Hoffmann "+cal.getTime().toLocaleString()+"\n");
				outputStreamWriter.write("! Molecul Mass:  "+mol+"   Source:"+mfilename+"\n");
				while (line!=null) {
					// do something with the settings from the file
					line=line.trim();
					line=line.replaceAll("\\s+"," ");

					if(line.startsWith("[")) {
						if(line.equalsIgnoreCase("[general]")) sectiontyp=0;
						else if(line.equalsIgnoreCase("[pumping]")) sectiontyp=1;
						else if(line.equalsIgnoreCase("[outgasing]")) sectiontyp=2;
						else if(line.equalsIgnoreCase("[lattice]")) sectiontyp=3;
						else if(line.equalsIgnoreCase("[events]")) sectiontyp=4;
						else if(line.equalsIgnoreCase("[plot]")) sectiontyp=5;
						else if(line.equalsIgnoreCase("[measurements]")) sectiontyp=6;
						else if(line.equalsIgnoreCase("[ZEUS]")) ;
						else if(line.equalsIgnoreCase("[H1]")) ;
						else  sectiontyp=9;
						Log.d(TAG,"SECTION: "+sectiontyp+"("+line+")");
					} else if(line.startsWith("#")) ;
					else if(line.startsWith("!")) ;
					else if(sectiontyp==0||sectiontyp==3) {
						if (line.startsWith("TITLE=")) line = line.replace("TITLE=", "TITLE, ");
						else if (line.startsWith("USE ")) line = line.replace("USE ", "USE, ");
						else if (line.startsWith("LINK ")) line = line.replace("LINK ", "LINK, ");
						else if(line.contains(":")) {
								Section sec=new Section();
								sec.parsefromline(line);
								Section sec2=sec.get_simple(mol,materials,pumps);
								line=sec2.format();
								Log.d(TAG,"Line="+line);
						}
						outputStreamWriter.write(line+"\n");
					}
					try {line = reader.readLine();} catch (IOException e) {reader.close();return(-1);}
				}
				reader.close();
				outputStreamWriter.close();
			}
		} catch (Exception e) {
			Log.e(TAG, e.toString()+e.getMessage());
		} finally {
			if (in != null) {
				try {in.close();} catch (IOException e) {return(-1);}
			}
			if (out != null) {
				try {out.close();} catch (IOException e) {return(-1);}
			}
		}
		return(0);
	}


    public String get_useline() {
    	String useline="";
    	String line=get_line(3,"USE,");
    	if(!line.isEmpty()) {
    	    	String[] sep = line.split(",");
    	    	if (sep[0].equalsIgnoreCase("USE")) useline = sep[1].trim();
    	    	museline = useline;
    	} else {
    	    	line=get_line(3,"USE");
    	    	String[] sep = line.split(" ");
    	    	if (sep[0].equalsIgnoreCase("USE")) useline = sep[1].trim();
    	    	museline = useline;
    	}
    	return useline;
    }
    public String get_title() {
		String title="";
		String line=get_line(0,"TITLE=");
		if(!line.isEmpty()) {
			String[] sep = line.split("=");
			if (sep[0].equalsIgnoreCase("TITLE")) title = sep[1];
			if (title.length() >= 2 && title.charAt(0) == '"' && title.charAt(title.length() - 1) == '"') {
				title = title.substring(1, title.length() - 1);
			}
		} else {
			line=get_line(0,"TITLE,");
			String[] sep = line.split(",");
			if (sep[0].equalsIgnoreCase("TITLE")) title = sep[1].trim();

		}
		mtitle = title;
		return title;
	}

	void get_sections() {
		sections.clear();
		int sectiontyp=0;
		InputStream in = null;
		try {
			File dirdata=new File(MainActivity.Basestoragedirectory+"/"+"calcvac/");
			File file= new File(dirdata,mfilename);
			in = new FileInputStream(file);
			if(in!=null) {
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				BufferedReader reader = new BufferedReader(inputreader);
				String line;
				try {
					line = reader.readLine();
				} catch (IOException e) {
					reader.close();
					return;
				}
				while (line!=null) {
				    if(line.endsWith("&")) line = line.substring(0, line.length() - 1)+reader.readLine();
					// do something with the settings from the file
					line=line.trim();
					line=line.replaceAll("\\s+"," ");

					if(line.startsWith("[")) {
						if(line.equalsIgnoreCase("[general]")) sectiontyp=0;
						else if(line.equalsIgnoreCase("[pumping]")) sectiontyp=1;
						else if(line.equalsIgnoreCase("[outgasing]")) sectiontyp=2;
						else if(line.equalsIgnoreCase("[lattice]")) sectiontyp=3;
						else if(line.equalsIgnoreCase("[events]")) sectiontyp=4;
						else if(line.equalsIgnoreCase("[plot]")) sectiontyp=5;
						else if(line.equalsIgnoreCase("[measurements]")) sectiontyp=6;
						else if(line.equalsIgnoreCase("[ZEUS]")) ;
						else if(line.equalsIgnoreCase("[H1]")) ;
						else  sectiontyp=9;
					} else if(line.startsWith("#")) ;
					else if(line.startsWith("!")) ;
					else if((sectiontyp==3 ||isinffile) && line.matches("\\S+:(?!=).*")) {
						Section sec=new Section();
						Log.d(TAG,"matches: "+line);
						sec.parsefromline(line);
						sections.add(sec);
					} else Log.d(TAG,"LINE: "+line);
					try {line = reader.readLine();} catch (IOException e) {reader.close();return;}
				}
				reader.close();
			}
		} catch (FileNotFoundException e) { Log.e(TAG,"ERROR: filenotfound ");return;
		} catch (IOException e) { Log.e(TAG,"ERROR: IO error ");return;
		} finally { if (in != null) { try {in.close();} catch (IOException e) {return;} } }

	}



	String get_line(int sectyp,String pattern) {
		String result="";
		int sectiontyp=0;
		InputStream in = null;
		try {
			File dirdata=new File(MainActivity.Basestoragedirectory+"/"+"calcvac/");
			File file= new File(dirdata,mfilename);
			in = new FileInputStream(file);
			if(in!=null) {
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				BufferedReader reader = new BufferedReader(inputreader);
				String line;
				try {
					line = reader.readLine();
				} catch (IOException e) {
					reader.close();
					return ("");
				}
				while (line!=null) {
                    if(line.endsWith("&")) line = line.substring(0, line.length() - 1)+reader.readLine();

                    // do something with the settings from the file
					line=line.trim();
					line=line.replaceAll("\\s+"," ");

					if(line.startsWith("[")) {
						if(line.equalsIgnoreCase("[general]")) sectiontyp=0;
						else if(line.equalsIgnoreCase("[pumping]")) sectiontyp=1;
						else if(line.equalsIgnoreCase("[outgasing]")) sectiontyp=2;
						else if(line.equalsIgnoreCase("[lattice]")) sectiontyp=3;
						else if(line.equalsIgnoreCase("[events]")) sectiontyp=4;
						else if(line.equalsIgnoreCase("[plot]")) sectiontyp=5;
						else if(line.equalsIgnoreCase("[measurements]")) sectiontyp=6;
						else if(line.equalsIgnoreCase("[ZEUS]")) ;
						else if(line.equalsIgnoreCase("[H1]")) ;
						else  sectiontyp=9;
						Log.d(TAG,"SECTION: "+sectiontyp+"("+line+")");
					} else if(line.startsWith("#")) ;
					else if(line.startsWith("!")) ;
					else if((sectiontyp==sectyp||isinffile) && line.toUpperCase().startsWith(pattern)) {
						result=line;
						break;
					} else Log.d(TAG,"LINE: "+line);
					try {line = reader.readLine();} catch (IOException e) {reader.close();return("");}
				}
				reader.close();
			}
		} catch (FileNotFoundException e) { Log.e(TAG,"ERROR: filenotfound ");return("");
		} catch (IOException e) { Log.e(TAG,"ERROR: IO error ");return("");
		} finally { if (in != null) { try {in.close();} catch (IOException e) {} } }
		return result;
	}

    public static void create_dummy_file(String filename) {
    	    File dirdata=new File(MainActivity.Basestoragedirectory+"/"+"calcvac/");
    	    File f=new File(dirdata,filename);
    	    if(!f.exists()) {
    		    try {
    			    f.createNewFile();
    			    FileOutputStream fos=null;
    			    OutputStreamWriter osw=null;
    			    Calendar cal = Calendar.getInstance();
    			    fos=new FileOutputStream(f,true);
    			    osw=new OutputStreamWriter(fos);
    			    osw.write("# new Calvac File "+f.getAbsolutePath()+" created on "+cal.getTime().toLocaleString()+"\n");
    			    osw.write("[general]\n");
    			    osw.write("# set a verbose title\n");
    			    osw.write("TITLE=\"This is a new vacuum line\"\n");
    			    osw.write("[pumping]\n");
    			    osw.write("tsp:  0(default)   1(2)   0(16)  1(28)\n");
    			    osw.write("neg:  0(default) 1.6(2)   0(16)  1(28)\n");
    			    osw.write("igp:  0(default)   2(2) 0.6(16)  1(28)\n");
    			    osw.write("[outgasing]\n");
    			    osw.write("# Unit: mbar l/s /cm^2\n");
    			    osw.write("steel: 0(default) 1.0e-12(2)	0(16)  5e-13(28)\n");
    			    osw.write("Cu:    0(default) 1.0e-12(2)	0(16)  1e-12(28)\n");
    			    osw.write("Al:    0(default) 2.0e-12(2) 1e-13(16) 20e-12(28)\n");
    			    osw.write("[lattice]\n");
    			    osw.write("Begin: START, S0=0, BOUNDARY(Q=0)\n");
    			    osw.write("TUBE: SECTION, L=1, q=5e-12, W=13, A=3100\n");
    			    osw.write("PUMP: SECTION, L=1, q=5e-12, W=45, A=1900, Ps=34\n");

    			    osw.write("End: END, BOUNDARY(Q=0)\n");
    			    osw.write("Vacuumline: LINE=(Begin,TUBE,PUMP,End)\n");
    			    osw.write("USE Vacuumline\n");
    			    osw.flush();
    			    osw.close();
    			    fos.close();

    			    Log.d(TAG,"new file created: "+f);
    		    } catch (IOException e1) {
    			    e1.printStackTrace();
    		    }
    	    }
    }
}
