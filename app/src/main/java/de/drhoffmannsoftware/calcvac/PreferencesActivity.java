package de.drhoffmannsoftware.calcvac;

/* PreferencesActivity.java (c) 2018-2021 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2021
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;

public class PreferencesActivity extends PreferenceActivity  implements OnSharedPreferenceChangeListener{
  private final static String HOMEPAGE="https://codeberg.org/kollo/Calcvac-Android";
  private final static String WIKI_PAGE="https://codeberg.org/kollo/Calcvac-Android/wikis/home";
  private final static String FORUM_PAGE="https://codeberg.org/kollo/Calcvac-Android/issues";
  private final static String ISSUES_PAGE="https://codeberg.org/kollo/Calcvac-Android/issues";
  private final static String EXAMPLES_PAGE="https://codeberg.org/kollo/Calcvac_examples";
  private final static String MANUAL_PAGE="https://codeberg.org/kollo/Calcvac-Android/raw/master/doc/User-Manual/Calcvac-manual-2.01.pdf";
  private final static String LICENSE_PAGE="https://codeberg.org/kollo/Calcvac-Android/raw/master/LICENSE";
  private final static String SOURCE_PAGE="https://codeberg.org/kollo/Calcvac-Android";
  private final static String HERA_03_23="http://www-library.desy.de/ahluwali/herareports/2003/HERA-03-23.pdf";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    addPreferencesFromResource(R.xml.preferences);
         //   PreferenceManager.setDefaultValues(Preferences.this, R.xml.preferences, false);

    findPreference("about_version").setSummary(applicationVersion());
    findPreference("about_homepage").setSummary(HOMEPAGE);
    findPreference("about_guestbook").setSummary(ISSUES_PAGE);
    // findPreference("about_wiki").setSummary(WIKI_PAGE);
    findPreference("about_forum").setSummary(FORUM_PAGE);
    findPreference("about_example_programs").setSummary(EXAMPLES_PAGE);
    findPreference("about_manual").setSummary(MANUAL_PAGE);
    findPreference("about_herareport").setSummary(HERA_03_23);

    //    findPreference("about_license").setSummary(Constants.LICENSE_URL);
    //    findPreference("about_source").setSummary(Constants.SOURCE_URL);
    //    findPreference("about_market_app").setSummary(String.format("market://details?id=%s", getPackageName()));
    //    findPreference("about_market_publisher").setSummary("market://search?q=pub:\"Markus Hoffmann\"");
    	// Get a reference to the preferences
    //    mCheckBoxPreference = (CheckBoxPreference)getPreferenceScreen().findPreference(KEY_ADVANCED_CHECKBOX_PREFERENCE);
    //    mListPreference = (ListPreference)getPreferenceScreen().findPreference("select_fileformat");
    for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++) {
      initSummary(getPreferenceScreen().getPreference(i));
    }
  }
  @Override
  protected void onResume() {
    super.onResume();
    // Set up a listener whenever a key changes 	    
    getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
  }

  @Override
  protected void onPause() {
    super.onPause();
    // Unregister the listener whenever a key changes		  
    getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);     
 
  }
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) { 
    updatePrefSummary(findPreference(key));
    MainActivity.nplot=(int)(Math.max(32,Double.parseDouble(sharedPreferences.getString("prefs_numpoints", "3072"))));
    MainActivity.resolution=(Double.parseDouble(sharedPreferences.getString("prefs_resolution", "0.001")));
    if(MainActivity.resolution<=0) MainActivity.resolution=0.001;
  }

  private void initSummary(Preference p) {
    if(p instanceof PreferenceCategory) {
      PreferenceCategory pCat = (PreferenceCategory) p;
      for(int i=0;i<pCat.getPreferenceCount();i++) {
        initSummary(pCat.getPreference(i));
      }
    } else updatePrefSummary(p);
  }

  private void updatePrefSummary(Preference p){
      if (p instanceof ListPreference) {
  	  ListPreference listPref = (ListPreference) p; 
  	  p.setSummary(listPref.getEntry()); 
      }
      if (p instanceof EditTextPreference) {
  	  EditTextPreference editTextPref = (EditTextPreference) p; 
  	  p.setSummary(editTextPref.getText()); 
      }
  }

  private String applicationVersion() {
    try {return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;}
    catch (NameNotFoundException x)  { return "unknown"; }
  }

  @Override
  public boolean onPreferenceTreeClick(final PreferenceScreen preferenceScreen, final Preference preference)  {
    final String key = preference.getKey();
    if("about_license".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LICENSE_PAGE)));
      finish();
    } else if ("about_version".equals(key)) {
      startActivity(new Intent(this, InfoActivity.class));
    } else if ("about_example_programs".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(EXAMPLES_PAGE)));
      finish();
    } else if ("about_source".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SOURCE_PAGE)));
      finish();
    } else if ("about_homepage".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HOMEPAGE)));
      finish();
    } else if ("about_guestbook".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ISSUES_PAGE)));
      finish();
    } else if ("about_wiki".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(WIKI_PAGE)));
      finish();
    } else if ("about_forum".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FORUM_PAGE)));
      finish();
    } else if ("about_manual".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MANUAL_PAGE)));
      finish();
    } else if ("about_herareport".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HERA_03_23)));
      finish();
    } else if ("about_market_app".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", getPackageName()))));
      finish();
    } else if ("about_market_publisher".equals(key))  {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:\"Markus Hoffmann\"")));
      finish();
    }
    return false;
  }
}
