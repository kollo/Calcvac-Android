package de.drhoffmannsoftware.calcvac;

/* MolChooserActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MolChooserActivity extends Activity {
    LinearLayout checkboxes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.molecule);

        ActionBar actionBar = getActionBar();
        //  actionBar.setHomeButtonEnabled(true);
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.molecule_mass_chooser);
        }
        checkboxes=findViewById(R.id.checkBoxes);
    }

    @Override
    public void onStart() {
        super.onStart();
        ArrayList<Integer> moi=VacFile.mols_of_interest;

        checkboxes.removeAllViews();
        CheckBox cb;
        int i;
        for(i=0;i<Gas.moles.length;i++) {
            if(Gas.moles[i].length()>0) {
                cb = new CheckBox(this);
                cb.setText(Gas.moles[i]);
                if (i == 0) {
                    cb.setChecked(true);
                    cb.setEnabled(false);
                } else if(moi.contains(i)) {
                    cb.setChecked(true);
                }
                checkboxes.addView(cb);
            }
        }
    }
    @Override
    protected void onStop(){
        super.onStop();
        String t;
        String a[];
        int idx;
        int i=checkboxes.getChildCount();
        VacFile.mols_of_interest.clear();
        while(--i>=0) {
            t=((CheckBox)checkboxes.getChildAt(i)).getText().toString();
            a=t.split(" ",2);
            if(!a[0].isEmpty()) {
                idx = Integer.parseInt(a[0]);
                if(((CheckBox)checkboxes.getChildAt(i)).isChecked()) {
                    VacFile.mols_of_interest.add(idx);
                }
            }
        }
     }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.moleculemenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed
                // in the Action Bar.
                finish();
                return true;
            case R.id.mc_options_help:
                showDialog(9);
                return true;
        }
        return true;
    }
    @Override
    protected Dialog onCreateDialog(final int id) {
        Dialog dialog = null;
        if(id==9) {
            dialog = Tools.scrollableDialog(this, "Info", getResources().getString(R.string.moleculehelpdialog));
        }
        return dialog;
    }
}
