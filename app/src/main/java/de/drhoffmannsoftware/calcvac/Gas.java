package de.drhoffmannsoftware.calcvac;

/* Gas.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

/* This is a constant array of names of gas species by molecule mass */

class Gas {
    public static final String moles[]={"0 default","","2 H2 Hydrogen","","4 He Helium","","","","","",
            "","","","","","","16 CH4 Methane","17 NH3 Ammonia","18 H2O Water","",
            "20 Ne Neon","","","","","","","27 HCN Hydrogen Cyanide","28 N2 Nitrogene, CO Carbon Monoxide","",
            "30 NO Nitric Oxide","","32 O2 oxygene","","34 H2S Hydrogen Sulfide, PH3 Phosphine","","36 HCl Hydrogen Chloride","","","",
            "40 Ar argon","","","","44 CO2 carbon dioxyde","","46 NO2 Nitrogen Dioxide","","","",
            "","","","","","","","","","",
            "","","","","64 SO2 Sulfur Dioxide","","","67 ClO2 Chlorine Dioxide","","",
            "","71 Cl2 Chlorine","","","","","","","","",
            "","","","","84 Kr krypton","","","","",""
    };
}
