package de.drhoffmannsoftware.calcvac;

/* Spectrum.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.util.Log;
import java.util.ArrayList;

class Spectrum {
    private final static String TAG="Spectrum";

    class entry {
        int mol;
        double value;
    }

    String name;
    double s_default;
    ArrayList<entry> spec;

    Spectrum() {
        name="";
        s_default=0;
        spec=new ArrayList<entry>();
    }

    public Spectrum(String nam) {
        name=nam;
        s_default=0;
        spec=new ArrayList<entry>();

    }

    public double get_value(int m) {
        if(spec.size()>0) {
            for(int i=0;i<spec.size();i++) {
                if(spec.get(i).mol==m) return(spec.get(i).value);
            }
        }
        return(s_default);
    }
    public void add_spec(int m, double v) {
        entry e=new entry();
        e.mol=m;
        e.value=v;
        spec.add(e);
    }
    public String format() {
        String t=name+": "+s_default+"(default)";
        if(spec.size()>0) {
            for(int i=0;i<spec.size();i++) {
                t=t+" "+spec.get(i).value+"("+spec.get(i).mol+")";
            }
        }
        return t;
    }
    public int parsefromline(String lin) {
        String[] sep = lin.split(":", 2);
        name = sep[0].trim();
        lin = sep[1].trim();
        sep=lin.split(" ");
        if(sep.length>0) {
            int i;
            for(i=0;i<sep.length;i++) {
                sep[i]=sep[i].trim();
                if(sep[i].contains("(")) {
                    String[] sep2=sep[i].split("\\(");
                    if (sep2[1].length() >= 1 && sep2[1].charAt(sep2[1].length() - 1) == ')') {
                        sep2[1] = sep2[1].substring(0, sep2[1].length() - 1);
                    }

                    double v;
                    try {v = Double.parseDouble(sep2[0]);} catch(NumberFormatException nfe) {v=0;}
                    Log.d(TAG,"we have "+sep2[1]);
                    if(sep2[1].startsWith("default")) {
                        s_default=v;
                    } else {
                        int m = Integer.parseInt(sep2[1]);
                        add_spec(m,v);
                    }
                }
            }
        }
        return(spec.size());
    }
}
