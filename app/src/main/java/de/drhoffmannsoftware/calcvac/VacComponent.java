package de.drhoffmannsoftware.calcvac;

/* VacComponent.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

class VacComponent {
  public int typ;
  public int ftyp;
  public double len;
  public int m;    /* Molecule mass number */
  public String name;

  VacComponent() {
    name="noname";
    len=1;
    m=28;
    ftyp=0;
    typ=0;
  }
}
