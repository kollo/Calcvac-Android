package de.drhoffmannsoftware.calcvac;

/* Twiss.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


class Twiss {
    String name;
    double pos;
    double len;
    double pressure;
    double flow;
    double conductance;
    double orate;
    double pspeed;
    double pavg;
    double area;
}
