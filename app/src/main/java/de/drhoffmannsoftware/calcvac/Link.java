package de.drhoffmannsoftware.calcvac;

/* Link.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

class Link {
  int idx;
  String name1;
  String name2;
  double pos1;
  double pos2;
  double flow;
  double pressure;

  public String toString() {
    return(name1+" <--> "+name2);
  }
  public String format(boolean isinffile) {
    if(isinffile) return "LINK, "+name1+", "+name2;
    else return "LINK "+name1+", "+name2;
  }
}
