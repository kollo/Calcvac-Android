package de.drhoffmannsoftware.calcvac;

/* LineEditorActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;


public class LineEditorActivity  extends Activity {
    private final static String TAG="LINEEDIT";
    Section sec;
    EditText sec_name;
    Button button_done,button_cancel;
    ListView list;
    ArrayList<String> ht;
    ArrayAdapter<String> adapter;
    String mSelectedElement;
    int mselect;
    View resetview=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.linecomposer);

        ActionBar actionBar = getActionBar();
		//  actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.line_editor);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);



        sec_name=findViewById(R.id.component_name);
        button_done=findViewById(R.id.button_done);
        button_cancel=findViewById(R.id.button_cancel);
        button_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        button_done.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sec.name=sec_name.getText().toString();
                if(sec.typ==1) {
                    int i;
                    sec.line ="";
                    if(ht.size()>0) {
                        for (i = 0; i < ht.size(); i++) {
                            if (i > 0) sec.line = sec.line + ",";
                            sec.line = sec.line + ht.get(i);
                        }
                    }
                }
                if(!sec.line.isEmpty()) {
                    VacFile vac = ComponentEditorActivity.vacfile;
                    vac.set_section(sec);
                }
                finish();
            }
        });

        list=findViewById(R.id.list_1);
        list.setTextFilterEnabled(true);


        ht=new ArrayList();
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, ht);

		list.setAdapter(adapter);

		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view,
        	        int position, long id) {
        	    resetview=view;
        	    view.setBackgroundColor(Color.BLUE);
        	    mselect=(int)id;
        	    showDialog(0);

        	}
        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
        public boolean onItemLongClick(AdapterView<?> parent, View view,
                                int position, long id) {
            resetview=view;
            view.setBackgroundColor(Color.BLUE);
            mselect=(int)id;
            showDialog(0);
            return true;
            }
        });
    }
    @Override
    public void onStart() {
	super.onStart();
        VacFile vac=ComponentEditorActivity.vacfile;
        int idx=vac.find_subline(ComponentEditorActivity.mSelectedElement);
        if(idx>=0) {
            sec=vac.sections.get(idx);
            button_done.setText(R.string.word_apply);
        }
        else {
            sec=new Section(ComponentEditorActivity.mSelectedElement);
            sec.typ=1;
            sec.line="A,B";
            button_done.setText(R.string.word_add);
        }
        sec_name.setText(sec.name);
        liste_setup();
    }

    void liste_setup() {
        adapter.clear();
        String sep[]=sec.line.split(",");
        int i;
        if(sep.length>0) {
            for(i=0;i<sep.length;i++) adapter.add(sep[i]);
        }
        adapter.notifyDataSetChanged();
        if(resetview!=null) {
            resetview.setBackgroundColor(Color.BLACK);
            resetview=null;
        }
    }
    void liste_del(int idx) {
        String dsc="";
        String sep[]=sec.line.split(",");
        int i,cnt=0;
        if(sep.length>0) {
            for(i=0;i<sep.length;i++) {
                if(i!=idx) {
                    if (cnt > 0) dsc = dsc + ",";
                    dsc = dsc + sep[i];
                    cnt++;
                }
            }
        }
        sec.line=dsc;
        liste_setup();
    }
    String liste_get(int idx) {
        String sep[]=sec.line.split(",");
        return sep[idx];
    }
    void liste_insert(int idx,String ins) {
        String dsc="";
        String sep[]=sec.line.split(",");
        int i,cnt=0;
        if(sep.length>0) {
            for (i = 0; i < sep.length; i++) {
                if (i == idx) {
                    if (cnt > 0) dsc = dsc + ",";
                    dsc = dsc + ins;
                    cnt++;
                }
                if (cnt > 0) dsc = dsc + ",";
                dsc = dsc + sep[i];
                cnt++;
            }
        }
        if(sep.length<=idx) {
            if (cnt > 0) dsc = dsc + ",";
            dsc = dsc + ins;

        }
        sec.line=dsc;
        liste_setup();
    }
    void liste_move_up(int idx) {
        if(idx>0) {
            String a=liste_get(idx);
            liste_del(idx);
            liste_insert(idx-1,a);
        }
        liste_setup();
    }
    void liste_move_down(int idx) {
        String a=liste_get(idx);
        liste_del(idx);
        liste_insert(idx+1,a);
        liste_setup();
    }
    @Override
	public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed
                // in the Action Bar.
                finish();
                return true;
        }
        return true;
    }
    public class Item {
		public String text;
		public int icon;
		Item(String text, Integer icon) {
			this.text = text;
			this.icon = icon;
		}
		@Override
		public String toString() {
			return text;
		}
	}
    @Override
	protected Dialog onCreateDialog(final int id) {
        Dialog dialog = null;
        if (id == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(mSelectedElement);
			builder.setIcon(R.drawable.pumpe);
			final String[] mitem={"^ move up","v move down","delete","add above ...","add below ...","Cancel"};
			builder.setItems(mitem, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch(which) {
                        case 0:
                            liste_move_up(mselect);
                            break;
                        case 1:
                            liste_move_down(mselect);
                            break;
                        case 2:  /* delete */
                            liste_del(mselect);
                            break;
                        case 3:
					    case 4:
					        if(which==4) mselect++;
					        showDialog(1);
					        break;
                        default:
						    if(resetview!=null) {
                                resetview.setBackgroundColor(Color.BLACK);
                                resetview=null;
                            }
					}
				}});
			dialog = builder.show();
			dialog.setCanceledOnTouchOutside(false);
        } else if(id==1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Enter Vacuum line:");
            builder.setIcon(R.drawable.pumpe);

            // Set up the input

            final VacFile vacfile=ComponentEditorActivity.vacfile;
            final Item[] items;
            items = new Item[vacfile.sections.size()];
            for(int i=0;i<vacfile.sections.size();i++) {
                items[i]=new Item(vacfile.sections.get(i).name,R.drawable.ic_menu_compose);
            }
            ListAdapter adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item,android.R.id.text1,items){
                public View getView(int position, View convertView, ViewGroup parent) {
                    //User super class to create the View
                    View v = super.getView(position, convertView, parent);
                    TextView tv = v.findViewById(android.R.id.text1);
                    if(vacfile.sections.get(position).typ==1) tv.setTextColor(Color.MAGENTA);
                    else if(vacfile.sections.get(position).typ==0) tv.setTextColor(Color.CYAN);
                    else if(vacfile.sections.get(position).typ==2) tv.setTextColor(Color.CYAN);
                    else if(vacfile.sections.get(position).typ==3) tv.setTextColor(Color.GREEN);
                    else if(vacfile.sections.get(position).typ==6) tv.setTextColor(Color.RED);
                    //Put the image on the TextView
                    tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                    //Add margin between image and text (support various screen densities)
                    int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                    tv.setCompoundDrawablePadding(dp5);
                    return v;
                }
            };

            builder.setAdapter(adapter,  new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                  liste_insert(mselect,vacfile.sections.get(which).name);
                }});

            final AlertDialog mad;
            mad = builder.create(); //don't show dialog yet
            mad.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    ListView lv = mad.getListView(); //this is a ListView with your "buds" in it
                    lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(AdapterView<?> parent, View view, int which, long id) {
                            Log.d("Long Click!","List Item #"+which+" was long clicked");
                            // showDialog(2);
                            return true;
                        }
                    });
                }
            });
            dialog = mad;

        }
        return dialog;
    }
}
