package de.drhoffmannsoftware.calcvac;

/* PlottoolActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class PlottoolActivity  extends Activity {
    private PlotView plot;
    private static final String TAG = "CALCVAC Plottool";

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.plottool);
	plot= findViewById(R.id.mainplot);
	plot.setTimeX(false);
	TextView testTextView = findViewById(R.id.dummyTextView);
	float textSize = testTextView.getTextSize();
	plot.setTextsize(textSize);
	plot.setData(MainActivity.data);
	plot.setAutoRangeX();
	plot.setAutoGridX();
	plot.setAutoRangeY1();
	plot.setAutoGridY1();
	plot.setAutoRangeY2();
	plot.setAutoGridY2();

	ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setTitle(getResources().getString(R.string.word_plottooltitle));
		}
    }
	@Override
	public void onStart() {
		super.onStart();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

		plot.setHistEnable(prefs.getBoolean("histograms", false));
		plot.setGridEnable(prefs.getBoolean("grid", true));
		plot.setEventEnable(prefs.getBoolean("event", false));
		plot.setLabelEnable(prefs.getBoolean("label", false));
		plot.setLinksEnable(prefs.getBoolean("links", true));
		plot.setLatticeEnable(prefs.getBoolean("lattice", true));
	}
	//	    @Override
	//	    public void onResume() {
	//	        super.onResume();
	//	    }

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.plottoolmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed
			// in the Action Bar.
			finish();
			return true;
		case R.id.vdl_options_preferences:
			startActivity(new Intent(this, PreferencesActivity.class));
			return true;
		case R.id.vdl_options_about:
			showDialog(0);
			return true;
		case R.id.vdl_options_calibration:
			startActivity(new Intent(this, CalibrationActivity.class));
			return true;
		case R.id.vdl_options_help:
			showDialog(1);
			return true;
		case R.id.vdl_options_finish:
			finish();
			return true;
		case R.id.menu_savebitmap:
			savebitmap();
			return true;
		default: 
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(final int id) {
		Dialog dialog = null;
		if(id==1) {
			dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.plottoolhelpdialog));
		} else if(id==0) {
			dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.description)+
					getResources().getString(R.string.impressum));
		}
		return dialog;
	}


	private void updatePlot(DataContent data) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		boolean fix1=prefs.getBoolean("fixscale1", false);
		float minscale1=prefs.getFloat("minscale1", 0);
		float maxscale1=prefs.getFloat("maxscale1", 0);
		float stepscale1=prefs.getFloat("stepscale1", 0);
		boolean fix2=prefs.getBoolean("fixscale2", false);
		float minscale2=prefs.getFloat("minscale2", 0);
		float maxscale2=prefs.getFloat("maxscale2", 0);
		float stepscale2=prefs.getFloat("stepscale2", 0);
		plot.setData(data);
		plot.setAutoRangeX();
		if(fix1) plot.setY1Range(minscale1,maxscale1);
		else plot.setAutoRangeY1();
		if(fix2) plot.setY2Range(minscale2,maxscale2);
		else plot.setAutoRangeY2();
		plot.setAutoGridX();
		if(fix1) plot.setYGrid(stepscale1);
		else plot.setAutoGridY1();
		if(fix2) plot.setY2Grid(stepscale2);
		else plot.setAutoGridY2();
		plot.postInvalidate();
	}

    public void savebitmap() {
    	    FileOutputStream fos=null;
    	    Date date = new Date();
    	    SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd-HHmmss",Locale.US);

    	    String filename="Diagram-"+sdf.format(date)+".png";
    	    Bitmap bmp;
    	    try {
    		    /* Hier jetzt ein externe von aussen lesbares verzeichnis*/
    		    File dirdata=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    		    dirdata.mkdirs();
    		    File file=new File(dirdata,filename);
    		    fos=new FileOutputStream(file,false);
    		    bmp=View2Bitmap(plot);
    		    bmp.compress(Bitmap.CompressFormat.PNG, 90, fos);
    		    fos.flush();
    		    Toast.makeText(getApplicationContext(), String.format("File %s saved.",file.getAbsolutePath()), Toast.LENGTH_LONG).show();
    	    } catch (Throwable t) {
    		    // FilenotFound oder IOException
    		    Log.e(TAG,"open/save. ",t);
    	    } finally {
    		    if(fos!=null) {
    			    try {
    				    fos.close();
    			    } catch (IOException e) {
    				    Log.e(TAG,"fos.close ",e);
    			    }
    		    }
    	    }
    }
    public static Bitmap View2Bitmap(View v) {
        Log.d(TAG,"width="+v.getLayoutParams().width);
        v.setDrawingCacheEnabled(true);
        Bitmap b= v.getDrawingCache();
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }
}
