package de.drhoffmannsoftware.calcvac;

/* ElementEditorActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


public class ElementEditorActivity  extends Activity {
    private static final String TAG = ElementEditorActivity.class.getSimpleName();
    Section sec;
    EditText sec_name,sec_length;
    Button button_done,button_cancel;
    TextView text_length, text_form, text_material, text_pumptyp;
    RadioButton t1, t2, t3, t4, t5,t6;
    RadioGroup rg1,rg2,rg3,rg4;
    CheckBox bound_p, bound_q;
    EditText bound_bp, bound_bq;
    private RadioButton ft1, ft2, ft3, ft4, ft5;
    private TextView text_par1, text_par2, text_par3, text_par4,text_par_q,text_par_ps,text_par_speed;
    private TextView parameter_1, parameter_2, parameter_3, parameter_4,parameter_q,parameter_ps,parameter_speed,molmass,temp;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.componentdetail);

        ActionBar actionBar = getActionBar();
		//  actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(getResources().getString(R.string.menu_elementeditor));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        sec_name=findViewById(R.id.component_name);
        text_length=findViewById(R.id.Text_Length);
        text_form=findViewById(R.id.text_form);
        text_material=findViewById(R.id.text_material);
        text_pumptyp= findViewById(R.id.text_pumptype);
        sec_length= findViewById(R.id.component_length);
        button_done= findViewById(R.id.button_done);
        button_cancel= findViewById(R.id.button_cancel);
        button_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
			finish();
            }
        });
        button_done.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sec.name=sec_name.getText().toString();
                sec.len=sec_length.getText().toString();
                if(sec.boundaryp) try{ sec.bp=Double.parseDouble(bound_bp.getText().toString());} catch(NumberFormatException nfp) {sec.bp=0;sec.boundaryp=false;}
                if(sec.boundaryq) try{ sec.bq=Double.parseDouble(bound_bq.getText().toString());} catch(NumberFormatException nfp) {sec.bq=0;sec.boundaryq=false;}
                if(sec.typ==4) sec.a=parameter_1.getText().toString();
                else if(sec.typ==0) {
                    sec.q = parameter_q.getText().toString();
                    sec.W = parameter_2.getText().toString();
                    sec.a = parameter_3.getText().toString();
                    sec.Ps = parameter_ps.getText().toString();
                } else if(sec.typ==2) {
                    /*form, material, parameter*/
                    if(sec.mat.isEmpty()) sec.q = parameter_q.getText().toString();
                    if(sec.form==2) sec.d=parameter_1.getText().toString();
                    else if(sec.form>=0 && sec.form<=4) sec.a=parameter_1.getText().toString();
                    if(sec.form==1 || sec.form==3 || sec.form==4) sec.b=parameter_2.getText().toString();
                    if(sec.form==4) {
                        sec.c=parameter_3.getText().toString();
                        sec.d=parameter_4.getText().toString();
                    }

                } else if(sec.typ==3) {
                    /*form, material, pumptyp, parameter*/
                    if(sec.mat.isEmpty()) sec.q = parameter_q.getText().toString();
                    if(sec.form==2) sec.d=parameter_1.getText().toString();
                    else if(sec.form>=0 && sec.form<=4) sec.a=parameter_1.getText().toString();
                    if(sec.form==1 || sec.form==3 || sec.form==4) sec.b=parameter_2.getText().toString();
                    if(sec.form==4) {
                        sec.c=parameter_3.getText().toString();
                        sec.d=parameter_4.getText().toString();
                    }
                    if(sec.ptyp.isEmpty()) sec.Ps = parameter_ps.getText().toString();
                    else sec.speed = parameter_speed.getText().toString();
                }

                VacFile vac=ComponentEditorActivity.vacfile;
                vac.set_section(sec);
    			finish();
            }
        });
        t1 =  findViewById(R.id.section_typ_general);
        t2 =  findViewById(R.id.section_typ_tube);
        t3 =  findViewById(R.id.section_typ_pump);
        t4 =  findViewById(R.id.section_typ_start);
        t5 =  findViewById(R.id.section_typ_end);
        t6 =  findViewById(R.id.section_typ_marker);

        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_typ(0);
            }
        });
        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_typ(2);

            }
        });
        t3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_typ(3);

            }
        });
        t4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_typ(4);
            }
        });
        t5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_typ(5);
            }
        });
        t6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_typ(6);
            }
        });

        rg1= findViewById(R.id.radiogroup_1);
        rg2= findViewById(R.id.radiogroup_2);
        rg3= findViewById(R.id.radiogroup_3);
        rg4= findViewById(R.id.radiogroup_4);

        bound_p= findViewById(R.id.check_boundary_p);
        bound_q= findViewById(R.id.check_boundary_q);
        bound_p.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {@Override public void onCheckedChanged(CompoundButton group, boolean ischecked) {set_boundp(ischecked); }});
        bound_q.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {@Override public void onCheckedChanged(CompoundButton group, boolean ischecked) {set_boundq(ischecked); }});
        bound_bp= findViewById(R.id.boundary_p);
        bound_bq= findViewById(R.id.boundary_q);

        ft1 = findViewById(R.id.form_square);
        ft2 = findViewById(R.id.form_rect);
        ft3 = findViewById(R.id.form_circ);
        ft4 = findViewById(R.id.form_ellipt);
        ft5 = findViewById(R.id.form_schluesselloch);

        ft1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(0);
            }
        });
        ft2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(1);
            }
        });
        ft3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(2);
            }
        });
        ft4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(3);
            }
        });
        ft5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(4);
            }
        });

        text_par1 =    findViewById(R.id.text_par1);
        text_par2 =    findViewById(R.id.text_par2);
        text_par3 =    findViewById(R.id.text_par3);
        text_par4 =    findViewById(R.id.text_par4);
        text_par_q =   findViewById(R.id.text_par_q);
        text_par_ps =  findViewById(R.id.text_par_ps);
        text_par_speed=findViewById(R.id.text_par_speed);

        parameter_1 =   findViewById(R.id.parameter_1);
        parameter_2 =   findViewById(R.id.parameter_2);
        parameter_3 =   findViewById(R.id.parameter_3);
        parameter_4 =   findViewById(R.id.parameter_4);
        parameter_q =   findViewById(R.id.parameter_q);
        parameter_ps =  findViewById(R.id.parameter_ps);
        parameter_speed=findViewById(R.id.parameter_speed);

    }

    @Override
	public void onStart() {
		super.onStart();
        VacFile vac=ComponentEditorActivity.vacfile;
        int idx=vac.find_section(ComponentEditorActivity.mSelectedElement);
        if(idx>=0) {
            sec=vac.sections.get(idx);
            button_done.setText(R.string.word_apply);
        }
        else {
            sec=new Section(ComponentEditorActivity.mSelectedElement);
            if(sec.name.startsWith("mark_")) sec.typ=6;
            button_done.setText(R.string.word_add);
        }
        sec_name.setText(sec.name);



        if(vac.isinffile || sec.typ==1) {
            rg1.setVisibility(View.GONE);
        } else {
            rg1.setVisibility(View.VISIBLE);
        }
            if(sec.typ>=4) {
                getActionBar().setTitle(R.string.marker_editor);
                t1.setVisibility(View.GONE);
                t2.setVisibility(View.GONE);
                t3.setVisibility(View.GONE);
                t4.setVisibility(View.VISIBLE);
                t5.setVisibility(View.VISIBLE);
                t6.setVisibility(View.VISIBLE);
            } else {
               getActionBar().setTitle(getResources().getString(R.string.menu_elementeditor));
                t4.setVisibility(View.GONE);
                t5.setVisibility(View.GONE);
                t6.setVisibility(View.GONE);
                t1.setVisibility(View.VISIBLE);
                t2.setVisibility(View.VISIBLE);
                t3.setVisibility(View.VISIBLE);
            }


        update_rg1();
        update_rg2();

        RadioButton v;
        rg3.removeAllViews();
        v = new RadioButton(this);
        v.setText("none");
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_mat("");
            }
        });
        rg3.addView(v);
        if(ComponentEditorActivity.vacfile.materials.size()>0) {
            int i;
            v.setChecked(sec.mat.isEmpty());
            for(i=0;i<ComponentEditorActivity.vacfile.materials.size();i++) {
                v = new RadioButton(this);
                v.setText(ComponentEditorActivity.vacfile.materials.get(i).name);
                v.setChecked((ComponentEditorActivity.vacfile.materials.get(i).name.equalsIgnoreCase(sec.mat)));
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        set_mat(((RadioButton) view).getText().toString());
                    }
                });
                rg3.addView(v);
            }
        } else v.setChecked(true);

        rg4.removeAllViews();
        v = new RadioButton(this);
        v.setText("none");
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ptyp("");
            }
        });

        rg4.addView(v);
        if(ComponentEditorActivity.vacfile.pumps.size()>0) {
            int i;
            v.setChecked(sec.ptyp.isEmpty());
            for(i=0;i<ComponentEditorActivity.vacfile.pumps.size();i++) {
                v = new RadioButton(this);
                v.setText(ComponentEditorActivity.vacfile.pumps.get(i).name);
                v.setChecked((ComponentEditorActivity.vacfile.pumps.get(i).name.equalsIgnoreCase(sec.ptyp)));
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        set_ptyp(((RadioButton) view).getText().toString());
                    }
                });
                rg4.addView(v);
            }
        } else v.setChecked(true);



        set_typ(sec.typ);

        set_boundp(sec.boundaryp);
        set_boundq(sec.boundaryq);

        bound_p.setChecked(sec.boundaryp);
        bound_q.setChecked(sec.boundaryq);
        bound_bp.setText(""+sec.bp);
        bound_bq.setText(""+sec.bq);

    }

    void update_rg1() {
        t1.setChecked(sec.typ == 0);
        t2.setChecked(sec.typ == 2);
        t3.setChecked(sec.typ == 3);
        t4.setChecked(sec.typ == 4);
        t5.setChecked(sec.typ == 5);
        t6.setChecked(sec.typ == 6);
    }
    void update_rg2() {
        ft1.setChecked(sec.form == 0);
        ft2.setChecked(sec.form == 1);
        ft3.setChecked(sec.form == 2);
        ft4.setChecked(sec.form == 3);
        ft5.setChecked(sec.form == 4);
    }

    private void set_mat(String m) {
        sec.mat=m;
        if(m.isEmpty()) {     //  wenn mat leer ist, dann biete q als Parameter an.
            text_par_q.setVisibility(View.VISIBLE);
            parameter_q.setVisibility(View.VISIBLE);
            parameter_q.setText(sec.q);
        } else {
            text_par_q.setVisibility(View.GONE);
            parameter_q.setVisibility(View.GONE);
        }
    }
    private void set_ptyp(String p) {
        sec.ptyp=p;
        if(p.isEmpty()) {   // wenn ptyp leer ist, dann biete Ps als Parameter an.
            text_par_ps.setVisibility(View.VISIBLE);
            parameter_ps.setVisibility(View.VISIBLE);
            parameter_ps.setText(sec.Ps);
            text_par_speed.setVisibility(View.GONE);
            parameter_speed.setVisibility(View.GONE);
        } else {
            text_par_ps.setVisibility(View.GONE);
            parameter_ps.setVisibility(View.GONE);
            text_par_speed.setVisibility(View.VISIBLE);
            parameter_speed.setText(sec.speed);
            parameter_speed.setVisibility(View.VISIBLE);
        }
    }
    private void set_ftyp(int ftyp) {
        sec.form = ftyp;
        text_par1.setVisibility(View.GONE);
        text_par2.setVisibility(View.GONE);
        text_par3.setVisibility(View.GONE);
        text_par4.setVisibility(View.GONE);
        text_par_q.setVisibility(View.GONE);
        text_par_ps.setVisibility(View.GONE);
        text_par_speed.setVisibility(View.GONE);

        parameter_1.setVisibility(View.GONE);
        parameter_2.setVisibility(View.GONE);
        parameter_3.setVisibility(View.GONE);
        parameter_4.setVisibility(View.GONE);
        parameter_q.setVisibility(View.GONE);
        parameter_ps.setVisibility(View.GONE);
        parameter_speed.setVisibility(View.GONE);


        switch(sec.typ) {
            case 0:  /* SECTION */
            text_par_q.setVisibility(View.VISIBLE);
            parameter_q.setVisibility(View.VISIBLE);
            parameter_q.setText(sec.q);
            text_par_ps.setVisibility(View.VISIBLE);
            parameter_ps.setVisibility(View.VISIBLE);
            parameter_ps.setText(sec.Ps);
            text_par2.setText("W=   [l/s/m]");
            text_par2.setVisibility(View.VISIBLE);
            parameter_2.setText(sec.W);
            parameter_2.setVisibility(View.VISIBLE);
            text_par3.setText("A=   [cm²/m]");
            text_par3.setVisibility(View.VISIBLE);
            parameter_3.setVisibility(View.VISIBLE);
            parameter_3.setText(sec.a);
            break;
            case 1:  /* LINE */
                break;
            case 2:  /* TUBE */
                set_mat(sec.mat);
                break;
            case 3:  /* PUMP */
                set_mat(sec.mat);
                set_ptyp(sec.ptyp);
                break;
            case 4:  /* START */
            text_par1.setText("S0=     [m]");
            text_par1.setVisibility(View.VISIBLE);
            parameter_1.setVisibility(View.VISIBLE);
            parameter_1.setText(sec.a);
            break;
        }
        if(sec.typ==2 || sec.typ==3) {
            switch(ftyp) {
                case 0:
                    text_par1.setText(R.string.acls);
                    text_par1.setVisibility(View.VISIBLE);
                    parameter_1.setVisibility(View.VISIBLE);
                    parameter_1.setText(sec.a);
                    break;
                case 1:
                text_par1.setText(R.string.aclls);
                text_par2.setText(R.string.bclss);
                text_par1.setVisibility(View.VISIBLE);
                text_par2.setVisibility(View.VISIBLE);
                parameter_1.setVisibility(View.VISIBLE);
                    parameter_1.setText(sec.a);
                parameter_2.setVisibility(View.VISIBLE);
                    parameter_2.setText(sec.b);
                break;
                case 2:
                text_par1.setText(R.string.dcd);
                text_par1.setVisibility(View.VISIBLE);
                parameter_1.setVisibility(View.VISIBLE);
                    parameter_1.setText(sec.d);
                break;
                case 3:
                text_par1.setText(R.string.dcbd);
                text_par2.setText(R.string.dcsd);
                text_par1.setVisibility(View.VISIBLE);
                text_par2.setVisibility(View.VISIBLE);
                parameter_1.setVisibility(View.VISIBLE);
                    parameter_1.setText(sec.a);
                parameter_2.setVisibility(View.VISIBLE);
                    parameter_2.setText(sec.b);
                break;
                case 4:
                text_par1.setText(R.string.acbd);
                text_par2.setText(R.string.bcsd);
                text_par3.setText(R.string.cclc);
                text_par4.setText(R.string.dcld);
                text_par1.setVisibility(View.VISIBLE);
                text_par2.setVisibility(View.VISIBLE);
                text_par3.setVisibility(View.VISIBLE);
                text_par4.setVisibility(View.VISIBLE);
                parameter_1.setVisibility(View.VISIBLE);
                parameter_1.setText(sec.a);
                parameter_2.setVisibility(View.VISIBLE);
                parameter_2.setText(sec.b);
                parameter_3.setVisibility(View.VISIBLE);
                parameter_3.setText(sec.c);
                parameter_4.setVisibility(View.VISIBLE);
                parameter_4.setText(sec.d);
                break;
            }
        }
    }

    private void set_typ(int typ) {
        sec.typ = typ;
        if (typ == 0) {
            text_length.setVisibility(View.VISIBLE);
            text_form.setVisibility(View.GONE);
            text_material.setVisibility(View.GONE);
            text_pumptyp.setVisibility(View.GONE);
            sec_length.setVisibility(View.VISIBLE);
            sec_length.setText(sec.len);
            rg2.setVisibility(View.GONE);
            rg3.setVisibility(View.GONE);
            rg4.setVisibility(View.GONE);
        } else if (typ == 1) {
            sec_length.setVisibility(View.GONE);
            text_length.setVisibility(View.GONE);
            text_form.setVisibility(View.GONE);
            text_material.setVisibility(View.GONE);
            text_pumptyp.setVisibility(View.GONE);
            rg2.setVisibility(View.GONE);
            rg3.setVisibility(View.GONE);
            rg4.setVisibility(View.GONE);
        } else if (typ == 2) {
             text_length.setVisibility(View.VISIBLE);
            sec_length.setVisibility(View.VISIBLE);
            sec_length.setText(sec.len);
            text_form.setVisibility(View.VISIBLE);
            text_material.setVisibility(View.VISIBLE);
            text_pumptyp.setVisibility(View.GONE);
            rg2.setVisibility(View.VISIBLE);
            update_rg2();
            rg3.setVisibility(View.VISIBLE);
            rg4.setVisibility(View.GONE);
        } else if (typ == 3) {
            text_length.setVisibility(View.VISIBLE);
            sec_length.setVisibility(View.VISIBLE);
            sec_length.setText(sec.len);
            text_form.setVisibility(View.VISIBLE);
            text_material.setVisibility(View.VISIBLE);
            text_pumptyp.setVisibility(View.VISIBLE);
            rg2.setVisibility(View.VISIBLE);
            update_rg2();
            rg3.setVisibility(View.VISIBLE);
            rg4.setVisibility(View.VISIBLE);
        } else if (typ == 4) {
            sec_length.setVisibility(View.GONE);
            text_length.setVisibility(View.GONE);
            text_form.setVisibility(View.GONE);
            text_material.setVisibility(View.GONE);
            text_pumptyp.setVisibility(View.GONE);
            rg2.setVisibility(View.GONE);
            rg3.setVisibility(View.GONE);
            rg4.setVisibility(View.GONE);
        } else if (typ == 5) {
            sec_length.setVisibility(View.GONE);
            text_length.setVisibility(View.GONE);
            text_form.setVisibility(View.GONE);
            text_material.setVisibility(View.GONE);
            text_pumptyp.setVisibility(View.GONE);
            rg2.setVisibility(View.GONE);
            rg3.setVisibility(View.GONE);
            rg4.setVisibility(View.GONE);
        } else if (typ == 6) {
            sec_length.setVisibility(View.GONE);
            text_length.setVisibility(View.GONE);
            text_form.setVisibility(View.GONE);
            text_material.setVisibility(View.GONE);
            text_pumptyp.setVisibility(View.GONE);
            rg2.setVisibility(View.GONE);
            rg3.setVisibility(View.GONE);
            rg4.setVisibility(View.GONE);
        }
        set_ftyp(sec.form);
    }

    void set_boundp(boolean enable) {
        if(enable) bound_bp.setVisibility(View.VISIBLE);
        else bound_bp.setVisibility(View.GONE);
        sec.boundaryp=enable;
    }
    void set_boundq(boolean enable) {
        if(enable) bound_bq.setVisibility(View.VISIBLE);
        else bound_bq.setVisibility(View.GONE);
        sec.boundaryq=enable;
    }
    @Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.elementeditmenu, menu);
		return true;
	}
    @Override
	public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed
                // in the Action Bar.
                finish();
                return true;
            case R.id.ee_options_help:
                showDialog(9);
                return true;
        }
        return true;
    }
    @Override
    protected Dialog onCreateDialog(final int id) {
        Dialog dialog = null;
        if(id==9) {
            dialog = Tools.scrollableDialog(this,"Info",getResources().getString(R.string.elementedithelpdialog));
        }
        return dialog;
    }
}
