package de.drhoffmannsoftware.calcvac;

/* PlotView.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;

/* An interactive Dataplot 
 *
 * (c) by Markus Hoffmann 2011-2018
 * 
 */ 

public class PlotView extends View  {
    private static final String TAG = PlotView.class.getSimpleName(); 
    private Paint paint;
    private int bx,by,bw,bh;
    private int sy,sw,sh;
    private DataTrace trace_x;
    private ArrayList<DataTrace> traces_y1;
    private ArrayList<DataTrace> traces_y2;

    private DataTrace hist1,hist2;

    ArrayList<Label> labels;
    ArrayList<Twiss> lattice;

    private float textsize=16;
    private int darstellung=0;  /*  0=Steps 
    				    1=lines
    				    2=points
    				    3=dots
    				    4=lines+points
    				    5=candlestics 1Min
    				    6=TODO */


    private double xstep=1000,ystep=10,y2step=10;
    private String ey="Pressure [mbar]",ex="Position [m]",ey2="Flow [nbar l/s]";
    private double xmin=-9999,xmax=9999,y1min=-99,y1max=100,y2min=-99,y2max=100;
    private double xminbound=0,xmaxbound=32100;
    private GestureDetector mgd;
    private ScaleGestureDetector mscalegd;
    private boolean istime=false;
    private boolean islogscale=true;
    private boolean dohist=false;
    private boolean dogrid=true;
    public boolean doevents=false;
    public boolean dolabels=false;
    public boolean dolinks=true;
    public boolean dolattice=true;
    public boolean doy1=true;
    public boolean doy2=true;

    public void setY1Enable(boolean a) {doy1=a;}
	public void setY2Enable(boolean a) {doy2=a;}
	public void setHistEnable(boolean a)  {	dohist=a;  }
	public void setGridEnable(boolean a)  { dogrid=a;  }
	public void setEventEnable(boolean a) {	doevents=a;}
	public void setLabelEnable(boolean a) {	dolabels=a;}
	public void setLinksEnable(boolean a) {	dolinks=a;}
	public void setLatticeEnable(boolean a) {	dolattice=a;}
	public void setUnit(String a)         { ey=a;      }
	public void setTextsize(float a) {
		textsize=a;
		Log.d(TAG,"Textsize: "+a);
	}
	public void setDarstellung(int a) { darstellung=a;}
	public void setTimeX(boolean a)   { istime=a;  }

	public void setLabels(ArrayList<Label> l) {
		labels=l;
	}
	public void setLattice(ArrayList<Twiss> l) {
		lattice=l;
	}
	public void setTraces_y1(ArrayList<DataTrace> l) {
		traces_y1=l;
	}
	public void setTraces_y2(ArrayList<DataTrace> l) {
		traces_y2=l;
	}

	public void setData(final DataContent data) {
		traces_y1=data.p_traces;
		traces_y2=data.q_traces;
		trace_x=data.x_trace;
		y1min=data.pmin;
		y2min=data.qmin;
		y1max=data.pmax;
		y2max=data.qmax;
		xmin=trace_x.ymin;
		xmax=trace_x.ymax;
		setLabels(data.labels);
		setLattice(data.lattice);
		if(traces_y1.size()>0) hist1=traces_y1.get(traces_y1.size()-1).calc_hist();
		if(traces_y2.size()>0) hist2=traces_y2.get(traces_y2.size()-1).calc_hist();
	}

	public void setXlabel(String label) {
		ex=label;
	}

	public void setRange(double x1,double x2,double y1,double y2) {
		xmin=x1;xmax=x2;y1min=y1;y1max=y2;
	}
	public void setXRange(double x1,double x2) {
		xmin=x1;xmax=x2;
	}
	public void setXBounds(double x1,double x2) {
		xminbound=x1;xmaxbound=x2;
	}
	public void setAutoRange() {
		setAutoRangeX();
		setAutoRangeY1();
		setAutoRangeY2();
	}
	public void setAutoRangeX() {
		if(trace_x.anzdata>0)  {
			double tmin=9e100,tmax=-9e100;
			tmin=Math.floor(trace_x.ymin);
			tmax=Math.floor(trace_x.ymax)+1;
			setXRange(tmin,tmax);
			setXBounds(tmin,tmax);
		}
	}

	public void setAutoRangeY1() {
		if(trace_x.anzdata>0) {
			double tmin=99999,tmax=-99999;
			double m1,m2;
			for(int i=0;i<traces_y1.size();i++) {
				m1=traces_y1.get(i).ymin;
				m2=traces_y1.get(i).ymax;
				if(m1>0) tmin=Math.min(tmin,m1);
				if(m2>0) tmax=Math.max(tmax,m2);
				Log.d(TAG,"Y1 Trace"+i+": min="+traces_y1.get(i).ymin+" max="+traces_y1.get(i).ymax);
			}
			if(islogscale) {
				tmin = Math.pow(10,Math.floor(Math.log10(tmin)));
				tmax = Math.pow(10,Math.floor(Math.log10(tmax)) + 1);
			} else {
				tmin = Math.floor(tmin);
				tmax = Math.floor(tmax) + 1;
			}
			Log.d(TAG,"Y1 Autorange/1: tmin="+tmin+" tmax="+tmax);
			setY1Range(tmin, tmax);
		}
	}
	public void setAutoRangeY1(double x1,double x2) {
		double tmin=99999,tmax=-99999;
		double m;
		if(trace_x!=null) {
			if (trace_x.anzdata > 0 && traces_y1.size() > 0) {
				for (int i = 0; i < trace_x.anzdata; i++) {
					if (trace_x.y[i] > x1 && trace_x.y[i] < x2) {
						for (int j = 0; j < traces_y1.size(); j++) {
							m = traces_y1.get(j).y[i];
							if (m > 0) {
								tmin = Math.min(tmin, m);
								tmax = Math.max(tmax, m);
							}
						}
					}
				}
				if (islogscale) {
					tmin = Math.pow(10, Math.floor(Math.log10(tmin)));
					tmax = Math.pow(10, Math.floor(Math.log10(tmax)) + 1);
				} else {
					tmin = Math.floor(tmin);
					tmax = Math.floor(tmax) + 1;
				}
				Log.d(TAG, "Y1 Autorange: tmin=" + tmin + " tmax=" + tmax);
				setY1Range(tmin, tmax);
			}
		}
	}
	public void setAutoRangeY2() {
		if(trace_x.anzdata>0) {
			double tmin=99999,tmax=-99999;
			for(int i=0;i<traces_y2.size();i++) {
				tmin=Math.min(tmin,traces_y2.get(i).ymin);
				tmax=Math.max(tmax,traces_y2.get(i).ymax);
			}
			tmin=(Math.floor(tmin*100)/100.0);
			tmax=(Math.floor(tmax*100)+1)/100.0;
			if(tmax>tmin) setY2Range(tmin, tmax);
		}
	}
	public void setAutoRangeY2(double x1,double x2) {
		double tmin=99999,tmax=-99999;
		if(trace_x==null || traces_y2==null) return;
		if(trace_x.anzdata>0 && traces_y2.size()>0) {
			for(int i=0;i<trace_x.anzdata;i++) {
				if(trace_x.y[i]>x1 && trace_x.y[i]<x2) {
					for(int j=0;j<traces_y2.size();j++) {
						tmin=Math.min(tmin,traces_y2.get(j).y[i]);
						tmax=Math.max(tmax,traces_y2.get(j).y[i]);
					}
				}
			}
			tmin=(Math.floor(tmin*100)/100.0);
			tmax=(Math.floor(tmax*100)+1)/100.0;
			if(tmax>tmin) setY2Range(tmin,tmax);
		}
	}
	public void setAutoGrid() {
		setAutoGridX();
		setAutoGridY1();
		setAutoGridY2();
	}

	public void setAutoGridX() {
		double a=xmax-xmin;
		if(istime) {
			if(a<=20) setXGrid(1); 
			else if(a<=60) setXGrid(5); 
			else if(a<=2*60) setXGrid(10); 
			else if(a<=5*60) setXGrid(30);
			else if(a<=10*60) setXGrid(60);
			else if(a<=60*60) setXGrid(60*5);
			else if(a<=2*60*60) setXGrid(60*10);
			else if(a<=5*60*60) setXGrid(60*30);
			else if(a<=24*60*60) setXGrid(60*60);
			else if(a<=24*60*60*3) setXGrid(12*60*60);
			else if(a<=24*60*60*14) setXGrid(60*60*24);
			else setXGrid(60*60*24*7);
		} else {
			if(a<=1.5) setXGrid(0.1);
			else if(a<=15) setXGrid(1);
			else if(a<=50) setXGrid(5);
			else if(a<=100) setXGrid(10);
			else if(a<=300) setXGrid(50);
			else if(a<=1000) setXGrid(100);
			else if(a<=3000) setXGrid(500);
			else if(a<=10000) setXGrid(1000);
			else if(a<=30000) setXGrid(5000);
			else if(a<=100000) setXGrid(10000);
			else if(a<=300000) setXGrid(50000);
			else if(a<=1000000) setXGrid(100000);
			else if(a<=3000000) setXGrid(500000);
			else if(a<=10000000) setXGrid(1000000);
			else setXGrid(500000);
		}
	}
	public void setAutoGridY1() {
		double a;
		if(islogscale) {
			a=Math.log10(y1max)-Math.log10(y1min);
		} else {
			a = y1max - y1min;
		}
			if(a<=1.1) setYGrid(0.1);
			else if(a<=3) setYGrid(0.5);
			else if(a<=11) setYGrid(1);
			else if(a<=30) setYGrid(5);
			else if(a<=100) setYGrid(10);
			else if(a<=300) setYGrid(50);
			else if(a<=1000) setYGrid(100);
			else if(a<=3000) setYGrid(500);
			else if(a<=10000) setYGrid(1000);
			else setYGrid(5000);

	}
	public void setAutoGridY2() {
		double a=y2max-y2min;
		if(a<0.0015) setY2Grid(0.0001);
		else if(a<0.015) setY2Grid(0.001);
		else if(a<0.15) setY2Grid(0.01);
		else if(a<0.3) setY2Grid(0.05);
		else if(a<1) setY2Grid(0.1);
		else if(a<3) setY2Grid(0.5);
		else if(a<10) setY2Grid(1);
		else if(a<30) setY2Grid(5);
		else if(a<100) setY2Grid(10);
		else if(a<=300) setY2Grid(50);
		else if(a<=1000) setY2Grid(100);
		else if(a<=3000) setY2Grid(500);
		else if(a<=10000) setY2Grid(1000);
		else setY2Grid(5000);
	}



	public void setY1Range(double y1,double y2) {
		Log.d(TAG,"Y1range: "+y1+" "+y2);
		y1min=y1;y1max=y2;
	}
	public void setY2Range(double y1,double y2) {
		Log.d(TAG,"Y2range: "+y1+" "+y2);
		y2min=y1;y2max=y2;
	}

	public void setGrid(double gx, double gy) {xstep=gx;ystep=gy;}
	public void setXGrid(double gx)  {xstep=gx;}
	public void setYGrid(double gy)  {ystep=gy;}
	public void setY2Grid(double gy) {
		Log.d(TAG,"Y2grid: "+gy);
		y2step=gy;
	}

	public void setLogscaleY(boolean f) {
		islogscale=f;
	}

	public PlotView(Context context) {
		this(context, null, 0);
	}

	public PlotView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public PlotView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);

		mscalegd = new ScaleGestureDetector(context,new MySimpleOnScaleGestureListener());
		setFocusable(true); //necessary for getting the touch events
		setFocusableInTouchMode(true);
		paint=new Paint();

		// Gesture detection

		mgd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
			public boolean onDoubleTap(MotionEvent e) {
				xmin=xminbound;
				xmax=xmaxbound;
				setAutoGridX();
				setAutoRangeY1();
				setAutoRangeY2();
				setAutoGridY1();
				setAutoGridY2();
				invalidate();
				return false;
			}
			public void onLongPress(MotionEvent e){
				setAutoRangeY1(xmin,xmax);
				setAutoRangeY2(xmin,xmax);
				setAutoGridY1();
				setAutoGridY2();
				invalidate();
			}
			public void onMove(float dx, float dy) {
				dx=(float)dx/(float)bw*(float)(xmax-xmin);
				if(xmin-dx<xminbound) dx=(float)(xmin-xminbound);
				else if(xmax-dx>xmaxbound) dx=(float)(xmax-xmaxbound);
				xmin-=dx;
				xmax-=dx;
				invalidate();
			}
			public void onResetLocation() {
				xmin=xminbound;
				invalidate();
			}
			private long startTime;
			private long endTime;
			private float totalAnimDx;
			private float totalAnimDy;
			private Interpolator animateInterpolator;


			void onAnimateMove(float dx, float dy, long duration) {
				//  animateStart = new Matrix(translate);
				animateInterpolator = new OvershootInterpolator();
				startTime = System.currentTimeMillis();
				endTime = startTime + duration;
				totalAnimDx = dx;
				totalAnimDy = dy;
				post(new Runnable() {
					@Override
					public void run() {
						onAnimateStep();
					}
				});
			}
			private void onAnimateStep() {
				long curTime = System.currentTimeMillis();
				float percentTime = (float) (curTime - startTime)
						/ (float) (endTime - startTime);
				float percentDistance = animateInterpolator
						.getInterpolation(percentTime);
				float curDx = percentDistance * totalAnimDx;
				float curDy = percentDistance * totalAnimDy;
				// translate.set(animateStart);
				onMove(curDx, curDy);

				if (percentTime < 1.0f) {
					post(new Runnable() {
						@Override
						public void run() {
							onAnimateStep();
						}
					});
				}
			}
			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}
			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2,float distanceX, float distanceY) {
				onMove(-distanceX, -distanceY);
				return true;
			}
			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				final float distanceTimeFactor = 0.1f;
				final float totalDx = (distanceTimeFactor * velocityX/2);
				final float totalDy = (distanceTimeFactor * velocityY/2);
				this.onAnimateMove(totalDx, totalDy,(long) (1000 * distanceTimeFactor));
				return false;
			}
		});
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int modex=MeasureSpec.getMode(widthMeasureSpec);
		final int sizex=MeasureSpec.getSize(widthMeasureSpec);
		final int modey=MeasureSpec.getMode(heightMeasureSpec);
		final int sizey=MeasureSpec.getSize(heightMeasureSpec);
		if (modex == MeasureSpec.UNSPECIFIED) sw=256;
		else sw=sizex;
		if (modey == MeasureSpec.UNSPECIFIED) sh=256;
		else sh=sizey;
		setMeasuredDimension(sw, sh);
	}
	private int kx(double dux)  {return bx+(int)((dux-xmin)/(xmax-xmin)*bw);}
	private int ky(double duy)  {
		if(islogscale) {
			double yymin=0,yymax=0;
			if(duy>0) duy=Math.log10(duy);
			if(y1min>0) yymin=Math.log10(y1min);
			if(y1max>0) yymax=Math.log10(y1max);

			return by+bh-(int)((duy-yymin)/(yymax-yymin)*bh);
		} else return by+bh-(int)((duy-y1min)/(y1max-y1min)*bh);
	}
	private int ky2(double duy) {return by+bh-(int)((duy-y2min)/(y2max-y2min)*bh);}

	float sperrpos=-1;
	void drawcomponent(Canvas canvas,Twiss component, int ixd) {

		if(component.pos+component.len>=xmin && component.pos<xmax) {
			float lpos=sh-sh/20-5-textsize;
			float dist=Math.max(sh/20,2*textsize);

			float x1=kx(component.pos);
			float x2=kx(component.pos+component.len);
			Paint cpaint=new Paint(paint);
			cpaint.setAntiAlias(true);
			cpaint.setStrokeCap(Cap.ROUND);
			cpaint.setStrokeWidth(1);
			if(component.conductance<0.001) {
				cpaint.setColor(Color.argb(150, 255, 0, 0));
			} else if(component.pspeed>0) {
				cpaint.setColor(Color.argb(150, 0, 255, 0));
			} else cpaint.setColor(Color.argb(150,00, 255, 255));
			cpaint.setStyle(Paint.Style.FILL_AND_STROKE);

			canvas.drawRect(x1,lpos,x2,
					lpos+textsize+5,cpaint);
			cpaint.setColor(Color.argb(200,255, 255, 0));
			String label=component.name;
			if(x1==x2) {
				float delta=0;
				if(x1<sperrpos) delta=dist/2;
				canvas.drawLine(x1, lpos,x1,lpos-dist+delta, cpaint);
				canvas.drawLine(x1, lpos-dist+delta,x1+dist,lpos-dist+delta, cpaint);
				canvas.drawText(label, x1+dist, lpos-dist+delta + textsize/2, cpaint);
				if(delta==0) sperrpos=x1+dist+cpaint.measureText(label);
				cpaint.setStrokeWidth(8);
				canvas.drawPoint(x1,lpos,cpaint);

			} else {
				if (cpaint.measureText(label) < x2 - x1) {
					canvas.drawText(label, x1 + (x2 - x1 - cpaint.measureText(label)) / 2, lpos + textsize, cpaint);
				}
			}
			if(Math.abs(component.flow)>5e-10) {
					canvas.drawLine(x2 - 10, lpos + textsize + 10, x2 + 10, lpos + textsize + 10, cpaint);
					if (component.flow > 0) {
						canvas.drawLine(x2 + 10-5, lpos + textsize + 10-3, x2 + 10, lpos + textsize + 10, cpaint);
						canvas.drawLine(x2 + 10-5, lpos + textsize + 10+3, x2 + 10, lpos + textsize + 10, cpaint);

					} else {
						canvas.drawLine(x2 - 10, lpos + textsize + 10, x2 - 10+5, lpos + textsize + 10-3, cpaint);
						canvas.drawLine(x2 - 10, lpos + textsize + 10, x2 - 10+5, lpos + textsize + 10+3, cpaint);
					}
				//	cpaint.setStrokeWidth(8);
				//	canvas.drawPoint(x2,ky2(component.flow*1e6),cpaint);
				}

		}
	}


	void drawlink(Canvas canvas,Link link, int ixd, int part, int color) {
		Paint ppaint=new Paint(paint);
		ppaint.setAntiAlias(true);
		ppaint.setColor(color);
		Paint npaint=new Paint(ppaint);
		ppaint.setStrokeCap(Cap.ROUND);
		ppaint.setStrokeWidth(8);
		npaint.setStrokeWidth(1);
		npaint.setTextSize(textsize);
		int lpos=ky2(-link.flow*1000000);
		float x1=kx(link.pos1);
        float x2=kx(link.pos2);
        int flag=0;
		if(link.pos1>=xmin && link.pos1<xmax) {
			if(part==1) canvas.drawPoint(x1,lpos,ppaint);
			double p=link.pressure;
			if(part==0) canvas.drawPoint(x1,ky(p),ppaint);
			flag=1;
		}
		if(link.pos2>=xmin && link.pos2<xmax) {
			if(part==1) canvas.drawPoint(x2,lpos,ppaint);
			double p=link.pressure;
			if(part==0) canvas.drawPoint(x2,ky(p),ppaint);
			flag=flag+2;
		}
		if(flag>0) {
            if(x1<0) x1=0;
            if(x2<0) x2=0;
            if(x1>bw) x1=bw;
            if(x2>bw) x2=bw;
			if(part==1) {
				canvas.drawLine(x1, lpos, x2, lpos, npaint);
				canvas.drawText("" + Math.abs(link.flow), x1 + (x2 - x1 - npaint.measureText("" + Math.abs(link.flow))) / 2, lpos + textsize, npaint);
				npaint.setColor(Color.argb(200,255, 255, 0));

				if(Math.abs(link.flow)>1e-9) {
					x2=x1+(x2-x1)/2;
					canvas.drawLine(x2 - 10, lpos - textsize/2, x2 + 10, lpos-textsize/2, npaint);
					if (link.flow < 0) {
						canvas.drawLine(x2 + 10-5, lpos-textsize/2-3, x2 + 10, lpos-textsize/2, npaint);
						canvas.drawLine(x2 + 10-5, lpos-textsize/2+3, x2 + 10, lpos-textsize/2, npaint);

					} else {
						canvas.drawLine(x2 - 10, lpos-textsize/2, x2 - 10+5, lpos-textsize/2-3, npaint);
						canvas.drawLine(x2 - 10, lpos-textsize/2, x2 - 10+5, lpos-textsize/2+3, npaint);
					}
					//	cpaint.setStrokeWidth(8);
					//	canvas.drawPoint(x2,ky2(component.flow*1e6),cpaint);
				}
			} else if(part==0) {
				double p = link.pressure;
				canvas.drawLine(x1, ky(p), x2, ky(p), npaint);
				canvas.drawText("#" + ixd, x1 + (x2 - x1 - npaint.measureText("#" + ixd)) / 2, ky(p) + textsize, npaint);
			}
		}
	}



	void drawlabel(Canvas canvas,Label label) {
		double datay[]=traces_y1.get(0).y;
		double datay2[]=traces_y2.get(0).y;
		Paint ppaint=new Paint(paint);
		ppaint.setAntiAlias(true);
		ppaint.setColor(Color.WHITE);
		Paint npaint=new Paint(ppaint);
		ppaint.setStrokeCap(Cap.ROUND);
		ppaint.setStrokeWidth(8);
		npaint.setStrokeWidth(1);
		npaint.setTextSize(textsize);
		float lymin,lymax;

		if(trace_x.y[label.idx]>=xmin && trace_x.y[label.idx]<xmax) {
			float x=kx(trace_x.y[label.idx]);
			lymax=lymin=ky(datay[label.idx]);
			canvas.drawPoint(x,ky(datay[label.idx]),ppaint);
			if(datay2!=null && datay2[label.idx]>0) {
				canvas.drawLine(x, ky(datay[label.idx]),x,ky2(datay2[label.idx]), npaint);
				canvas.drawPoint(x,ky2(datay2[label.idx]),ppaint);
				lymin=Math.min(ky2(datay2[label.idx]), lymin);
				lymax=Math.max(ky2(datay2[label.idx]), lymax);
				
			}

			canvas.save();
			canvas.rotate(-90, x, lymax);
			canvas.drawText(label.name,x+(lymax-lymin-npaint.measureText(label.name))/2,lymax+textsize,npaint);
			canvas.restore();
		}
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		double x,y;
		int voi=0,i;
		String xss;
		sw=getMeasuredWidth();
		sh=getMeasuredHeight();
		bw=sw;
		bh=sh;
		by=bx=0;

		canvas.drawColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setColor(Color.WHITE);
		paint.setTextSize(textsize);
		// int textWidth = (int) paint.measureText("W");
		int textHeight = (int) paint.measureText("yY");
		paint.setStrokeWidth(0);
		canvas.drawRect(bx,by,bx+bw-1,by+bh-1, paint);


		Log.d(TAG,"OnDraw: xmin="+xmin+" xmax="+xmax);
/*  Grid und Koordinatenachsen+Beschriftung für X */

		int g1=(int)(xmin/xstep);
		int g2=(int)(xmax/xstep)+1;

		// Log.d(TAG,"grid="+dogrid+" range=["+xmin+":"+xmax+"] step="+xstep);

		String dstr="";

		for(i=g1;i<g2;i++) {
			x=(double)i*xstep;
			if(dogrid) {
				paint.setColor(Color.GRAY);
				if(x>xmin && x<xmax) canvas.drawLine(kx(x),by,kx(x),by+bh, paint);
			}
			paint.setColor(Color.WHITE);
			paint.setAntiAlias(true);
			if(x>xmin && x<xmax) canvas.drawLine(kx(x),ky(0)+2,kx(x),ky(0)-2, paint);
			if(istime) {
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis((long)(x*1000.0));
				if(xmax-xmin<60*10) 
					xss=String.format(Locale.US,"%02d:%02d:%02d",
							cal.getTime().getHours(),
							cal.getTime().getMinutes(),
							cal.getTime().getSeconds());
				else if(xmax-xmin<=24*60*60)
					xss=String.format(Locale.US,"%02d:%02d",cal.getTime().getHours(),cal.getTime().getMinutes());
				else xss=String.format(Locale.US,"%02d",cal.getTime().getHours());

				y=(ky(0)-textHeight<sy+sh-2-textHeight)?ky(0)-textHeight:sy+sh-2-textHeight;
				y=Math.max(by+2*textHeight, y);

				if(istime) {
					String a=String.format(Locale.US,"%04d-%02d-%02d",
							(cal.getTime().getYear()+1900),
							(cal.getTime().getMonth()+1),
							cal.getTime().getDate()
							);
					if(!a.equals(dstr)) {
						if(kx(x)-paint.measureText(dstr)/2>bx) dstr=a;


						canvas.drawText(dstr,kx(x)-paint.measureText(dstr)/2,(float)y,paint);
					}
				}

			} else xss=String.format(Locale.US,"%.1f",x);

			if(kx(x)-(int)paint.measureText(xss)/2>voi+2) {
				y=(ky(0)+textHeight<sy+sh-2)?ky(0)+textHeight:sy+sh-2;
				y=Math.max(by+textHeight, y);
				voi=(int)paint.measureText(xss);
				canvas.drawText(xss,kx(x)-voi/2,(float)y,paint);
				voi+=kx(x)-voi/2;

			}
			paint.setAntiAlias(false);
		}

		Log.d(TAG,"OnDraw: y1min="+y1min+" y1max="+y1max+" ystep="+ystep);
		/*  Grid und Koordinatenachsen+Beschriftung für Y1 (rot) */

        if(y1min>0) {
			if (islogscale) {
				g1 = (int) (Math.log10(y1min) / ystep);
				g2 = (int) (Math.log10(y1max) / ystep) + 1;
			} else {
				g1 = (int) (y1min / ystep);
				g2 = (int) (y1max / ystep) + 1;
			}
			for (i = g1; i < g2; i++) {
				if (islogscale) {
					y = Math.pow(10, (double) i * ystep);
				} else {
					y = (double) i * ystep;
				}
				paint.setColor(Color.argb(160, 255, 50, 50));
				if (dogrid) {
					if (y > y1min && y < y1max) canvas.drawLine(bx, ky(y), bx + bw, ky(y), paint);
				}
				//paint.setColor(Color.WHITE);
				paint.setAntiAlias(true);
				if (y > y1min && y < y1max)
					canvas.drawLine(kx(0) + 2, ky(y), kx(0) - 2, ky(y), paint);
				if (islogscale) xss = String.format(Locale.US,"%3.1g", y);
				else xss = "" + Math.round(y * 10.0) / 10.0;

				voi = (int) paint.measureText(xss);
				paint.setColor(Color.argb(255, 255, 50, 50));
				canvas.drawText(xss, bx + 2, ky(y) - 1, paint);
				paint.setAntiAlias(false);
			}
		}

		/*  Grid und Koordinatenachsen+Beschriftung für Y2 (grün) */
		Log.d(TAG,"OnDraw: y2min="+y2min+" y2max="+y2max);

		if(doy2) {
			g1=(int)(y2min/y2step);
			g2=(int)(y2max/y2step)+1;
			for(i=g1;i<g2;i++) {
				y=(double)i*y2step;
				paint.setColor(Color.argb(128,50, 255, 50));
				if(dogrid) {
					if(y>y2min && y<y2max) canvas.drawLine(kx(xmin),ky2(y),kx(xmax),ky2(y), paint);
				}
				paint.setAntiAlias(true);
				if(y>y2min && y<y2max) canvas.drawLine(kx(xmax)+2,ky2(y),kx(xmax)-2,ky2(y), paint);
				xss=""+y;
				voi=(int)paint.measureText(xss);
				paint.setColor(Color.argb(200,50, 255, 50));
				canvas.drawText(xss,kx(xmax)-voi-2,ky2(y)-1,paint);
				paint.setAntiAlias(false);
			}
		}
		// Koordinatenachsen
		paint.setColor(Color.WHITE);
		if(Math.signum(xmin)!=Math.signum(xmax)) 
			canvas.drawLine(kx(0),ky(y1min),kx(0),ky(y1max),paint);

		if(Math.signum(y1min)!=Math.signum(y1max))
			canvas.drawLine(kx(xmin),ky(0),kx(xmax),ky(0),paint);

		for(i=-(int)textsize/2;i<=textsize/2;i++) {
			// Pfeile
			if(Math.signum(xmin)!=Math.signum(xmax)) 
				canvas.drawLine(kx(0)+i,ky(y1max)+textsize,kx(0),ky(y1max),paint);
			if(Math.signum(y1min)!=Math.signum(y1max))
				canvas.drawLine(kx(xmax)-textsize,ky(0)+i,kx(xmax),ky(0),paint);

		}
		// Beschriftung
		paint.setAntiAlias(true);
		canvas.drawText(ex,bx+bw-(int)paint.measureText(ex)-2,Math.max((ky(0)-textsize<sy+sh)?ky(0)-textsize:sy+sh-3,by+30),paint);
		paint.setColor(Color.argb(255,255, 50, 50));	
		canvas.drawText(ey,Math.max(0+2,kx(0)),by+15,paint);
		if(doy2) {
			paint.setColor(Color.argb(200,50, 255, 50));
			canvas.drawText(ey2,Math.max(0,kx(xmax)-(int)paint.measureText(ey2)-8),by+15,paint);
		}

		// Daten
		paint.setStrokeWidth(2);
		if(traces_y1!=null) {
			// Pressure-Kurven zeichnen
			for (int k = 0; k < traces_y1.size(); k++) Y1_plottrace(canvas, traces_y1.get(k));
		}
		if(traces_y2!=null) {
			// Flow-Kurven zeichnen
			for (int k = 0; k < traces_y2.size(); k++) Y2_plottrace(canvas, traces_y2.get(k));
		}
		// Ettiketten an Kurven
		if(dolabels && labels!=null && labels.size()>0) {
			Log.d(TAG,"Mark "+labels.size()+" Labels.");
			for(i=0;i<labels.size();i++) drawlabel(canvas,labels.get(i));
		}
		sperrpos=-1;
		// Darstellung des Lattice
		if(dolattice && lattice!=null && lattice.size()>0) {
			// Log.d(TAG,"Mark "+lattice.size()+" Components.");
			for(i=0;i<lattice.size();i++) drawcomponent(canvas,lattice.get(i),i);
		}
		/* Histogramm zeichnen */
		if(dohist && hist1!=null) draw_hist(canvas,hist1,false);
		/* Histogramm zeichnen */
		if(dohist && hist2!=null) draw_hist(canvas,hist2,true);
		paint.setAntiAlias(false);
	}

	void draw_hist(Canvas canvas,DataTrace hist,boolean right) {
		float pts2[]=new float[8*hist.anzdata];
		int count=0;
		float a=bx,b;
		if(right) a=bx+bw;
		if(right) paint.setColor(Color.argb(100,0, 255, 255));
		else paint.setColor(Color.argb(100,255, 0, 255));
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		for(int i=0;i<hist.anzdata;i++) {
			pts2[4*count]=a;
			if(right) pts2[4*count+1]=ky2(y2min+(double)i*(y2max-y2min)/hist.anzdata);
			else pts2[4*count+1]=ky(y1min+(double)i*(y1max-y1min)/hist.anzdata);
			if(right) b=(float) (bx+bw-0.3*((double)hist.y[i]/hist.ymax)*bw);
			else b=(float) (bx+0.3*((double)hist.y[i]/hist.ymax)*bw);
			pts2[4*count+2]=b;
			if(right) pts2[4*count+3]=ky2(y2min+(double)i*(y2max-y2min)/hist.anzdata);
			else pts2[4*count+3]=ky(y1min+(double)i*(y1max-y1min)/hist.anzdata);
			count++;
			pts2[4*count]=b;
			if(right) pts2[4*count+1]=ky2(y2min+(double)i*(y2max-y2min)/hist.anzdata);
			else pts2[4*count+1]=ky(y1min+(double)i*(y1max-y1min)/hist.anzdata);
			pts2[4*count+2]=b;
			if(right) pts2[4*count+3]=ky2(y2min+(double)(i+1)*(y2max-y2min)/hist.anzdata);
			else pts2[4*count+3]=ky(y1min+(double)(i+1)*(y1max-y1min)/hist.anzdata);
			count++;
			if(right) canvas.drawRect(bx,ky2(y2min+(double)i*(y2max-y2min)/hist.anzdata),b,ky2(y2min+(double)(i+1)*(y2max-y2min)/hist.anzdata), paint);
			else canvas.drawRect(bx,ky(y1min+(double)i*(y1max-y1min)/hist.anzdata),b,ky(y1min+(double)(i+1)*(y1max-y1min)/hist.anzdata), paint);
			a=b;
		}
		paint.setStyle(Paint.Style.STROKE);
		if(right)paint.setColor(Color.argb(180,0,255, 255));
		else paint.setColor(Color.argb(180,255, 0, 255));

		canvas.drawLines(pts2,0,count*4,paint);
	}


	/* Routine zeichnet einen Datensatz */
	private void Y1_plottrace(Canvas canvas, DataTrace trace) {
		int count=0;
		int anzdata=trace_x.anzdata;
		float pts[]=new float[8*anzdata];
		paint.setColor(trace.color);
		/* Label an die Kurve */
		float lx,ly;
		lx=kx(trace_x.y[trace.idxmax]);
        ly=ky(trace.ymax);
		if(lx<bx+bw && ly<by+bh && lx+40+paint.measureText(trace.name)>0) {
			Paint npaint=new Paint(paint);
			npaint.setColor(Color.argb(200,200,200,200));
			npaint.setStrokeCap(Cap.ROUND);
			npaint.setStrokeWidth(8);
			npaint.setTextSize(textsize);

			canvas.drawPoint(lx,ly,npaint);
			npaint.setStrokeWidth(1);
			canvas.drawLine(lx, ly, lx + 20, ly - 20, npaint);
			canvas.drawLine(lx + 20, ly - 20, lx + 40, ly - 20, npaint);
			canvas.drawText(trace.name, lx + 40, ly - 20+textsize/2, npaint);
		}

        if(darstellung==0) {
			for(int i=0;i<anzdata;i++) {
				if((i==anzdata-1 || trace_x.y[i+1]>xmin) && trace_x.y[i]<xmax) {
					if(i>0) {
						pts[4*count]=kx(trace_x.y[i]);
						pts[4*count+1]=ky(trace.y[i-1]);
						pts[4*count+2]=kx(trace_x.y[i]);
						pts[4*count+3]=ky(trace.y[i]);
						count++;
					}
					if(i<anzdata-1) {
						pts[4*count]=kx(trace_x.y[i]);
						pts[4*count+1]=ky(trace.y[i]);
						pts[4*count+2]=kx(trace_x.y[i+1]);
						pts[4*count+3]=ky(trace.y[i]);
						count++;
					}
				} 
			}
			canvas.drawLines(pts,0,count*4,paint);
		} else if(darstellung==1 || darstellung==4) {
			for(int i=0;i<anzdata;i++) {
				if((i==anzdata-1 || trace_x.y[i+1]>xmin) && trace_x.y[i]<xmax) {
					if(i<anzdata-1) {
						pts[4*count]=kx(trace_x.y[i]);
						pts[4*count+1]=ky(trace.y[i]);
						pts[4*count+2]=kx(trace_x.y[i+1]);
						pts[4*count+3]=ky(trace.y[i+1]);
						count++;
					}
				}
			}
			canvas.drawLines(pts,0,count*4,paint);
		}
		if(darstellung==2 || darstellung==3 || darstellung==4) {
			count=0;
			for(int i=0;i<anzdata;i++) {
				if(trace_x.y[i]>xmin && trace_x.y[i]<xmax) {
					pts[2*count]=kx(trace_x.y[i]);
					pts[2*count+1]=ky(trace.y[i]);
					count++;
				}
			}
			if(darstellung==3) canvas.drawPoints(pts, 0, 2*count, paint);
			else {
				Paint npaint=new Paint(paint);
				npaint.setStrokeCap(Cap.ROUND);
				npaint.setStrokeWidth(8);
				canvas.drawPoints(pts, 0, 2*count, npaint);
			}
		} else if(darstellung==5) {
			// TODO
		}
		// Darstellung der Links (in flow-Kurve, grün)
		if(dolinks && trace.links!=null && trace.links.size()>0) {
			for(int i=0;i<trace.links.size();i++) drawlink(canvas,trace.links.get(i),i,0,trace.color);
		}
	}
	/* Routine zeichnet einen Datensatz */
	private void Y2_plottrace(Canvas canvas, DataTrace trace) {
		int count=0;
		int anzdata=trace_x.anzdata;
		float pts[]=new float[8*anzdata];
		paint.setColor(trace.color);

		/* Label an die Kurve */
		float lx,ly;
		lx=kx(trace_x.y[trace.idxmax]);
		ly=ky2(trace.ymax);
		if(lx<bx+bw && ly<by+bh && lx+40+paint.measureText(trace.name)>0) {
			Paint npaint=new Paint(paint);
			npaint.setStrokeCap(Cap.ROUND);
			npaint.setStrokeWidth(8);
			npaint.setTextSize(textsize);
			npaint.setColor(Color.argb(200,200,200,200));

			canvas.drawPoint(lx,ly,npaint);
			npaint.setStrokeWidth(1);
			canvas.drawLine(lx, ly, lx + 20, ly - 20, npaint);
			canvas.drawLine(lx + 20, ly - 20, lx + 40, ly - 20, npaint);
			canvas.drawText(trace.name, lx + 40, ly - 20+textsize/2, npaint);
		}


		if(darstellung==0) {
			for(int i=0;i<anzdata;i++) {
				if((i==anzdata-1 || trace_x.y[i+1]>xmin) && trace_x.y[i]<xmax) {
					if(i>0) {
						pts[4*count]=kx(trace_x.y[i]);
						pts[4*count+1]=ky2(trace.y[i-1]);
						pts[4*count+2]=kx(trace_x.y[i]);
						pts[4*count+3]=ky2(trace.y[i]);
						count++;
					}
					if(i<anzdata-1) {
						pts[4*count]=kx(trace_x.y[i]);
						pts[4*count+1]=ky2(trace.y[i]);
						pts[4*count+2]=kx(trace_x.y[i+1]);
						pts[4*count+3]=ky2(trace.y[i]);
						count++;
					}
				}
			}
			canvas.drawLines(pts,0,count*4,paint);
		} else if(darstellung==1 || darstellung==4) {
			for(int i=0;i<anzdata;i++) {
				if((i==anzdata-1 || trace_x.y[i+1]>xmin) && trace_x.y[i]<xmax) {
					if(i<anzdata-1) {
						pts[4*count]=kx(trace_x.y[i]);
						pts[4*count+1]=ky2(trace.y[i]);
						pts[4*count+2]=kx(trace_x.y[i+1]);
						pts[4*count+3]=ky2(trace.y[i+1]);
						count++;
					}
				}
			}
			canvas.drawLines(pts,0,count*4,paint);
		}
		if(darstellung==2 || darstellung==3 || darstellung==4) {
			count=0;
			for(int i=0;i<anzdata;i++) {
				if(trace_x.y[i]>xmin && trace_x.y[i]<xmax) {
					pts[2*count]=kx(trace_x.y[i]);
					pts[2*count+1]=ky2(trace.y[i]);
					count++;
				}
			}
			if(darstellung==3) canvas.drawPoints(pts, 0, 2*count, paint);
			else {
				Paint npaint=new Paint(paint);
				npaint.setStrokeCap(Cap.ROUND);
				npaint.setStrokeWidth(8);
				canvas.drawPoints(pts, 0, 2*count, npaint);
			}
		} else if(darstellung==5) {
			// TODO
		}
		// Darstellung der Links (in flow-Kurve, grün)
		if(dolinks && trace.links!=null && trace.links.size()>0) {
			for(int i=0;i<trace.links.size();i++) drawlink(canvas,trace.links.get(i),i,1,trace.color);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mscalegd.onTouchEvent(event);
		mgd.onTouchEvent(event);
		return true;
	}

	public class MySimpleOnScaleGestureListener extends SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			final float scaleFactor = detector.getScaleFactor();
			double x=detector.getFocusX()/bw*(xmax-xmin)+xmin;
			xmin=(xmin-x)/scaleFactor+x;
			xmax=(xmax-x)/scaleFactor+x;
			xmax=Math.min(xmax,xmaxbound);
			xmin=Math.max(xmin,xminbound);
			//   Log.d("TAG","Scale "+detector.getScaleFactor()+" x="+x);
			setAutoGridX();
			invalidate();
			return true;
		}
	}
}
