package de.drhoffmannsoftware.calcvac;

/* TabletoolActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.util.Locale;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import static de.drhoffmannsoftware.calcvac.MainActivity.data;

public class TabletoolActivity  extends Activity {
    private static final String TAG = TabletoolActivity.class.getSimpleName();

    TableLayout mtable=null;
    TableLayout legende=null;
    TextView message1;
    TextView message2;
    ScaleGestureDetector mscalegd;
    DataContent mdata;

    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabletool);

		mdata= data;

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setTitle(getResources().getString(R.string.word_tabletooltitle));
		}
		mtable= findViewById(R.id.maintable);
		legende= findViewById(R.id.legende);
		message1= findViewById(R.id.message1);
		message2= findViewById(R.id.message2);

		mscalegd = new ScaleGestureDetector(this,new MySimpleOnScaleGestureListener());
		mtable.setFocusable(true); //necessary for getting the touch events
		mtable.setFocusableInTouchMode(true);
		mtable.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				if(!mscalegd.isInProgress()) mscalegd.onTouchEvent(event);
				return true;
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
			if(mdata!=null) updateTable(mdata);
	}

	private String find_link(String name) {
		String ret="";
		if(data.p_traces.size()>0 && data.p_traces.get(0).links.size()>0) {
			for (int i = 0; i < data.p_traces.get(0).links.size();i++){
              if(name.equalsIgnoreCase(data.p_traces.get(0).links.get(i).name1)) {
              	ret=ret+"(*"+data.p_traces.get(0).links.get(i).idx+")";
			  }
			  if(name.equalsIgnoreCase(data.p_traces.get(0).links.get(i).name2)) {
              	ret=ret+"("+data.p_traces.get(0).links.get(i).idx+"*)";
              }
			}
		}
		return ret;
	}

	private void updateTable(DataContent data) {
		TableRow.LayoutParams params0 = new TableRow.LayoutParams(100, 40);
		TableRow.LayoutParams params1 = new TableRow.LayoutParams(60, 40);
		if(mtable!=null) mtable.removeAllViews();
		if(legende!=null) legende.removeAllViews();

		/*Mache Legende für die Tabelle*/

		TableRow row = new TableRow(this);
		for (int j = 0; j < 6; j++) {
			TextView text = new TextView(this);
			text.setTextColor(Color.WHITE);

			if(j==0) text.setLayoutParams(params0);
			else text.setLayoutParams(params1);
			if(j==0) text.setText(getString(R.string.word_name));
			else if(j==1) text.setText("Pos [m]");
			else if(j==2) text.setText("L [m]");
			else if(j==3) text.setText("Surf [cm²]");
			else if(j==4) text.setText("Cond [l m/s]");
			else if(j==5) text.setText("Pump [l s/m]");
			else if(j==6) text.setText("Ogas [mbar l/s m]");
			else text.setText("");
			row.addView(text);
		}
		row.setLongClickable(true);
		if(legende!=null) legende.addView(row);

		for (int i = 0; i < (data.lattice.size()<256?data.lattice.size():256); i++) {
				row = new TableRow(this);
				for (int j = 0; j < 6; j++) {
					TextView text = new TextView(this);
					text.setTextColor(Color.YELLOW);
					if(j==0) text.setLayoutParams(params0);
					else text.setLayoutParams(params1);
					if(j==0) {
						String a = find_link(data.lattice.get(i).name);
                        text.setText(String.format(Locale.US,"%s %s", data.lattice.get(i).name,a));
						if(data.lattice.get(i).len<0) text.setTextColor(Color.RED);
						else if(data.lattice.get(i).len==0) text.setTextColor(Color.YELLOW);
						else if(data.lattice.get(i).pspeed>0) text.setTextColor(Color.GREEN);
						else if(data.lattice.get(i).conductance<0.001) text.setTextColor(Color.RED);
						else text.setTextColor(Color.CYAN);
					} else if(j==1) {
						text.setTextColor(Color.YELLOW);
						text.setText(""+data.lattice.get(i).pos);
					} else if(j==2) {
						if(data.lattice.get(i).len==0) text.setTextColor(Color.DKGRAY);
						else text.setTextColor(Color.YELLOW);
						text.setText(""+data.lattice.get(i).len);
					} else if(j==3) {
						if(data.lattice.get(i).area<=0) text.setTextColor(Color.DKGRAY);
						else text.setTextColor(Color.YELLOW);
						text.setText(""+data.lattice.get(i).area);
					} else if(j==4) {
						if(data.lattice.get(i).conductance<=0) text.setTextColor(Color.DKGRAY);
						else text.setTextColor(Color.YELLOW);
						text.setText(""+data.lattice.get(i).conductance);
					} else if(j==5) {
						if(data.lattice.get(i).pspeed<=0) text.setTextColor(Color.DKGRAY);
						else text.setTextColor(Color.YELLOW);
						text.setText(""+data.lattice.get(i).pspeed);
					}
					row.addView(text);
				}
			    row.setLongClickable(true);
			    if(mtable!=null) mtable.addView(row);
			}
			message1.setText(""+data.lattice.size()+" components.\n"+
					((data.p_traces.size()>0 && data.p_traces.get(0).links!=null)?data.p_traces.get(0).links.size():0)+" links.\n"
			);
			message2.setText(""+(data.labels!=null?data.labels.size():0)+" labels. Press=["+
					String.format("%.5g",data.pmin)+":"+String.format("%.5g",data.pmax)+"], "+
					"flow=["+String.format("%.5g",data.qmin)+":"+String.format("%.5g",data.qmax)+"].");

	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.tabletoolmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed
			// in the Action Bar.
			finish();
			return true;
		case R.id.vdl_options_preferences:
			startActivity(new Intent(this, PreferencesActivity.class));
			return true;
		case R.id.vdl_options_about:
			showDialog(0);
			return true;
		case R.id.vdl_options_help:
			showDialog(1);
			return true;
		case R.id.vdl_options_finish:
			finish();
			return true; 
		default:
			return super.onOptionsItemSelected(item);
		}
	}

    @Override
    protected Dialog onCreateDialog(final int id) {
    	    Dialog dialog = null;
    	    if(id==1) dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.tabletoolhelpdialog));
    	    else if(id==0) dialog = Tools.scrollableDialog(this,"Calcvac",
    			    getResources().getString(R.string.description)+
    			    getResources().getString(R.string.impressum));
    	    return dialog;
    }

    public class MySimpleOnScaleGestureListener extends SimpleOnScaleGestureListener {
    	    @Override
    	    public boolean onScale(ScaleGestureDetector detector) {
    		    final float scaleFactor = detector.getScaleFactor();
    		    double x=detector.getFocusX();
    		    Log.d(TAG,"Scale: "+scaleFactor+" x="+x);
    	    //      if(scaleFactor>1 && displaylevel>0) displaylevel--;
    //  	    else if(scaleFactor<1 && displaylevel<5) displaylevel++;
    		    if(mdata!=null) {
    //  		    if(displaylevel!=olddl) {
    				    mtable.invalidate();
    //  		    }
	    }
	    return true;
	}
    }
}
