package de.drhoffmannsoftware.calcvac;

/* LeitwerteCalculatorActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TableRow;
import android.widget.TextView;

public class LeitwerteCalculatorActivity extends Activity {
    private static final String TAG = LeitwerteCalculatorActivity.class.getSimpleName();
    private VacComponent vaccomp;
    private static int form_type = 0;
    private TextView component_length;
    private TextView text_par1, text_par2, text_par3, text_par4;
    private TextView parameter_1, parameter_2, parameter_3, parameter_4,molmass,temp;
    private TextView result;
    private RadioButton ft1, ft2, ft3, ft4, ft5;
    private SeekBar seek_temp;

    double par1, par2, par3, par4;
    int molekul_mass=28;
    int temperature=300;
    boolean hinder_loop=false;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leitwertcalculator);
        ActionBar actionBar = getActionBar();
        //  actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.menu_leitwerte));

        vaccomp = new VacComponent();
        component_length = findViewById(R.id.component_length);
        result = findViewById(R.id.conductance_result);
        text_par1 = findViewById(R.id.text_par1);
        text_par2 = findViewById(R.id.text_par2);
        text_par3 = findViewById(R.id.text_par3);
        text_par4 = findViewById(R.id.text_par4);

        parameter_1 = findViewById(R.id.parameter_1);
        parameter_2 = findViewById(R.id.parameter_2);
        parameter_3 = findViewById(R.id.parameter_3);
        parameter_4 = findViewById(R.id.parameter_4);
        molmass = findViewById(R.id.molmass);
        Button mol_select = findViewById(R.id.molmass_select);
        temp =findViewById(R.id.temperature);
        seek_temp=findViewById(R.id.seek_temp);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mol_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(7);
            }
        });
        seek_temp.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
          int progress = 0;

          // When Progress value changed.
          @Override
          public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
              progress = progressValue;
              if(!hinder_loop) temp.setText(""+progressValue*10);
          }

          // Notification that the user has started a touch gesture.
          @Override
          public void onStartTrackingTouch(SeekBar seekBar) {
          }

          // Notification that the user has finished a touch gesture
          @Override
          public void onStopTrackingTouch(SeekBar seekBar) {
              temp.setText(""+progress*10);
          }
      });


        parameter_1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    try {par1 = Double.parseDouble(s.toString());} catch(NumberFormatException nfe) {par1=0;}
                    recalc();
                }

            }
        });
        parameter_2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    try {par2 = Double.parseDouble(s.toString());} catch(NumberFormatException nfe) {par2=0;}
                    recalc();
                }

            }
        });
        parameter_3.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    try {par3 = Double.parseDouble(s.toString());} catch(NumberFormatException nfe) {par3=0;}
                    recalc();
                }

            }
        });
        parameter_4.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    try {par4 = Double.parseDouble(s.toString());} catch(NumberFormatException nfe) {par4=0;}
                    recalc();
                }

            }
        });
        molmass.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                   try { molekul_mass = Integer.parseInt(s.toString());} catch(NumberFormatException nfe) {molekul_mass=28;}
                    if(molekul_mass>0) recalc();
                }

            }
        });
        temp.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    try { temperature = Integer.parseInt(s.toString());} catch(NumberFormatException nfe) {temperature=300;}
                    hinder_loop=true;
                    seek_temp.setProgress(temperature/10);
                    hinder_loop=false;
                    if(temperature>0) recalc();
                }

            }
        });

        component_length.setText("" + vaccomp.len);
        component_length.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    try {vaccomp.len = Double.parseDouble(s.toString());} catch(NumberFormatException nfe) {vaccomp.len=0;}
                    recalc();
                }
            }
        });
        ft1 = (RadioButton) findViewById(R.id.form_square);
        ft2 = (RadioButton) findViewById(R.id.form_rect);
        ft3 = (RadioButton) findViewById(R.id.form_circ);
        ft4 = (RadioButton) findViewById(R.id.form_ellipt);
        ft5 = (RadioButton) findViewById(R.id.form_schluesselloch);


        ft1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(0);
            }
        });
        ft2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(1);
            }
        });
        ft3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(2);
            }
        });
        ft4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(3);
            }
        });
        ft5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_ftyp(4);
            }
        });

        set_ftyp(vaccomp.ftyp);
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        set_ftyp(prefs.getInt("lc_ftyp", 0));
        ft1.setChecked(vaccomp.ftyp == 0);
        ft2.setChecked(vaccomp.ftyp == 1);
        ft3.setChecked(vaccomp.ftyp == 2);
        ft4.setChecked(vaccomp.ftyp == 3);
        ft5.setChecked(vaccomp.ftyp == 4);
        temperature=prefs.getInt("lc_temperature", 300);
        temp.setText(""+temperature);
        molekul_mass=prefs.getInt("lc_molmass", 28);
        molmass.setText(""+molekul_mass);
        par1=prefs.getFloat("lc_par1", 1);
        par2=prefs.getFloat("lc_par2", 1);
        par3=prefs.getFloat("lc_par3", 1);
        par4=prefs.getFloat("lc_par4", 1);
        parameter_1.setText(""+par1);
        parameter_2.setText(""+par2);
        parameter_3.setText(""+par3);
        parameter_4.setText(""+par4);
        vaccomp.len=prefs.getFloat("lc_complen", (float)1.0);
        component_length.setText(""+vaccomp.len);
        recalc();
    }
    @Override
    protected void onStop(){
        super.onStop();

       // We need an Editor object to make preference changes.
       // All objects are from android.context.Context
       SharedPreferences settings = getPreferences(MODE_PRIVATE);
       SharedPreferences.Editor editor = settings.edit();
       editor.putFloat("lc_par1", (float)par1);
       editor.putFloat("lc_par2", (float)par2);
       editor.putFloat("lc_par3", (float)par3);
       editor.putFloat("lc_par4", (float)par4);
       editor.putFloat("lc_complen", (float)vaccomp.len);
       editor.putInt("lc_molmass", molekul_mass);
       editor.putInt("lc_temperature", temperature);
       editor.putInt("lc_ftyp", vaccomp.ftyp);

       // Commit the edits!
       editor.apply();
     }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.leitwertcalculatormenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent viewIntent;
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed
                // in the Action Bar.
                finish();
                return true;

            case R.id.lc_options_about:
                showDialog(0);
                break;
            case R.id.lc_options_help:
                showDialog(1);
                break;
            case R.id.lc_options_finish:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public class Item {
        public String text;
        public int icon;
        Item(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }
        @Override
        public String toString() {
            return text;
        }
    }

    static Integer[] tab;
    static Item[] items;
    @Override
    protected Dialog onCreateDialog(final int id) {
        Dialog dialog = null;
        if (id == 0) {
            dialog = Tools.scrollableDialog(this, "About", getResources().getString(R.string.description) +
                    getResources().getString(R.string.impressum));
        } else if (id == 1) {
            dialog = Tools.scrollableDialog(this, "Short Help", getResources().getString(R.string.leitwertcalchelpdialog));
        }  else if(id==7) {/* Gas AUswahl */
            int i;
            int cnt=0;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.select_gas);
            builder.setIcon(R.drawable.gas);
            for(i=0;i<Gas.moles.length;i++) {
                if(!Gas.moles[i].isEmpty()) cnt++;
            }

            tab=new Integer[cnt];
            items = new Item[cnt];
            cnt=0;
            for(i=0;i<Gas.moles.length;i++) {
                if(!Gas.moles[i].isEmpty()) {
                    tab[cnt]=i;
                    items[cnt++]=new Item(Gas.moles[i],R.drawable.atom);
                }
            }
            ListAdapter adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item,
                    android.R.id.text1,items) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    //User super class to create the View
                    View v = super.getView(position, convertView, parent);
                    TextView tv = (TextView)v.findViewById(android.R.id.text1);

                    //Put the image on the TextView
                    tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                    //Add margin between image and text (support various screen densities)
                    int dp5 = pix(5);
                    tv.setCompoundDrawablePadding(dp5);
                    return v;
                }
            };

            builder.setAdapter(adapter,  new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // (Gas.moles[tab[which]]);
                    molmass.setText(""+tab[which]);
                }});
            dialog= builder.create();
        } else {
            dialog = Tools.scrollableDialog(this, "", getResources().getString(R.string.description) +
                    getResources().getString(R.string.impressum));
        }
        return dialog;
    }

    private void set_ftyp(int ftyp) {
        vaccomp.ftyp = ftyp;
        if (ftyp == 0) {
            text_par1.setText(R.string.acls);
            text_par1.setVisibility(View.VISIBLE);
            text_par2.setVisibility(View.GONE);
            text_par3.setVisibility(View.GONE);
            text_par4.setVisibility(View.GONE);

            parameter_1.setVisibility(View.VISIBLE);
            parameter_2.setVisibility(View.GONE);
            parameter_3.setVisibility(View.GONE);
            parameter_4.setVisibility(View.GONE);

        } else if (ftyp == 1) {
            text_par1.setText(R.string.aclls);
            text_par2.setText(R.string.bclss);
            text_par1.setVisibility(View.VISIBLE);
            text_par2.setVisibility(View.VISIBLE);
            text_par3.setVisibility(View.GONE);
            text_par4.setVisibility(View.GONE);
            parameter_1.setVisibility(View.VISIBLE);
            parameter_2.setVisibility(View.VISIBLE);
            parameter_3.setVisibility(View.GONE);
            parameter_4.setVisibility(View.GONE);

        } else if (ftyp == 2) {
            text_par1.setText(R.string.dcd);
            text_par1.setVisibility(View.VISIBLE);
            text_par2.setVisibility(View.GONE);
            text_par3.setVisibility(View.GONE);
            text_par4.setVisibility(View.GONE);
            parameter_1.setVisibility(View.VISIBLE);
            parameter_2.setVisibility(View.GONE);
            parameter_3.setVisibility(View.GONE);
            parameter_4.setVisibility(View.GONE);

        } else if (ftyp == 3) {
            text_par1.setText(R.string.dcbd);
            text_par2.setText(R.string.dcsd);
            text_par1.setVisibility(View.VISIBLE);
            text_par2.setVisibility(View.VISIBLE);
            text_par3.setVisibility(View.GONE);
            text_par4.setVisibility(View.GONE);
            parameter_1.setVisibility(View.VISIBLE);
            parameter_2.setVisibility(View.VISIBLE);
            parameter_3.setVisibility(View.GONE);
            parameter_4.setVisibility(View.GONE);

        } else if (ftyp == 4) {
            text_par1.setText(R.string.acbd);
            text_par2.setText(R.string.bcsd);
            text_par3.setText(R.string.cclc);
            text_par4.setText(R.string.dcld);
            text_par1.setVisibility(View.VISIBLE);
            text_par2.setVisibility(View.VISIBLE);
            text_par3.setVisibility(View.VISIBLE);
            text_par4.setVisibility(View.VISIBLE);
            parameter_1.setVisibility(View.VISIBLE);
            parameter_2.setVisibility(View.VISIBLE);
            parameter_3.setVisibility(View.VISIBLE);
            parameter_4.setVisibility(View.VISIBLE);

        }
        recalc();
    }


    /*  Calculate leitwerte */

    private static double umfang_rechteck(double a, double b) {
        return (2 * (a + b) * 100 * 100);
    } /* in [cm²/m] */

    private static double umfang_ellipt(double ua, double ub) {
        double eprod=1, oprod=1, aa, bb, eps, sum=1;
        int i;
        if (ub > ua) {
            aa = ub;
            ub = ua;
            ua = aa;
        }
        aa = ua / 2;
        bb = ub / 2;
        eps = (aa * aa - bb * bb) / aa / aa;
        for (i = 1; i <= 10; i++) {
            oprod *= 2 * (double) i - 1;
            eprod *= 2 * (double) i;
            sum += -1 / (2 * (double) i - 1) * Math.pow(eps, (double) i) * (oprod / eprod) * (oprod / eprod);
        }
        // Genauigkeit etwa 1e-4
        return (Math.floor(ua * Math.PI * sum * 100 * 100));   // in cm^2/m
    }
    private static double leitwert_schluesselloch(double a, double b, double c, double d, double l, double m,double t) {
        return leitwert_ellipt(a,b,l,m,t)+leitwert_rechteck(c,d,l,m,t);
    }
    private static double umfang_schluesselloch(double a, double b, double c, double d) {
        return 2*c*100*100+umfang_ellipt(a,b);
    }
    private static double kk(double x) {return(0.0653947/(x+0.0591645)+1.0386);}
    private static double leitwert_rechteck(double a, double b, double l, double m,double t) {
        double c1, c2, c3;
        a*=100;
        b*=100;
        l*=100;
        c1=11.6*a*b;
        c2=30.9*kk(Math.min(a/b, b/a));
        c2*=a*a*b*b/(a+b)/l;
        c2=17.1*a*a*b*b/Math.sqrt(a * a + b * b) / l;
        c3=1/(1/c1+1/c2);
        return (Math.floor(Math.sqrt(28/m*t/300)*c3*100)/100.0); // in l/s
    }
    private static double leitwert_ellipt(double a,double b,double l,double m,double t) {
        double c1, c2, c3;
        a = a * 100;
        b = b * 100;
        l = l * 100;
        c1 = 11.6 * a * b * Math.PI / 4;
        c2 = 17.1 * a * a * b * b / Math.sqrt(a * a + b * b) / l;
        c3 = 1 / (1 / c1 + 1 / c2);
        return Math.floor(Math.sqrt(28 / m*t/300) * c3 * 100) / 100.0; // in l / s
    }
    public static double leitwert_rund(double a,double l,double m,double t) {
        double c1,c2,c3;
        a*=100;
        l*=100;
        c1=11.6*a*a*Math.PI/4;
        c2=17.1*a*a*a*a/Math.sqrt(a*a+a*a)/l;
        c3=1/(1/c1+1/c2);
        return Math.floor(Math.sqrt(28/m*t/300)*c3*100)/100.0;  // in l/s
    }


    private void recalc() {
       double w=0,a=0;
       if(vaccomp.ftyp==0) {
            a=umfang_rechteck(par1,par1);
            w=leitwert_rechteck(par1,par1,vaccomp.len,molekul_mass,temperature);
       } else if(vaccomp.ftyp==1) {
            a=umfang_rechteck(par1,par2);
            w=leitwert_rechteck(par1,par2,vaccomp.len,molekul_mass,temperature);
       } else if(vaccomp.ftyp==2) {
           a=umfang_ellipt(par1,par1);
           w=leitwert_rund(par1,vaccomp.len,molekul_mass,temperature);
       } else if(vaccomp.ftyp==3) {
           a=umfang_ellipt(par1,par2);
           w=leitwert_ellipt(par1,par2,vaccomp.len,molekul_mass,temperature);
       } else if(vaccomp.ftyp==4) {
           a=umfang_schluesselloch(par1,par2,par3,par4);
           w=leitwert_schluesselloch(par1,par2,par3,par4,vaccomp.len,molekul_mass,temperature);
       }
       result.setText("C="+w+" l/s\nA="+a+" cm²/m");
    }

    private int pix(int dp) {
        return (int) (dp * getResources().getDisplayMetrics().density + 0.5f);
    }
}
