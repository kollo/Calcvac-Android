package de.drhoffmannsoftware.calcvac;

/* MainActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.view.ViewGroup;
import android.content.DialogInterface.OnShowListener;


public class MainActivity extends Activity {
    private static final String TAG = "Calcvac";
    public static String mChosenFile="new.vac";
    private static String mSelectedFile="";
    public static File Basestoragedirectory;
    private static boolean misloaded=false;
    private static String[] mFileList;
    private int dialogi=0;
    private static final String RELINFO = "prefs_relinfo_1";
    private AlertDialog mad;
    static Item[] items;
    private TextView statustext;
    private TextView ubertext;
    private TextView uberstatus;
    private PlotView plot;
    private static ProgressBar progress;
    public static String last_errors="no errors";
    public static String last_warnings="no warnings";
    public static String last_infos="no infos";
    public static int nplot=3*1024;
    public static double resolution=0.001;

    public static DataContent data=new DataContent();
    final static Handler h=new Handler();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences settings =PreferenceManager.getDefaultSharedPreferences(this);

		/*Zeige RELEASE Notes an, wenn ganz neu installiert...*/
		boolean shown_releaseinfo=settings.getBoolean(RELINFO, false);
		if(!shown_releaseinfo) {
		  SharedPreferences.Editor editor = settings.edit();
		  editor.putBoolean(RELINFO, true);
		  showDialog(0);
		  editor.apply();
		}

		setContentView(R.layout.main);
		statustext=findViewById(R.id.status);
		ubertext= findViewById(R.id.ueberschrift);
		uberstatus= findViewById(R.id.ueberstatus);
		plot= findViewById(R.id.plot);
		progress=findViewById(R.id.progress1);


		// There are two situations where onCreate is called: (a) a completely new activity;
		// (b) a new activity created as part of a change in orientation.
		// We can tell the difference by looking at the result of getLastNonConfigurationInstance.
		Object retained = getLastNonConfigurationInstance();
		if (retained == null) {
			// If there is no retained object, no Background Thread is in Progress
			/* Hier dürfen wir davon ausgehen, dass die App zum ersten mal gestartet wird, und nicht noch
			   im Hintergrund läuft oder nur Wiedergestartet wurde.
			   */

			/* Info vom vacline binary anzeigen: */
			set_statustext(run_vacline(""));
			/* Check if the SD-Card is usable. If not, display an error and try to use the internal
			 * Storage. */
			boolean SDcardusable = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
			if(SDcardusable) {
				Basestoragedirectory=Environment.getExternalStorageDirectory();
			} else {
				Basestoragedirectory=getFilesDir();
				showDialog(7);
			}
			asset_copy("calcvac"); /*copy the example files*/
		}
		if(misloaded) setTitle(mChosenFile);
		else setTitle("Calcvac");
		if(laststatus!=null) statustext.setText(laststatus);
		if(lastuberstatus!=null) uberstatus.setText(lastuberstatus);
		if(data!=null) {
			plot.setData(data);
			plot.setAutoRangeX();
			plot.setAutoGridX();
			plot.setAutoRangeY1();
			plot.setAutoRangeY2();
			plot.setAutoGridY1();
			plot.setAutoGridY2();
		}
	}
    private static String laststatus;
	private void set_statustext(String l) {
		laststatus=l;
		statustext.setText(l);
	}
	private static String lastuberstatus;
	private void set_uberstatus(String l) {
		lastuberstatus=l;
		uberstatus.setText(l);
	}


/* Die Funktion Load setzt nur den Filenamen, sonst nix.*/

	private void do_load(final String filename) {
		misloaded=true;
		setTitle(filename);
		Log.d(TAG,"Program loaded: "+filename);
	}
    private static int mProgressStatus = 0;
	public static void updateProgress(int i) {
		mProgressStatus=i;
		h.post(new Runnable() {
			public void run() {
				progress.setProgress(mProgressStatus);

			}
		});
	}


	/*   ######  MENU ######## */

	@Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.menu, menu);
	        return true;
	    }
	@Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	    	Intent viewIntent;
	        switch (item.getItemId()) {
	            case R.id.options_about:
	            	viewIntent = new Intent(MainActivity.this, InfoActivity.class);
	            	startActivity(viewIntent);
	            	break;
	            case R.id.options_settings:
	            	startActivity(new Intent(this, PreferencesActivity.class));
	            	break;
	            case R.id.load:
					loadFileList();
					showDialog(10+dialogi++);
					break;
	            case R.id.run:
	            	new Thread(new Runnable() {
						  	public void run() { do_run(); }}).start();
					break;
				case R.id.editor:
					call_editor();
					break;
				case R.id.component_editor:
					call_component_editor();
					break;
				case R.id.leitwerte:
					call_leitwerte_calculator();
					break;
				case R.id.newnew:
					// X11basicView.New();
					mChosenFile="new.vac";
					set_uberstatus("-new-");
					ubertext.setText(R.string.message_new);
					set_statustext("-new-");
					data.clear();
					plot.setData(data);
					setTitle(mChosenFile);
					toaster("New.");
					misloaded=true;
					break;
				case R.id.vdl_options_statistics:
					Dialog dialog = Tools.scrollableDialog(this,getResources().getString(R.string.menu_statistics),
					"<h4>Errors:</h4>"+last_errors+"<h4>Warnings:</h4>"+last_warnings+
							"<h4>Infos:</h4>"+last_infos+"<h4>Data:</h4>"+data.get_statistics());
					dialog.show();
					return true;
				case R.id.vdl_options_plottool:
					startActivity(new Intent(this, PlottoolActivity.class));
					return true;
				case R.id.vdl_options_tabletool:
					startActivity(new Intent(this, TabletoolActivity.class));
					return true;
	           case R.id.options_finish:
	        	   finish();
	        	   break;
				case R.id.help:
					dialog = Tools.webDialog(this,getResources().getString(R.string.menu_help),"");
					dialog.show();
					break;
	            default: 
	            	return super.onOptionsItemSelected(item);
	        }
	        return true;
	    }


	public void call_component_editor() {
		if(mChosenFile.toLowerCase().endsWith(".vac") || mChosenFile.toLowerCase().endsWith(".inf")) {
			VacFile.create_dummy_file(mChosenFile);
			startActivity(new Intent(this, ComponentEditorActivity.class));
		} else toaster("ERROR: Please load a .vac file first.");
	}
	public void call_leitwerte_calculator() {
		startActivity(new Intent(this, LeitwerteCalculatorActivity.class));
	}



	public void call_editor() {
		Log.d(TAG,"call editor with "+mChosenFile);
		
		Intent intent = new Intent(Intent.ACTION_EDIT);
		File dirdata=new File(Basestoragedirectory+"/"+"calcvac/");
		dirdata.mkdir();
		File f=new File(dirdata,mChosenFile);
		if(mChosenFile.endsWith(".vac")) {
			VacFile.create_dummy_file(mChosenFile);
		}
		Uri uri = Uri.fromFile(f);
		Log.d(TAG,uri.toString());
		intent.setDataAndType(uri, "text/plain");
		try {startActivityForResult(intent,1);}
		catch(ActivityNotFoundException e) {
			toaster(String.format(getResources().getString(R.string.error_editor), e.getMessage()));
			showDialog(9);
		}
		catch(SecurityException e) {
			toaster(String.format(getResources().getString(R.string.error_editor), e.getMessage()));
			showDialog(9);
		}
	}

/* Ein Vacfile spezifiert Konfigurationen fuer mehere Gas-Spezies. Es werden mehere .inf files generiert und
* vacline mehrmals darauf losgelassen. */

	private int do_run_vacfile() {
		String pfilename=Basestoragedirectory.getAbsolutePath()+"/calcvac/"+mChosenFile;
		h.post(new Runnable() {
			public void run() {
				ubertext.setText("processing "+mChosenFile+" ...");
				progress.setProgress(0);
			}
		});
		VacFile vacfile=new VacFile(mChosenFile);

		/* Hier wissen wir nun, welche Spezies berechnet werden sollen.*/

		int mol;
		int ret=0;
		if(VacFile.mols_of_interest.size()>0) {
			for (int cnt = 0; cnt < VacFile.mols_of_interest.size(); cnt++) {
				mol = VacFile.mols_of_interest.get(cnt);
				if (mol > 0) {
					Log.d(TAG, "Spezies: " + mol);
					ret = vacfile.vac2inf(mol);     /* Das inffile heisst jetzt mChosenFile+".28.inf" */
					if (ret == 0) {
						Log.d(TAG, "Progress: " + cnt + "/" + VacFile.mols_of_interest.size());
						ret = do_run_inffile(mChosenFile + "." + mol + ".inf", cnt-1, VacFile.mols_of_interest.size()-1);

						if (ret < 0) {
							h_toaster(getResources().getString(R.string.error_compilation));
							break;
						}
					} else {
						h_toaster(getResources().getString(R.string.error_compilation));
						break;
					}
				}
			}
		}
		return(ret);
	}
	private int do_run_inffile(final String ChosenFile, final int fileidx, final int anzfiles) {
		String pfilename=Basestoragedirectory.getAbsolutePath()+"/calcvac/"+ChosenFile;
		String twissfilename = ChosenFile + ".twiss";
		final String datfilename=Basestoragedirectory.getAbsolutePath()+"/calcvac/"+ChosenFile+".dat";
		twissfilename =Basestoragedirectory.getAbsolutePath()+"/calcvac/"+ twissfilename;
		h.post(new Runnable() {
			public void run() {
				ubertext.setText("processing "+ChosenFile+" ...");
				progress.setProgress(0);
			}
		});
		int ret=do_calcvac(pfilename,datfilename, twissfilename);
		if(ret==0) {

			h.post(new Runnable() {
				public void run() {
					progress.setProgress(50+(int)(50*fileidx/anzfiles));
				}
			});
			if(fileidx==0) {
				data.clear();
				final int anztwiss=data.loadtwissfile(twissfilename);
				h.post(new Runnable() {
					public void run() {
						progress.setProgress(55+(int)(50*fileidx/anzfiles));
					}
				});
			}
			final int anzdata=data.loaddatafile(datfilename);
			if(fileidx>=anzfiles-1) {
				if(anzfiles>1) {
					data.calculate_total_pressure();
					data.calculate_total_flow();
					data.calc_pminmax();
					data.calc_qminmax();
					data.arrange_colors();
				}
				h.post(new Runnable() {
					public void run() {
						set_uberstatus("" + data.lattice.size() + " lattice components and " +
								anzdata + " data points. "+
						String.format("P=[%.5g:%.5g] mbar Q=[%.5g:%.5g] mbar l/s.",data.pmin,data.pmax,data.qmin,data.qmax));
						plot.setData(data);
						progress.setProgress(100);
						plot.setAutoRangeX();
						plot.setAutoRangeY1();
						plot.setAutoRangeY2();
						plot.setAutoGridX();
						plot.setAutoGridY1();
						plot.setAutoGridY2();

						plot.invalidate();
						}
				});
			}
		}
		else if(ret==-1) h_toaster(getResources().getString(R.string.message_nothing_to_do));
		else h_toaster(getResources().getString(R.string.error_compilation));
		return(ret);
	}


	/* Diese Funktion interpretert das vorher ausgewählte file und produziert die
	* Druckverlaufaten und das twiss-file.*/

	private void do_run() {
		/*Teste, ob ein Programm geladen wurde, wenn nicht --> Toast
		 * generiere outputfilenamen aus input
		 * rufe native excecutable vacline auf. */
		if(misloaded) {

			/* Unterscheide zwischen .vac und .inf */
			if(mChosenFile.toLowerCase().endsWith(".vac")) do_run_vacfile();
			else do_run_inffile(mChosenFile,0,1);

		} else h_toaster(getResources().getString(R.string.message_nothing_to_do2));
	}

	int do_calcvac(String filename, String ofilename, String tfilename) {
        last_warnings="";
        last_errors="";
        last_infos="";
		final String a=run_vacline("-r "+resolution+" -n "+nplot+" -o "+ofilename+" --twiss "+tfilename+" "+filename);
		Log.d(TAG,"result "+a);
		/* Fehlermeldungen analysieren und als Alert darstellen.*/
		String[] sep=a.split("\n");
		int i;
		for(i=0;i<sep.length;i++) {
		    if(sep[i].startsWith("ERROR:")) last_errors=last_errors+sep[i]+"<br/>";
		    else if(sep[i].startsWith("WARNING:")) last_warnings=last_warnings+sep[i]+"<br/>";
		    else if(sep[i].startsWith("INFO:")) last_infos=last_infos+sep[i]+"<br/>";
        }

        if(last_errors.length()>0) {
            h.post(new Runnable() {
                public void run() {
                    showDialog(1);
                }
            });
        } else if (last_warnings.length()>0) {
            h.post(new Runnable() {
                public void run() {
                    showDialog(2);
                }
            });
        }
		h.post(new Runnable() {
                           public void run() { if(!a.isEmpty()) set_statustext(a);
                           else set_statustext("<empty>"); }});
		return(0);
	}

        /* This method of running the vacline excecutable 
	 * works only up to  Android 10. (not yet tested). 
	 */
	 
	String run_vacline(String cmd) {
		String result;
		Log.d(TAG,"run vacline with <"+cmd+">");
		String binary="vacline."+Build.CPU_ABI;
		String filename=getFilesDir()+"/"+binary;
		Log.d(TAG,"binary path: "+filename);
		if(!(new File(filename).exists())) {
		  Log.d(TAG,"copy vacline binary to FilesDir. ");
			asset_bin_copy(binary,getFilesDir().getAbsolutePath());
			system("/system/bin/chmod 755 "+filename);
		}
		result=system(filename+" "+cmd);
		//result=system("ls -l "+filename+" "+cmd);
		Log.d(TAG,"result "+result);
		return result;
	}


/*Copy the vacline binary from assets to internal data folder*/
	private void asset_bin_copy(final String source, final String dest) {
		AssetManager assetManager = getAssets();
		InputStream in = null;
		OutputStream out = null;
		File destination;
		try {
			destination=new File(dest+"/" + source);
			if(destination.exists()) {
				Log.d(TAG,"File "+destination+" already there. will not copy.");
			} else {
				in = assetManager.open("calcvac/vacline/"+source);
				out = new FileOutputStream(destination);
				Log.d(TAG,"copy asset file: "+"calcvac/vacline/"+source+" --> "+destination);
				Tools.copyFile(in, out);
				in.close();
				out.flush();
				out.close();
			}
		} catch(Exception e) { Log.e(TAG, e.toString()+e.getMessage()); }
	}

	String system(final String cmd) {
		Log.d(TAG,"System: "+cmd);
		try {
			// Executes the command. E.g. "/system/bin/ls /sdcard"
			Process process = Runtime.getRuntime().exec(cmd);
			// Reads stdout.
			// NOTE: You can write to stdin of the command using
			//       process.getOutputStream().
			BufferedReader reader = new BufferedReader(
				new InputStreamReader(process.getInputStream()));
			int read;
			char[] buffer = new char[4096];
			StringBuffer output = new StringBuffer();
			while ((read = reader.read(buffer)) > 0) {
				output.append(buffer, 0, read);
			}
			reader.close();
			// Waits for the command to finish.
			process.waitFor();
			return output.toString();
		} catch(IOException e) {
			throw new RuntimeException(e);
		} catch(InterruptedException e) {
			throw new RuntimeException(e);
		}
	}


	public class Item {
		public String text;
		public int icon;
		Item(String text, Integer icon) {
			this.text = text;
			this.icon = icon;
		}
		@Override
		public String toString() {
			return text;
		}
	}

	/* Macht items aus Fileliste */
	void make_items() {
		if(mFileList != null) {
			items = new Item[mFileList.length];
			for (int i = 0; i < mFileList.length; i++) {
				items[i] = new Item(mFileList[i], 0);
				if (mFileList[i].endsWith(".inf"))
					items[i] = new Item(mFileList[i], android.R.drawable.ic_menu_slideshow);
				else if (mFileList[i].endsWith(".vac"))
					items[i] = new Item(mFileList[i], android.R.drawable.ic_menu_view);
				else items[i] = new Item(mFileList[i], 0); /*no icon.*/
			}
		}
	}


	@Override
	protected Dialog onCreateDialog(final int id) {
		Dialog dialog = null;
		if(id==0) { /* Version info */
			dialog = Tools.scrollableDialog(this, "New in this version:", "");
		} else if(id==1) { /* Error Message*/
			dialog = Tools.scrollableDialog(this, "ERRORs", "");
		} else if(id==2) { /* Error Message*/
			dialog = Tools.scrollableDialog(this, "WARNINGs", "");
		} else if(id==3) {   /*The delete file dialog*/
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(getResources().getString(R.string.message_really_delete));
			builder.setMessage(String.format(getResources().getString(R.string.message_delete), mSelectedFile));
			builder.setPositiveButton(getResources().getString(R.string.word_proceed), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					File f=new File(Basestoragedirectory+"/calcvac/"+mSelectedFile);
					f.delete();
				} });
			builder.setNeutralButton(getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					;
				} });
			dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);

		} else if(id==4) { /* Dialog Filemenu*/
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(mSelectedFile);
			builder.setIcon(R.drawable.pumpe);
			final String[] mitem={"LOAD","LOAD+RUN","LOAD+edit","delete","Cancel"};
			builder.setItems(mitem, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which){
					if(mitem[which].startsWith("LOAD")) {
						mChosenFile = mSelectedFile;
						do_load(mChosenFile);
					} else if(mitem[which].equalsIgnoreCase("delete")) { /*delete*/
						showDialog(3);
						mad.dismiss();
					}
				    if(mitem[which].endsWith("edit"))  call_editor();
					else if(mitem[which].endsWith("RUN")) {mad.dismiss(); do_run();}
				}});
			dialog = builder.show();
		} else if(id==7) { /*Dialog, SD Card nicht verfügbar.*/
			dialog=Tools.scrollableDialog(this,"ERROR",MessageFormat.format(getResources().getString(R.string.sdcard_notb),Basestoragedirectory));
		} else if(id==9) {/* Dialog Fehlermeldung kein Editor */
			dialog=Tools.scrollableDialog(this,"ERROR",getResources().getString(R.string.editor_notfound));
		} else  {  /* Dialog zum File auswählen fuer Programm laden...*/
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(getResources().getString(R.string.message_select_file_load));
			builder.setIcon(R.drawable.pumpe);
			if(mFileList == null){
				dialog = builder.create();
				return dialog;
			}
			make_items();
			ListAdapter adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item,android.R.id.text1,items){
				public View getView(int position, View convertView, ViewGroup parent) {
					//User super class to create the View
					View v = super.getView(position, convertView, parent);
					TextView tv = v.findViewById(android.R.id.text1);

					//Put the image on the TextView
					tv.setCompoundDrawablesWithIntrinsicBounds(MainActivity.items[position].icon, 0, 0, 0);

					//Add margin between image and text (support various screen densities)
					int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
					tv.setCompoundDrawablePadding(dp5);
					return v;
				}
			};

			builder.setAdapter(adapter,  new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which){
					mChosenFile = mFileList[which];
					do_load(mChosenFile);
				}});
			mad = builder.create(); //don't show dialog yet
			mad.setOnShowListener(new OnShowListener() {
				@Override
				public void onShow(DialogInterface dialog) {
					ListView lv = mad.getListView(); //this is a ListView with your "buds" in it
					lv.setOnItemLongClickListener(new OnItemLongClickListener() {
						@Override
						public boolean onItemLongClick(AdapterView<?> parent, View view, int which, long id) {
							Log.d("Long Click!","List Item #"+which+" was long clicked");
							mSelectedFile = mFileList[which];
							showDialog(4);
							return true;
						}
					});
				}
			});
			dialog = mad;
		}
		return dialog;
	}


	/* Wird aufgerufen, jedesmal befor der Dialog angezeigt wird.*/
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);
		TextView tV;
		switch(id) {
			case 0:
				tV=  dialog.findViewById(R.id.TextView01);
				tV.setText(Html.fromHtml(Help.getreleasenotes(getAssets())));
				break;
			case 1:
			case 2:
				tV= dialog.findViewById(R.id.TextView01);
				if(id==1) tV.setText(Html.fromHtml(last_errors));
				else tV.setText(Html.fromHtml(last_warnings));
				break;
			case 3:
				((AlertDialog) dialog).setMessage(String.format(getResources().getString(R.string.message_delete), mSelectedFile));
				break;
			case 4:
				dialog.setTitle(mSelectedFile);
				break;
			case 10:
				break;
			default:
				break;
		}
	}

    private void loadFileList() {
   	File dirdata=new File(Basestoragedirectory+"/calcvac");
   	dirdata.mkdir();
   	if(dirdata.exists()){
   	     	FilenameFilter filter = new FilenameFilter(){
   	     		public boolean accept(File dir, String filename){
   	     			File sel = new File(dir, filename);
   	     			return (filename.endsWith(".vac") || filename.endsWith(".inf")) && !sel.isDirectory();
   	     		}
   	     	};
   	     	mFileList = dirdata.list(filter);
   	     	Arrays.sort(mFileList, new Comparator<String>() {
   	     		@Override
   	     		public int compare(String entry1, String entry2) {
   	     			return entry1.compareToIgnoreCase(entry2);
   	     		}
   	     	});
   	} else {
   	     	Log.d(TAG,"Path not found! "+dirdata);
   	     	mFileList= new String[0];
   	}
    }



	/*Copy the example programs from assets to bas folder*/
	private void asset_copy(final String path) {
		File dirdata=new File(Basestoragedirectory+"/"+path);
		dirdata.mkdir();
		File destination;

		AssetManager assetManager = getAssets();
		String[] files = null;
		try {
			files = assetManager.list(path);
		} catch (IOException e) { Log.e(TAG, e.getMessage());}

		for(String filename : files) {
			InputStream in = null;
			OutputStream out = null;
			try {
				destination=new File(dirdata+"/" + filename);
				if(destination.exists()) {
					Log.d(TAG,"File "+destination+" already there. will not copy.");
				} else {
					in = assetManager.open(path+"/"+filename);
					out = new FileOutputStream(dirdata+"/" + filename);

					Log.d(TAG,"copy file: "+filename+" -->"+dirdata+"/");
					Tools.copyFile(in, out);
					in.close();
					out.flush();
					out.close();
				}
			} catch(Exception e) {
				Log.e(TAG, e.toString()+e.getMessage());
			}
		}
	}

   public void toaster(final String message) {
       Log.d(TAG,message);
       Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
   }
   void h_toaster(final String message) {
   	   h.post(new Runnable() {
   		   public void run() {
   			   toaster(message);
   		   }
   	   });
   }
}
