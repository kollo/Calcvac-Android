package de.drhoffmannsoftware.calcvac;

/* CalibrationActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

public class CalibrationActivity extends Activity {
	private CheckBox histograms,grid,label,links,lattice;
	private CheckBox fixscale1,fixscale2,fixscale3;
	private EditText minscale1, maxscale1, stepscale1;
	private EditText minscale2, maxscale2, stepscale2;
	private RadioButton bt1,bt2,bt3,bt5;
	private SharedPreferences settings;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calibration);
		ActionBar actionBar = getActionBar();
		//  actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.plot_clibration_settings);

		histograms=findViewById(R.id.checkBox_histogram);
		grid=findViewById(R.id.checkBox_grid);
		label=findViewById(R.id.checkBox_label);
		links= findViewById(R.id.checkBox_links);
		lattice= findViewById(R.id.checkBox_lattice);

		fixscale1=findViewById(R.id.fixscale1);
		fixscale2=findViewById(R.id.fixscale2);
		fixscale3=findViewById(R.id.fixscale3);
		bt1=findViewById(R.id.bt1);
		bt2=findViewById(R.id.bt2);
		bt3=findViewById(R.id.bt3);
		bt5=findViewById(R.id.bt5);
		minscale1=findViewById(R.id.minscale1);
		minscale2=findViewById(R.id.minscale2);
		maxscale1=findViewById(R.id.maxscale1);
		maxscale2=findViewById(R.id.maxscale2);
		stepscale1=findViewById(R.id.stepscale1);
		stepscale2=findViewById(R.id.stepscale2);
		settings =PreferenceManager.getDefaultSharedPreferences(getBaseContext());
	}
	@Override
	public void onResume() {
		super.onResume();
		histograms.setChecked(settings.getBoolean("histograms", false));
		grid.setChecked(settings.getBoolean("grid", true));
		label.setChecked(settings.getBoolean("label", false));
		links.setChecked(settings.getBoolean("links", true));
		lattice.setChecked(settings.getBoolean("lattice", true));

		fixscale1.setChecked(settings.getBoolean("fixscale1", false));
		fixscale2.setChecked(settings.getBoolean("fixscale2", false));
		fixscale3.setChecked(settings.getBoolean("fixscale3", false));
		bt1.setChecked(settings.getInt("bluecurvetype", 1)==1);
		bt2.setChecked(settings.getInt("bluecurvetype", 1)==2);
		bt3.setChecked(settings.getInt("bluecurvetype", 1)==3);

		bt5.setChecked(settings.getInt("bluecurvetype", 1)==5);
		minscale1.setText(""+settings.getFloat("minscale1", (float)0.0));
		minscale2.setText(""+settings.getFloat("minscale2", (float)0.0));
		maxscale1.setText(""+settings.getFloat("maxscale1", (float)100.0));
		maxscale2.setText(""+settings.getFloat("maxscale2", (float)100.0));
		stepscale1.setText(""+settings.getFloat("stepscale1", (float)1.0));
		stepscale2.setText(""+settings.getFloat("stepscale2", (float)1.0));
	}
	@Override
	protected void onPause(){
		int bluecurvetype=0;
		super.onPause();

		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("histograms", histograms.isChecked());
		editor.putBoolean("grid", grid.isChecked());
		editor.putBoolean("label", label.isChecked());
		editor.putBoolean("links", links.isChecked());
		editor.putBoolean("lattice", lattice.isChecked());

		editor.putBoolean("fixscale1", fixscale1.isChecked());
		editor.putBoolean("fixscale2", fixscale2.isChecked());
		editor.putBoolean("fixscale3", fixscale3.isChecked());

		if(bt1.isChecked()) bluecurvetype=1;
		else if(bt2.isChecked()) bluecurvetype=2;
		else if(bt3.isChecked()) bluecurvetype=3;
		else if(bt5.isChecked()) bluecurvetype=5;

		editor.putInt("bluecurvetype",bluecurvetype );


		editor.putFloat("minscale1",(float)Double.parseDouble(minscale1.getText().toString()));
		editor.putFloat("maxscale1",(float)Double.parseDouble(maxscale1.getText().toString()));
		editor.putFloat("stepscale1",(float)Double.parseDouble(stepscale1.getText().toString()));

		editor.putFloat("minscale2",(float)Double.parseDouble(minscale2.getText().toString()));
		editor.putFloat("maxscale2",(float)Double.parseDouble(maxscale2.getText().toString()));
		editor.putFloat("stepscale2",(float)Double.parseDouble(stepscale2.getText().toString()));

		// Commit the edits!
		editor.apply();
	}
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed
			// in the Action Bar.
			finish();
			return true;
		default: 
			return super.onOptionsItemSelected(item);
		}
	}
}
