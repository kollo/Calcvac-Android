package de.drhoffmannsoftware.calcvac;

/* PumpEditorActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.ArrayList;

public class PumpEditorActivity extends Activity {
    private static final String TAG = PumpEditorActivity.class.getSimpleName();
    public static VacFile vacfile;
    TableLayout mtable,legende;
    ScaleGestureDetector mscalegd;
    String mSelectedElement;
    View resetview=null;
    ArrayList<Integer> mol_interest;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spectrum);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.pump_editor);
        }
        legende= findViewById(R.id.legende);
        mtable=findViewById(R.id.spectable);
        mtable.setFocusable(true); //necessary for getting the touch events
        mtable.setFocusableInTouchMode(true);
        mtable.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if(!mscalegd.isInProgress()) mscalegd.onTouchEvent(event);
                return true;
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        vacfile= new VacFile(MainActivity.mChosenFile);
        updateTable();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.pumpeditmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed
                // in the Action Bar.
                finish();
                return true;
            case R.id.pe_options_help:
                showDialog(9);
                return true;
            case R.id.addpump:
                mSelectedElement="new_"+vacfile.pumps.size();
                showDialog(1);
                return true;
            case R.id.addpredefined:
                showDialog(8);
                return true;
            case R.id.selectmol:
                startActivity(new Intent(getApplicationContext(), MolChooserActivity.class));
                return true;
        }
        return true;
    }

    public class Item {
        public String text;
        public int icon;
        Item(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }
        @Override
        public String toString() {
            return text;
        }
    }

    EditText pump_name,pump_default;
    TableLayout pump_table;
    static Integer[] tab;
    static Item[] items;

    @Override
    protected Dialog onCreateDialog(final int id) {
        Dialog dialog;
        if(id==1) {
            dialog = new Dialog(this);
            dialog.setTitle(R.string.create_pump);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(R.layout.pumpdialog);
            pump_name= dialog.findViewById(R.id.pump_name);
            pump_default= dialog.findViewById(R.id.pump_default);
            pump_table= dialog.findViewById(R.id.pump_table);

            Button add_species=dialog.findViewById(R.id.Button_add_species);
            add_species.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    //  Auswahl der Spezies
                    showDialog(7);
                }
            });
        } else if(id==2) {  /*Auswahl Edit, delete, Cancel*/
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(mSelectedElement);
            builder.setIcon(R.drawable.pumpe);
            final String[] mitem={"edit","delete","Cancel"};
            builder.setItems(mitem, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    Log.d(TAG,"Item #"+which+"was clicked");
                    if(which==0) {
                        showDialog(1);
                    } else if(mitem[which].equalsIgnoreCase("delete")) { /*delete*/
                        showDialog(6);  /*Confirm delete element*/
                    } else if(which==2) {
                        if(resetview!=null) resetview.setBackgroundColor(Color.BLACK);
                    }
                }});
            dialog = builder.show();
            dialog.setCanceledOnTouchOutside(false);

        }  else if(id==6) {/* Confirm delete Element Dialog */
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.message_really_delete));
            builder.setMessage(String.format(getResources().getString(R.string.message_delete_elem), mSelectedElement));
            builder.setPositiveButton(getResources().getString(R.string.word_proceed), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    vacfile.delete_pump(mSelectedElement);
                    vacfile.reload();
                    updateTable();
                    mtable.invalidate();
                } });
            builder.setNeutralButton(getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if(resetview!=null) resetview.setBackgroundColor(Color.BLACK);
                } });

            dialog = builder.create();
            dialog.setCanceledOnTouchOutside(false);
        }  else if(id==7) {/* Gas AUswahl */
            int i;
            int cnt=0;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.select_gas);
            builder.setIcon(R.drawable.gas);
            for(i=0;i<Gas.moles.length;i++) {
                if(!Gas.moles[i].isEmpty()) cnt++;
            }

            tab=new Integer[cnt];
            items = new Item[cnt];
            cnt=0;
            for(i=0;i<Gas.moles.length;i++) {
                if(!Gas.moles[i].isEmpty()) {
                    tab[cnt]=i;
                    items[cnt++]=new Item(Gas.moles[i],R.drawable.atom);
                }
            }
            ListAdapter adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item,android.R.id.text1,items){
				public View getView(int position, View convertView, ViewGroup parent) {
					//User super class to create the View
					View v = super.getView(position, convertView, parent);
					TextView tv = v.findViewById(android.R.id.text1);

					//Put the image on the TextView
					tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

					//Add margin between image and text (support various screen densities)
					int dp5 = pix(5);
					tv.setCompoundDrawablePadding(dp5);
					return v;
				}
			};

            builder.setAdapter(adapter,  new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which){
					// (Gas.moles[tab[which]]);
                    TableRow row=give_row(tab[which],"",2);
                    pump_table.addView(row);
                }});
            dialog= builder.create();
        }  else if(id==9) {
             dialog = Tools.scrollableDialog(this,"Info",getResources().getString(R.string.pumpedithelpdialog));
        } else  {
            dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.description)+
                    getResources().getString(R.string.impressum));
        }
        return dialog;
    }

    TableRow give_row(int mol, String value, int typ) {
       TableRow row;
       TextView text;
       EditText et;
       row=new TableRow(this);
       text = new TextView(this);
       text.setText(""+mol+": ");
       text.setTextSize(pix(16));
       if(typ==0) text.setTextColor(Color.YELLOW);
       else  if(typ==1) text.setTextColor(Color.CYAN);
       else  if(typ==2) text.setTextColor(Color.MAGENTA);

       row.addView(text);
       et=new EditText(this);
       et.setHint("[l/s/m] or factor");
       et.setText(value);
       row.addView(et);
       row.setEnabled(true);
       return row;
    }


    /* Wird aufgerufen, jedesmal bevor der Dialog angezeigt wird.*/
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        if(id==1) {// Set up the input
            if(mSelectedElement.startsWith("new_")) dialog.setTitle(R.string.create_pump);
            else dialog.setTitle(R.string.edit_pump);
            Button buttonOK = dialog.findViewById(R.id.Button_OKapply);
            Button buttonCANCEL = dialog.findViewById(R.id.Button_cancel);
            if(mSelectedElement.startsWith("new_")) buttonOK.setText(R.string.word_add);
            else buttonOK.setText(R.string.word_apply);
            pump_name.setText(mSelectedElement);
            pump_table.removeAllViews();
            int i,j;
            TableRow row;
            int idx=vacfile.find_pump(mSelectedElement);
            if(idx<0) {
                pump_default.setText("0.0");
                /* nur mols of interest spezies.*/
                for(i=0;i<VacFile.mols_of_interest.size();i++) {
                    j=VacFile.mols_of_interest.get(i);
                    if(j>0) {
                        pump_table.addView(give_row(j, "", 1));
                    }
                }
            } else {
                pump_default.setText(""+vacfile.pumps.get(idx).s_default);

                int k;
                boolean flag;

                /*Erst die Gas Sorten aus der Pumpendefinition ....*/
                for(i=0;i<vacfile.pumps.get(idx).spec.size();i++) {
                    row=give_row(vacfile.pumps.get(idx).spec.get(i).mol,""+vacfile.pumps.get(idx).spec.get(i).value,0);
                    pump_table.addView(row);
                }
                /* ... dann ergaenzen mit evtl. mols of interest spezies.*/
                for(i=0;i<VacFile.mols_of_interest.size();i++) {
                    j=VacFile.mols_of_interest.get(i);
                    flag=false;
                    for(k=0;k<vacfile.pumps.get(idx).spec.size();k++) {

                        if(j==vacfile.pumps.get(idx).spec.get(k).mol) {flag=true;break;}
                    }
                    if(!flag && j>0) {
                        row = give_row(j, "",1);
                        pump_table.addView(row);
                    }
                }
            }
            buttonCANCEL.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dismissDialog(1);
                    if(resetview!=null) resetview.setBackgroundColor(Color.BLACK);
                }
            });
            buttonOK.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Spectrum spec=new Spectrum();
                    spec.name=pump_name.getText().toString();
                    try {spec.s_default= Double.parseDouble(pump_default.getText().toString());} catch(NumberFormatException nfe) {spec.s_default=0;}
                    Log.d(TAG,"Childs: "+pump_table.getChildCount());
                    if(pump_table.getChildCount()>0) {
                        TableRow row;
                        double value;
                        int mol;
                        int i;
                        for (i = 0; i < pump_table.getChildCount(); i++) {
                            row=(TableRow)pump_table.getChildAt(i);
                            TextView tmol=(TextView)row.getChildAt(0);
                            EditText tvalue=(EditText)row.getChildAt(1);
                            String t=tvalue.getText().toString();
                            if(!t.isEmpty()) {
                                try {
                                    value = Double.parseDouble(t);
                                } catch (NumberFormatException nfe) {
                                    value = 0;
                                }
                                t = tmol.getText().toString();
                                String a[] = t.split(":", 2);
                                mol = Integer.parseInt(a[0]);
                                spec.add_spec(mol, value);
                            }
                        }
                    }

                    vacfile.set_pump(spec);
                    dismissDialog(1);
                    vacfile.reload();
                    updateTable();
                    mtable.invalidate();

                }
            });
        } else if(id==2) {// Set up the input
            ((AlertDialog)dialog).setTitle(mSelectedElement);
        } else if(id==6) {// Set up the input
            ((AlertDialog)dialog).setMessage(String.format(getResources().getString(R.string.message_delete_elem), mSelectedElement));
        }
    }


    private void updateTable() {
        TableRow.LayoutParams params0 = new TableRow.LayoutParams(pix(100), pix(50));
        TableRow.LayoutParams params1 = new TableRow.LayoutParams(pix(100), pix(50));
        TableRow.LayoutParams params2 = new TableRow.LayoutParams(pix(100), pix(50));
        if(mtable!=null) mtable.removeAllViews();
        TableRow row= new TableRow(this);
        TextView text;
        resetview=null;
        legende.removeAllViews();
        /* Mache Legende */
        for (int j = 0; j < 1+2; j++) {
            text = new TextView(this);
           if(j==0) {
                    text.setLayoutParams(params0);
                    text.setText(R.string.word_pump);
                } else if(j==1)  {
                    text.setLayoutParams(params1);
                    text.setText(R.string.word_default);
                } else  {
                    text.setLayoutParams(params2);
                    text.setText("m: pump speed");
                }
                row.addView(text);
        }
        legende.addView(row);
        row= new TableRow(this);
         for (int j = 0; j < 1+2; j++) {
            text = new TextView(this);
           if(j==0) {
                    text.setLayoutParams(params0);
                    text.setText(R.string.word_name);
                } else if(j==1)  {
                    text.setLayoutParams(params1);
                    text.setText("[l/s/m]");
                } else  {
                    text.setLayoutParams(params2);
                    text.setText("[l/s/m]");
                }
                row.addView(text);
        }
        legende.addView(row);

        for (int i = 0; i < vacfile.pumps.size(); i++) {
            row = new TableRow(this);
            for (int j = 0; j < vacfile.pumps.get(i).spec.size()+2; j++) {
                text = new TextView(this);
                text.setTextColor(Color.YELLOW);
                if(j==0) {
                    text.setLayoutParams(params0);
                    text.setText(vacfile.pumps.get(i).name);
                } else if(j==1)  {
                    text.setLayoutParams(params1);
                    text.setText(""+vacfile.pumps.get(i).s_default);
                } else if(j-2<vacfile.pumps.get(i).spec.size()) {
                    text.setLayoutParams(params2);
                    text.setText(""+vacfile.pumps.get(i).spec.get(j-2).mol+": "+vacfile.pumps.get(i).spec.get(j-2).value);
                }
                text.setLongClickable(true);
                text.setEnabled(true);
                text.setOnLongClickListener(new View.OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        // v.setBackgroundColor(Color.GRAY);
                        TableRow tablerow = (TableRow)v.getParent();
                        tablerow.setBackgroundColor(Color.BLUE);
                        resetview=tablerow;
                        TextView sample = (TextView) tablerow.getChildAt(0);
                        mSelectedElement=sample.getText().toString();
                        showDialog(2);
                        return false;
                    }
                });
                text.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // v.setBackgroundColor(Color.GRAY);
                        TableRow tablerow = (TableRow)v.getParent();
                        tablerow.setBackgroundColor(Color.BLUE);
                        resetview=tablerow;
                        TextView sample = (TextView) tablerow.getChildAt(0);
                        mSelectedElement=sample.getText().toString();
                        showDialog(1);
                    }
                });
                row.addView(text);
            }
            row.setLongClickable(true);
            row.setEnabled(true);
            row.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View v) {
                    TableRow tablerow = (TableRow)v;
                        tablerow.setBackgroundColor(Color.BLUE);
                        resetview=tablerow;
                        TextView sample = (TextView) tablerow.getChildAt(0);
                        mSelectedElement=sample.getText().toString();
                    showDialog(2);
                    return false;
                }
            });
            row.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TableRow tablerow = (TableRow)v;
                    tablerow.setBackgroundColor(Color.BLUE);
                    resetview=tablerow;
                    TextView sample = (TextView) tablerow.getChildAt(0);
                    mSelectedElement=sample.getText().toString();
                    showDialog(1);
                }
            });
            if(mtable!=null) mtable.addView(row);
        }
    }

    private int pix(int dp) {
        return (int) (dp * getResources().getDisplayMetrics().density + 0.5f);
    }
}
