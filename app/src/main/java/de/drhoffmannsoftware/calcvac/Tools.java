package de.drhoffmannsoftware.calcvac;

/* Tools.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Tools {
	private static final String TAG = Tools.class.getSimpleName(); 

	public static void sendEmail(Context context,String recipient, String subject, String message, String filename) {
		try {
			final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
			emailIntent.setType("plain/text");
			if (recipient != null)  emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{recipient});
			if (subject != null)    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			if (message != null)    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
			if(filename!=null) { 
				File f=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/"+filename);
				// f.setReadable(true, false);
				if(f.exists()) emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
				else Log.d(TAG,"File not found:"+f.getAbsolutePath());
			}
			context.startActivity(Intent.createChooser(emailIntent, "Send data as mail..."));
		} catch (ActivityNotFoundException e) {
			// cannot send email for some reason
			Toast.makeText(context,"cannot send email for reason: "+e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	public static Dialog scrollableDialog(Context c,String title, String text) {
		final Dialog dialog = new Dialog(c);
		if(title.isEmpty()) dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		else dialog.setTitle(title);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.maindialog);
		
		TextView wV=  dialog.findViewById(R.id.TextView01);
		wV.setText(Html.fromHtml(text));
		//set up button
		Button button = dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		return dialog;
	}

	public static Dialog infoDialog(Context c,final String title, final String text) {
		final Dialog dialog = new Dialog(c);
		if(title==null || title.isEmpty()) dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		else dialog.setTitle(title);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.webdialog);
		TextView tV=  dialog.findViewById(R.id.TextView01);
		tV.setVisibility(View.GONE);
		TextView tV2=  dialog.findViewById(R.id.textView1);
		tV2.setVisibility(View.GONE);
		final WebView wV= dialog.findViewById(R.id.webView1);
		Log.d(TAG,"Text: len="+text.length());

		if(text.length()>0) wV.loadDataWithBaseURL(null,"<html><body>"+text+"</body></html>" , "text/html", "utf-8", null);
		else {

			String relnotes="<html><body>"+Help.getreleasenotes(c.getAssets())+"</body></html>";
			Log.d(TAG,"RelNotesText: len="+relnotes.length());
			wV.loadDataWithBaseURL(null,relnotes , "text/html", "utf-8", null);
			wV.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(final WebView view, final String url) {
                super.onPageFinished(view, url);
                //wV.invalidate();
                wV.requestLayout();
            }
        });
		}
		//set up button
		Button button = dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		EditText eT= dialog.findViewById(R.id.editText1);
		eT.setVisibility(View.GONE);
		return dialog;
	}


	public static Dialog webDialog(final Context c,final String title, final String text) {
		final Dialog dialog = new Dialog(c);
		if(title==null || title.equals("")) dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		else dialog.setTitle(title);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.webdialog);
		TextView tV= dialog.findViewById(R.id.TextView01);
		tV.setText(c.getResources().getString(R.string.word_searchcomment));
		final WebView wV= dialog.findViewById(R.id.webView1);
		//wV.setMinimumWidth(WindowManager.LayoutParams.);
		wV.setVisibility(View.GONE);
		if(text.length()>0) wV.loadDataWithBaseURL(null,"<html><body>"+text+"</body></html>" , "text/html", "utf-8", null);
		else wV.loadDataWithBaseURL(null,"<html><body>"+Help.getonlinehelp(c.getAssets(),"_")+"</body></html>" , "text/html", "utf-8", null);

		wV.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(final WebView view, final String url) {
                super.onPageFinished(view, url);
                wV.setVisibility(View.VISIBLE);
                //wV.requestLayout();

            }
        });

		//set up button
		Button button = dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		EditText eT=dialog.findViewById(R.id.editText1);
		//Set the last entered command
		eT.addTextChangedListener(new TextWatcher(){
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void afterTextChanged(Editable s) {
				if(s.length()>0) {
					wV.loadDataWithBaseURL(null,"<html><body>"+Help.getonlinehelp(c.getAssets(),s.toString())+"</body></html>" , "text/html", "utf-8", null);
				}
			}
		});
	return dialog;
    }

    public static void copyFile(final InputStream in, final OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1) {
    	    out.write(buffer, 0, read);
        }
    }
}
