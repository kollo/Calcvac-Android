package de.drhoffmannsoftware.calcvac;

/* LinkEditorActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class LinkEditorActivity extends Activity {
    private final static String TAG="LINKEDIT";
    ListView list;
    ArrayList<String> ht;
    ArrayAdapter<String> adapter;
    int mselect=-1;
    View resetview=null;
    ArrayList<Link> links;
    Link actual_link=null;
    int link_hint=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.linkcomposer);

        ActionBar actionBar = getActionBar();
        //  actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.link_editor);

        list=(ListView) findViewById(R.id.linkliste);
        list.setTextFilterEnabled(true);

        ht=new ArrayList();
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, ht);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                resetview=view;
                view.setBackgroundColor(Color.BLUE);
                mselect=(int)id;
                actual_link=links.get(mselect);
                showDialog(5);

            }
        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                resetview=view;
                view.setBackgroundColor(Color.BLUE);
                mselect=(int)id;
                showDialog(0);
                return true;
            }
        });
    }
    @Override
	public void onStart() {
		super.onStart();
        VacFile vac=ComponentEditorActivity.vacfile;
        links=vac.links;
        liste_setup();
    }


    void liste_del(int idx) {
        links.remove(idx);
        links_writeout();
        liste_setup();
    }

    void liste_add(Link l) {
        links.add(l);
        links_writeout();
        liste_setup();
    }
    void liste_insert(int idx,Link l) {
        if(idx>=0) links.remove(idx);
        links.add(l);
        links_writeout();
        liste_setup();
    }
    void links_writeout() {
        VacFile vac=ComponentEditorActivity.vacfile;
        vac.links=links;
        vac.write_links();
    }


    void liste_setup() {
        adapter.clear();
        int i;
        if(links.size()>0) {
            for(i=0;i<links.size();i++) adapter.add(links.get(i).toString());
        }
        adapter.notifyDataSetChanged();
        if(resetview!=null) {
            resetview.setBackgroundColor(Color.BLACK);
            resetview=null;
        }
    }
    @Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.linkeditmenu, menu);
		return true;
	}
    @Override
	public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed
                // in the Action Bar.
                finish();
                return true;
            case R.id.le_options_help:
                showDialog(9);
                return true;
            case R.id.le_options_add:
                actual_link=new Link();
                mselect=-1;
                showDialog(5);
                return true;
            case R.id.le_options_quit:
                finish();
                return true;
        }
        return true;
    }
    public class Item {
        public String text;
        public int icon;
        Item(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }
        @Override
        public String toString() {
            return text;
        }
    }
     Button buttonA=null;
     Button buttonB=null;

    @Override
    protected Dialog onCreateDialog(final int id) {
        Dialog dialog = null;

        if (id == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(links.get(mselect).toString());
            builder.setIcon(R.drawable.pumpe);
            final String[] mitem={"delete","edit ...","Cancel"};
            builder.setItems(mitem, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch(which) {
                        case 0:  /* delete */
                            liste_del(mselect);
                            break;
                        case 1:
                            actual_link=links.get(mselect);
                            showDialog(5);
                            break;
                        default:
                            if(resetview!=null) {
                                resetview.setBackgroundColor(Color.BLACK);
                                resetview=null;
                            }
                    }
                }});
            dialog = builder.show();
            dialog.setCanceledOnTouchOutside(false);
        } else if(id==1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Select Vacuum lines:");
            builder.setIcon(R.drawable.pumpe);

            // Set up the input

            final VacFile vacfile=ComponentEditorActivity.vacfile;
            final Item[] items;
            items = new Item[vacfile.sections.size()];
            for(int i=0;i<vacfile.sections.size();i++) {
                items[i]=new Item(vacfile.sections.get(i).name,R.drawable.ic_menu_compose);
            }
            ListAdapter adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item,android.R.id.text1,items){
                public View getView(int position, View convertView, ViewGroup parent) {
                    //User super class to create the View
                    View v = super.getView(position, convertView, parent);
                    TextView tv = v.findViewById(android.R.id.text1);
                    if(vacfile.sections.get(position).typ==1) tv.setTextColor(Color.MAGENTA);
                    else if(vacfile.sections.get(position).typ==0) tv.setTextColor(Color.CYAN);
                    else if(vacfile.sections.get(position).typ==2) tv.setTextColor(Color.CYAN);
                    else if(vacfile.sections.get(position).typ==3) tv.setTextColor(Color.GREEN);
                    else if(vacfile.sections.get(position).typ==6) tv.setTextColor(Color.RED);
                    //Put the image on the TextView
                    tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                    //Add margin between image and text (support various screen densities)
                    int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                    tv.setCompoundDrawablePadding(dp5);
                    return v;
                }
            };

            builder.setAdapter(adapter,  new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    if(link_hint==1 && buttonA!=null) buttonA.setText(vacfile.sections.get(which).name);
                    else if(link_hint==2 && buttonB!=null) buttonB.setText(vacfile.sections.get(which).name);
                }});

            final AlertDialog mad;
            mad = builder.create(); //don't show dialog yet
            mad.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    ListView lv = mad.getListView(); //this is a ListView with your "buds" in it
                    lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(AdapterView<?> parent, View view, int which, long id) {
                            Log.d("Long Click!","List Item #"+which+" was long clicked");
                            // showDialog(2);
                            return true;
                        }
                    });
                }
            });
            dialog = mad;
        } else if(id==5) {
            dialog = new Dialog(this);
            dialog.setTitle("Create Link ...");
            dialog.setCancelable(true);
		    dialog.setCanceledOnTouchOutside(true);
		    dialog.setContentView(R.layout.linkdialog);
		    //set up buttons
		    buttonA = dialog.findViewById(R.id.Button01);
            buttonB = dialog.findViewById(R.id.Button02);
            buttonA.setOnClickListener(new View.OnClickListener() {
        	    public void onClick(View v) {
        	        link_hint=1;
                    showDialog(1);
			    }
            });
            buttonB.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    link_hint=2;
                    showDialog(1);
                }
            });
        } else if(id==9) {
            dialog = Tools.scrollableDialog(this,"Info",getResources().getString(R.string.linkedithelpdialog));
        }
        return dialog;
    }
    /* Wird aufgerufen, jedesmal befor der Dialog angezeigt wird.*/
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        if (id == 0) {
            dialog.setTitle(links.get(mselect).toString());
        } else if (id == 5) {// Set up the input
            if(mselect==-1) dialog.setTitle("Create Link ...");
            else dialog.setTitle("Edit Link ...");
            if(mselect==-1) {
                buttonA.setText("A");
                buttonB.setText("B");
            } else {
                buttonA.setText(actual_link.name1);
                buttonB.setText(actual_link.name2);
            }
            Button buttonOK = dialog.findViewById(R.id.Button_OKapply);
            if(mselect<0) buttonOK.setText(R.string.word_add);
            else buttonOK.setText(R.string.word_apply);
            buttonOK.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    actual_link.name1=buttonA.getText().toString();
                    actual_link.name2=buttonB.getText().toString();
                    liste_insert(mselect,actual_link);
                    dismissDialog(5);
                }
            });
        }
    }
}
