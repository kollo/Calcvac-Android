package de.drhoffmannsoftware.calcvac;

/* Section.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.util.Log;
import java.util.ArrayList;
import java.util.List;

class Section {
    private final static String TAG="Section";
    String name;
    int typ;       /* 0= SECTION, 1=LINE 2=ROHR 3=PUMP  4=START 5=END 6=MARKER */
    String len;
    String q;
    String W;
    String Ps;
    String a,b,c,d;
    String t;      /* Temperature*/
    boolean boundaryp;
    boolean boundaryq;
    double bp,bq;
    String mat;    /* aus tabelle */
    int form;   /*0=sqare, 1=rect, 2=circ, 3=ellipt, 4=schluesselloch, -1 generic*/
    String ptyp;   /* aus tabelle*/
    String speed;
    String line;
    public Section() {
        name="";
        clear();
    }
    public Section(String a) {
        name=a;
        clear();
    }
    private void clear() {
        typ=0;
        len="1";
        q="";
        W="";
        Ps="";
        a="";
        b="";
        c="";
        d="";
        t="";
        speed="";
        ptyp="";
        mat="";
        boundaryp=false;
        boundaryq=false;
        form=2;
    }
    public String toString() {
        return name;
    }
    public String formtostring() {
        String t="";
        switch(form) {
            case 0:
                t=t+"square("+a+")";
                break;
            case 1:
                t=t+"rect("+a+"x"+b+")";
                break;
            case 2:
                t=t+"circ("+d+")";
                break;
            case 3:
                t=t+"ellipt("+a+"x"+b+")";
                break;
            case 4:
                t=t+"schluesselloch("+a+"x"+b+";"+c+"x"+d+")";
                break;
            default:
                t=t+"(W="+W+" A="+a+")";
        }
        return t;
    }

    private String make_form() {
        String result="";
        switch(form) {
            case 0: /* Square */
                result=result+", FORM=square";
                if(a.length()>0) result=result+", a="+a;
                break;
            case 1:  /* rect */
                result=result+", FORM=rect";
                if(a.length()>0) result=result+", a="+a;
                if(b.length()>0) result=result+", b="+b;
                break;
            case 2: /* circ */
                result=result+", FORM=circ";
                if(d.length()>0) result=result+", d="+d;
                break;
            case 3:  /* ellipt */
                result=result+", FORM=ellipt";
                if(a.length()>0) result=result+", a="+a;
                if(b.length()>0) result=result+", b="+b;
                break;
            case 4:  /* schluesselloch */
                result=result+", FORM=schluesselloch";
                if(a.length()>0) result=result+", a="+a;
                if(b.length()>0) result=result+", b="+b;
                if(c.length()>0) result=result+", c="+c;
                if(d.length()>0) result=result+", d="+d;
                break;
            default:
                if(W.length()>0) result=result+", W="+W;
                if(a.length()>0) result=result+", A="+a;
        }
        return result;
    }
    private String make_mat() {
        String result = "";
        if(mat.length()>0) result=result+", MAT="+mat;
        else {
            if(q.length()>0) result=result+", q="+q;
        }
        if(t.length()>0) result=result+", T="+t;
        return result;
    }

    private String make_pump() {
        String result = "";
        if(ptyp.length()>0) {
            result=result+", TYP="+ptyp;
            if(speed.length()>0) result=result+", SPEED="+speed;
        } else {
            if(Ps.length()>0) result=result+", Ps="+Ps;
        }
        return result;
    }


    public String format() {
        String result=name+": ";
        switch(typ) {
            case 0: /* SECTION */
                result=result+"SECTION, L="+len;
                if(q.length()>0) result=result+", q="+q;
                if(W.length()>0) result=result+", W="+W;
                if(a.length()>0) result=result+", A="+a;
                if(Ps.length()>0) result=result+", Ps="+Ps;
                break;
            case 1: /* LINE */
                result=result+"LINE=("+line+")";
                break;
            case 2: /* TUBE */
                result=result+"TUBE, L="+len+make_form()+make_mat();
                break;
            case 3:  /* PUMP */
                result=result+"PUMP, L="+len+make_form()+make_mat()+make_pump();
                break;
            case 4: /* Start*/
                result=result+"START";
                if(a.length()>0) result=result+", S0="+a;
                break;
            case 5: /* END */
                result=result+"END";
                break;
            case 6: /* Marker*/
                result=result+"MARKER";
                break;
            default:
                Log.w(TAG,"unknown Section composition."+typ);
                return "";
        }
        if(typ>=0 && !(typ==1)) {
            if(boundaryp||boundaryq) {
                result=result+", BOUNDARY(";
                if(boundaryp) result=result+"P="+bp;
                if(boundaryq) {
                    if(boundaryp) result=result+",";
                    result=result+"Q="+bq;
                }
                result=result+")";
            }
        }
        return result;
    }

    public int parsefromline(String lin) {
        String[] sep=lin.split(":",2);
        name=sep[0].trim();
        lin=sep[1].trim();
        if(lin.toUpperCase().startsWith("LINE=")) {
            typ=1;
            sep=lin.split("=",2);
            line=sep[1].trim();
            if(line.length() >= 2 && line.charAt(0) == '(' && line.charAt(line.length() - 1) == ')') {
                line = line.substring(1, line.length() - 1);
            }
        } else {
            List<String> pars;
           try { pars=splitkomma(lin);} catch(IllegalArgumentException iae) { pars=new ArrayList<>();Log.e(TAG,"Illegal Argument bbb");}
           if(pars.size()>0) {
             if(pars.get(0).equalsIgnoreCase("END")) {
                typ=5;
            } else if(pars.get(0).equalsIgnoreCase("START")) {
                typ=4;
            } else if(pars.get(0).equalsIgnoreCase("MARKER")) {
                typ=6;
            } else if(pars.get(0).equalsIgnoreCase("SECTION")) {
                typ=0;
            } else if(pars.get(0).equalsIgnoreCase("PUMP")) {
                typ=3;
            } else if(pars.get(0).equalsIgnoreCase("TUBE") || pars.get(0).equalsIgnoreCase("ROHR")) {
                typ=2;
            } else {
                typ=-1;
                Log.w(TAG,"unknown section type: "+lin);
            }
           }
            if(pars.size()>1 && typ>=0) {
                int i;
                for(i=1;i<pars.size();i++) {
                    if(pars.get(i).startsWith("BOUNDARY(")) {
                        String t=pars.get(i);
                        t=t.substring(9,t.length()-1);
                        String[] sep2=t.split(",");
                        String[] sep3;
                        int j=sep2.length;
                        while(--j>=0) {
                            t=sep2[j];
                            sep3=t.split("=",2);
                            if(sep3[0].equalsIgnoreCase("P")) {
                                boundaryp=true;
                                bp= Double.parseDouble(sep3[1]);
                            }
                            else if(sep3[0].equalsIgnoreCase("Q")) {
                                boundaryq=true;
                                bq= Double.parseDouble(sep3[1]);
                            }
                        }
                    } else if(pars.get(i).contains("=")) {
                        Log.d(TAG,"Parameter: "+pars.get(i));
                        String[] sep2=pars.get(i).split("=",2);
                        if(sep2[0].equalsIgnoreCase("L")) len = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("S0")) a = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("a")) a = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("b")) b = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("c")) c = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("d")) d = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("T")) t = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("Q")) q = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("W"))   W = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("Ps"))  Ps = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("SPEED"))  speed = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("MAT"))  mat = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("TYP"))  ptyp = sep2[1];
                        else if(sep2[0].equalsIgnoreCase("FORM")) {
                            if(sep2[1].equalsIgnoreCase("sqare")) form = 0;
                            else if(sep2[1].equalsIgnoreCase("rect")) form = 1;
                            else if(sep2[1].equalsIgnoreCase("circ")) form = 2;
                            else if(sep2[1].equalsIgnoreCase("ellipt")) form = 3;
                            else if(sep2[1].equalsIgnoreCase("schluesselloch")) form = 4;
                            else form=-1;
                        } else Log.w(TAG,"Parser: Unknown Parameter at section: "+sep2[0]);
                    }
                }
            }
        }
        return(typ);
    }
    private void process_form(int mol) {
        String temp=t;
        String xa=a;
        if(temp.isEmpty()) temp="300";
        switch(form) {
            case 0: /* Square */
                if(a.length()>0)  {
                    a="_arect("+xa+","+xa+")";
                    W="_wrect("+xa+","+xa+","+len+","+mol+","+temp+")";
                }
                break;
            case 1:  /* rect */
                if(a.length()>0 && b.length()>0) {
                    a="_arect("+xa+","+b+")";
                    W="_wrect("+xa+","+b+","+len+","+mol+","+temp+")";
                }
                break;
            case 2: /* circ */
                if(d.length()>0) {
                    a="_acirc("+d+")";
                    W="_wcirc("+d+","+len+","+mol+","+temp+")";
                }
                break;
            case 3:  /* ellipt */
                if(a.length()>0 && b.length()>0) {
                    a="_aellipt("+xa+","+b+")";
                    W="_wellipt("+xa+","+b+","+len+","+mol+","+temp+")";
                }
                break;
            case 4:  /* schluesselloch */
                if(a.length()>0 && b.length()>0 && c.length()>0 && d.length()>0) {
                    a="_aschluessel("+xa+","+b+","+c+","+d+")";
                    W="_wschluessel("+xa+","+b+","+c+","+d+","+mol+","+temp+")";
                }
                break;
            default:
                if(mol!=28 && mol>0) {
                    if (W.length() > 0)
                        W = "(" + W + ")*" + Math.sqrt(28 / mol); /* Korrektur aus Molekulmasse (TODO: Regexp?, Temp?)*/
                }
                if(t.length()>0 && W.length()>0) {
                        W = "(" + W + ")*SQRT(("+t+")/300)";
                }
        }
    }

    /* Suche Ausgasraten aus Tabelle */
    private void process_material(int mol,ArrayList<Spectrum> outgas) {
        if(mat.length()>0 && outgas.size()>0) {
            int i;
            for(i=0;i<outgas.size();i++) {
                if(outgas.get(i).name.equalsIgnoreCase(mat)) {
                    q=""+outgas.get(i).get_value(mol);
                    break;
                }
            }
        }
    }
    private void process_pump(int mol,ArrayList<Spectrum> pumping) {
        if(ptyp.length()>0 && pumping.size()>0) {
            int i;
            for(i=0;i<pumping.size();i++) {
                if(pumping.get(i).name.equalsIgnoreCase(ptyp)) {
                    Ps="("+speed+")*"+pumping.get(i).get_value(mol)+"/"+len;
                    break;
                }
            }

        }
    }

    /* Convert Section to .inf compatible section*/
    public Section get_simple(int mol,ArrayList<Spectrum> outgas, ArrayList<Spectrum> pumping) {
        Section result=new Section();
        result.name=name;
        result.typ=typ;
        if(typ==1) {
            result.line=line;
        } else {
            Log.d(TAG,"Convert Section: "+name+" boundaryq="+boundaryq);
            result.len=len;
            result.boundaryp=boundaryp;
            result.boundaryq=boundaryq;
            result.bp=bp;
            result.bq=bq;
            result.a=a;
            result.Ps=Ps;
            result.W=W;
            result.q=q;
            if(typ==2) {
                process_form(mol);
                result.a=a;
                result.W=W;
                process_material(mol,outgas);
                result.q=q;
                result.typ=0;
            } else if(typ==3) {
                process_form(mol);
                result.a=a;
                result.W=W;
                process_material(mol,outgas);
                result.q=q;
                process_pump(mol,pumping);
                result.Ps=Ps;
                result.typ=0;
            }
        }
        // TODO: invalidate current section object, do not reuse (better not mdify)
        typ=-1;
        return result;
    }

    private static List<String> splitkomma(String input) {
        int nParens = 0;
        int start = 0;
        List<String> result = new ArrayList<>();
        for(int i=0; i<input.length(); i++) {
            switch(input.charAt(i)) {
            case ',':
                if(nParens == 0) {
                    result.add(input.substring(start, i).trim());
                    start = i+1;
                }
                break;
            case '(':
                nParens++;
                break;
            case ')':
                nParens--;
                if(nParens < 0)
                    throw new IllegalArgumentException("Unbalanced parenthesis at offset #"+i);
                break;
            }
        }
        if(nParens>0)
            throw new IllegalArgumentException("Missing closing parenthesis");
        result.add(input.substring(start).trim());
        return result;
    }
}
