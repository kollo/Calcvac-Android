package de.drhoffmannsoftware.calcvac;

/* InfoActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Button;

public class InfoActivity extends MainActivity {
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
        Button fertig=findViewById(R.id.okbutton);
        TextView readme=findViewById(R.id.description);
        readme.setText(Html.fromHtml(getResources().getString(R.string.description)+getResources().getString(R.string.readme)+
        		getResources().getString(R.string.news)+getResources().getString(R.string.impressum)
        		));
        fertig.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {finish();}
        });
    }
}
