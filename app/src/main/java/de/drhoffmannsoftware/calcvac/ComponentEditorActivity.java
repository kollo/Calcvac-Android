package de.drhoffmannsoftware.calcvac;

/* ComponentEditorActivity.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;

import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

public class ComponentEditorActivity extends Activity {
    private static final String TAG = ComponentEditorActivity.class.getSimpleName();
    public static VacFile vacfile;
    Button pumpbutton,outgasbutton,linkbutton;
    TableLayout mtable=null;
    ScaleGestureDetector mscalegd;
    TextView uebers,vactitle,useline;
    View resetview=null;

    public static String mSelectedElement="";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.componenteditor);

        ActionBar actionBar = getActionBar();
        //  actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.menu_componenteditor));

	    mtable=findViewById(R.id.maintable);
	    uebers=findViewById(R.id.fileuebers);
	    vactitle=findViewById(R.id.vactitle);
	    useline=findViewById(R.id.useline);
        mscalegd = new ScaleGestureDetector(this,new MySimpleOnScaleGestureListener());
	    mtable.setFocusable(true); //necessary for getting the touch events
	    mtable.setFocusableInTouchMode(true);
	    mtable.setOnTouchListener(new View.OnTouchListener() {
	        @Override
            public boolean onTouch(View arg0, MotionEvent event) {
			    if(!mscalegd.isInProgress()) mscalegd.onTouchEvent(event);
			    return true;
		    }
	    });
	    vactitle.setLongClickable(true);
	    vactitle.setMinHeight(30);
	    vactitle.setMinWidth(60);
	    uebers.setMinHeight(30);
	    uebers.setMinWidth(60);
	    uebers.setLongClickable(true);
	    useline.setMinHeight(30);
	    useline.setMinWidth(60);
	    useline.setLongClickable(true);
	    vactitle.setOnLongClickListener(new View.OnLongClickListener() {
		    public boolean onLongClick(View v) {
			    showDialog(3);
			    return(true);
		    }
	    });
		uebers.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
			showDialog(4);
            return(true);}
        });
		useline.setOnLongClickListener(new View.OnLongClickListener() {
			public boolean onLongClick(View v) {
				showDialog(5);
				return(true);}
		});

        pumpbutton=(Button) findViewById(R.id.pumpbutton);
		outgasbutton=(Button) findViewById(R.id.outgasbutton);
		linkbutton=(Button) findViewById(R.id.linksbutton);
		linkbutton.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		startActivity(new Intent(getApplicationContext(), LinkEditorActivity.class));
			}
   		     });
		pumpbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), PumpEditorActivity.class));
			}
		});
		outgasbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), MaterialEditorActivity.class));
			}
		});
    }

    @Override
    public void onStart() {
        super.onStart();
        vacfile= new VacFile(MainActivity.mChosenFile);
        uebers.setText(MainActivity.mChosenFile);
        vactitle.setText(vacfile.get_title());
        useline.setText(vacfile.get_useline());
        if(vacfile.isinffile) pumpbutton.setVisibility(View.GONE);
        else pumpbutton.setVisibility(View.VISIBLE);
        if(vacfile.isinffile) outgasbutton.setVisibility(View.GONE);
        else outgasbutton.setVisibility(View.VISIBLE);
        updateTable();
    }

    @Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.componenteditmenu, menu);
		return true;
	}
	private int dialogi=0;
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed
			// in the Action Bar.
			finish();
			return true;
		case R.id.vdl_options_preferences:
			startActivity(new Intent(this, PreferencesActivity.class));
			return true;
		case R.id.vdl_options_about:
			showDialog(0);
			return true;
		case R.id.vdl_options_help:
			showDialog(1);
			return true;
		case R.id.fileeditor:
			call_editor();
			return true;
			case R.id.addsection:
				mSelectedElement="new_"+vacfile.sections.size();
				startActivity(new Intent(getApplicationContext(), ElementEditorActivity.class));
				return true;
			case R.id.addsubline:
				mSelectedElement="newline_"+vacfile.sections.size();
				startActivity(new Intent(getApplicationContext(), LineEditorActivity.class));
				return true;
			case R.id.addmarker:
				mSelectedElement="mark_"+vacfile.sections.size();
				startActivity(new Intent(getApplicationContext(), ElementEditorActivity.class));
				return true;
			case R.id.links:
				startActivity(new Intent(getApplicationContext(), LinkEditorActivity.class));
				return true;
			case R.id.pumping:
				if(vacfile.isinffile) toaster("ERROR: Materials can only be used on .vac files.");
				else startActivity(new Intent(getApplicationContext(), PumpEditorActivity.class));
				return true;
			case R.id.material:
				if(vacfile.isinffile) toaster("ERROR: Pumps definitions can only be used on .vac files.");
				else startActivity(new Intent(getApplicationContext(), MaterialEditorActivity.class));
				return true;
			case R.id.file_statistics:
				Dialog dialog = Tools.scrollableDialog(this,getResources().getString(R.string.menu_statistics),
				"<h4>File Statistics:</h4>"+vacfile.info());
				dialog.show();
				return true;
		case R.id.vdl_options_finish:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

    public class Item {
        public String text;
        public int icon;
        Item(String text, Integer icon) {
            this.text = text;
	    	this.icon = icon;
		}
		@Override
	    public String toString() {
            return text;
        }
    }

	@Override
	protected Dialog onCreateDialog(final int id) {
		Dialog dialog = null;
		if(id==0) {
			dialog = Tools.scrollableDialog(this, "About", getResources().getString(R.string.description) +
					getResources().getString(R.string.impressum));
		} else if(id==1) {
			dialog = Tools.scrollableDialog(this,"Info",getResources().getString(R.string.componentedithelpdialog));
		} else if(id==2) {  /*Auswahl Edit, delete, Cancel*/
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(mSelectedElement);
			builder.setIcon(R.drawable.pumpe);
			final String[] mitem={
					"edit","delete","Cancel"
			};
			builder.setItems(mitem, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which){
					Log.d(TAG,"Item #"+which+"was clicked");
					if(which==0) {
						if(vacfile.isline(mSelectedElement)) startActivity(new Intent(getApplicationContext(), LineEditorActivity.class));
						else startActivity(new Intent(getApplicationContext(), ElementEditorActivity.class));
					} else if(mitem[which].equalsIgnoreCase("delete")) { /*delete*/
						showDialog(6);  /*Confirm delete element*/
					} else if(which==2) {
						if(resetview!=null) resetview.setBackgroundColor(Color.BLACK);
					}
				}});
			dialog = builder.show();
			dialog.setCanceledOnTouchOutside(false);
		} else if(id==3) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Enter new Title:");

			// Set up the input
			final EditText dialog_text_input = new EditText(this);
			// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
			dialog_text_input.setInputType(InputType.TYPE_CLASS_TEXT );
			dialog_text_input.setText(vactitle.getText().toString());
			builder.setView(dialog_text_input);

			// Set up the buttons
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String t = dialog_text_input.getText().toString();
					vacfile.set_title(t);
					vactitle.setText(t);
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
			dialog = builder.create();
			dialog.setCanceledOnTouchOutside(true);
		} else if(id==4) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Enter new Filename:");

			// Set up the input
			final EditText dialog_text_input = new EditText(this);
			// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
			dialog_text_input.setInputType(InputType.TYPE_CLASS_TEXT );
			dialog_text_input.setText(uebers.getText().toString());
			builder.setView(dialog_text_input);

			// Set up the buttons
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String t= dialog_text_input.getText().toString();
					Log.d(TAG,"String="+t);
					MainActivity.mChosenFile=vacfile.rename_file(t);
					uebers.setText(MainActivity.mChosenFile);
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
			});
			dialog = builder.create();
			dialog.setCanceledOnTouchOutside(true);
		} else if(id==5) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Enter Vacuum line to use:");
			builder.setIcon(R.drawable.pumpe);

			// Set up the input
			final Item[] items;
			items = new Item[vacfile.vaclines.size()];
			for(int i=0;i<vacfile.vaclines.size();i++) {
				items[i]=new Item(vacfile.vaclines.get(i),R.drawable.ic_menu_compose);
			}
			ListAdapter adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item,android.R.id.text1,items){
				public View getView(int position, View convertView, ViewGroup parent) {
					//User super class to create the View
					View v = super.getView(position, convertView, parent);
					TextView tv = (TextView)v.findViewById(android.R.id.text1);

					//Put the image on the TextView
					tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

					//Add margin between image and text (support various screen densities)
					int dp5 = pix(5);
					tv.setCompoundDrawablePadding(dp5);
					return v;
				}
			};

			builder.setAdapter(adapter,  new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which){
					vacfile.set_useline(vacfile.vaclines.get(which));
					useline.setText(vacfile.vaclines.get(which));
				}});

			final AlertDialog mad;
			mad = builder.create(); //don't show dialog yet
			mad.setOnShowListener(new DialogInterface.OnShowListener() {
				@Override
				public void onShow(DialogInterface dialog) {
					ListView lv = mad.getListView(); //this is a ListView with your "buds" in it
					lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
						@Override
						public boolean onItemLongClick(AdapterView<?> parent, View view, int which, long id) {
							Log.d("Long Click!","List Item #"+which+" was long clicked");
							showDialog(2);
							return true;
						}
					});
				}
			});
			dialog = mad;
		}  else if(id==6) {/* Confirm delete Element Dialog */
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getResources().getString(R.string.message_really_delete));
			builder.setMessage(String.format(getResources().getString(R.string.message_delete_elem), mSelectedElement));
			builder.setPositiveButton(getResources().getString(R.string.word_proceed), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					vacfile.delete(mSelectedElement);
					vacfile.reload();
					updateTable();
					mtable.invalidate();
				} });
			builder.setNeutralButton(getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if(resetview!=null) resetview.setBackgroundColor(Color.BLACK);
				} });
			dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
		}  else if(id==9) {/* Dialog Fehlermeldung kein Editor */
			dialog=Tools.scrollableDialog(this,"ERROR",getResources().getString(R.string.editor_notfound));
		} else  {
			dialog = Tools.scrollableDialog(this,"",getResources().getString(R.string.description)+
					getResources().getString(R.string.impressum));
		}
		return dialog;
	}
/* Wird aufgerufen, jedesmal befor der Dialog angezeigt wird.*/
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);
		if(id==2) {
			dialog.setTitle(mSelectedElement);
		} else if(id==3) {// Set up the input
			final EditText dialog_text_input = new EditText(this);
			// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
			dialog_text_input.setInputType(InputType.TYPE_CLASS_TEXT);
			dialog_text_input.setText(vactitle.getText().toString());
			((AlertDialog) dialog).setView(dialog_text_input);
		} else if(id==4) {// Set up the input
				final EditText dialog_text_input = new EditText(this);
				// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
				dialog_text_input.setInputType(InputType.TYPE_CLASS_TEXT );
				dialog_text_input.setText(uebers.getText().toString());
				((AlertDialog) dialog).setView(dialog_text_input);
		} else if(id==6) {// Set up the input
			((AlertDialog)dialog).setMessage(String.format(getResources().getString(R.string.message_delete_elem), mSelectedElement));
		}
	}
    private void updateTable() {
        Calendar cal = Calendar.getInstance();
        TableRow.LayoutParams params0 = new TableRow.LayoutParams(pix(80), pix(50));
        TableRow.LayoutParams params1 = new TableRow.LayoutParams(pix(50), pix(50));
        TableRow.LayoutParams params2 = new TableRow.LayoutParams(pix(250), pix(50));
        if(mtable!=null) mtable.removeAllViews();
        TableRow row;
        TextView text;
        resetview=null;
        for (int i = 0; i < vacfile.sections.size(); i++) {
            row = new TableRow(this);
            for (int j = 0; j < 3; j++) {
                text = new TextView(this);
                text.setTextColor(Color.YELLOW);
                if(j==0) text.setLayoutParams(params0);
                else if(j==1) text.setLayoutParams(params1);
                else text.setLayoutParams(params2);
                if(j==0) {
                    text.setText(vacfile.sections.get(i).name);
                    if(vacfile.sections.get(i).typ==0) {
                    	if(vacfile.sections.get(i).Ps.length()>0) text.setTextColor(Color.GREEN);
						// else if(vacfile.sections.get(i).W<5e-4) text.setTextColor(Color.RED);
                    	else text.setTextColor(Color.CYAN);
					}
					else if(vacfile.sections.get(i).typ==1) text.setTextColor(Color.MAGENTA);
					else if(vacfile.sections.get(i).typ==2) text.setTextColor(Color.CYAN);
					else if(vacfile.sections.get(i).typ==3) text.setTextColor(Color.GREEN);
					else if(vacfile.sections.get(i).typ==4) text.setTextColor(Color.YELLOW);
					else if(vacfile.sections.get(i).typ==5) text.setTextColor(Color.YELLOW);
                    else text.setTextColor(Color.RED);
                } else if(j==1) {
                	text.setText("L="+vacfile.sections.get(i).len);
                	if(vacfile.sections.get(i).typ==1 || vacfile.sections.get(i).typ==4 ||vacfile.sections.get(i).typ==5||vacfile.sections.get(i).typ==6) text.setTextColor(Color.DKGRAY);
				} else if(j==2) {
					if(vacfile.sections.get(i).typ==1) {
						text.setText(vacfile.sections.get(i).line);
						text.setTextColor(Color.MAGENTA);
					} else if(vacfile.sections.get(i).typ==0) {
						String t="Q="+vacfile.sections.get(i).q+", W="+vacfile.sections.get(i).W+", A="+vacfile.sections.get(i).a;
						if(vacfile.sections.get(i).Ps.length()>0) t=t+", Ps="+vacfile.sections.get(i).Ps;
						if(vacfile.sections.get(i).boundaryp) t=t+", bound_P="+vacfile.sections.get(i).bp;
						if(vacfile.sections.get(i).boundaryq) t=t+", bound_Q="+vacfile.sections.get(i).bq;
						text.setText(t);
						text.setTextColor(Color.YELLOW);
					} else if(vacfile.sections.get(i).typ==2) {
						String t=vacfile.sections.get(i).mat+", "+vacfile.sections.get(i).formtostring();
						if(vacfile.sections.get(i).mat.isEmpty()) t=t+", q="+vacfile.sections.get(i).q;
						if(vacfile.sections.get(i).boundaryp) t=t+", bound_P="+vacfile.sections.get(i).bp;
						if(vacfile.sections.get(i).boundaryq) t=t+", bound_Q="+vacfile.sections.get(i).bq;
						text.setText(t);
						text.setTextColor(Color.YELLOW);
					} else if(vacfile.sections.get(i).typ==3) {
						String t=vacfile.sections.get(i).ptyp+"("+vacfile.sections.get(i).speed+")"+"/"+vacfile.sections.get(i).mat+", "+vacfile.sections.get(i).formtostring();
						if(vacfile.sections.get(i).mat.isEmpty()) t=t+", q="+vacfile.sections.get(i).q;
						if(vacfile.sections.get(i).ptyp.isEmpty()) t=t+", Ps="+vacfile.sections.get(i).Ps;
						if(vacfile.sections.get(i).boundaryp) t=t+", bound_P="+vacfile.sections.get(i).bp;
						if(vacfile.sections.get(i).boundaryq) t=t+", bound_Q="+vacfile.sections.get(i).bq;
						text.setText(t);
						text.setTextColor(Color.YELLOW);
					} else if(vacfile.sections.get(i).typ==4) {
						text.setTextColor(Color.CYAN);
						text.setText("S0="+vacfile.sections.get(i).a);
					} else if(vacfile.sections.get(i).typ==5) {  /* END*/
						text.setText("END");
						text.setTextColor(Color.BLUE);
					} else if(vacfile.sections.get(i).typ==6) {  /* MARKER*/
						text.setText("MARKER");
						text.setTextColor(Color.DKGRAY);
					} else {
						text.setText("-");
						text.setTextColor(Color.DKGRAY);
					}

		} else text.setText(String.format(Locale.US,"(%d, %d)", i, j));
                text.setLongClickable(true);
                text.setEnabled(true);
                text.setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        // v.setBackgroundColor(Color.GRAY);
                        TableRow tablerow = (TableRow)v.getParent();
                        tablerow.setBackgroundColor(Color.BLUE);
                        resetview=tablerow;
                        TextView sample = (TextView) tablerow.getChildAt(0);
                        mSelectedElement=sample.getText().toString();
                        showDialog(2);
                        return false;
                    }
                });
                text.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                    // v.setBackgroundColor(Color.GRAY);
                    TableRow tablerow = (TableRow)v.getParent();
                    tablerow.setBackgroundColor(Color.BLUE);
                    resetview=tablerow;
                    TextView sample = (TextView) tablerow.getChildAt(0);
                    mSelectedElement=sample.getText().toString();
                    if(vacfile.isline(mSelectedElement)) startActivity(new Intent(getApplicationContext(), LineEditorActivity.class));
                    else startActivity(new Intent(getApplicationContext(), ElementEditorActivity.class));
                    }
                });
                row.addView(text);
            }
            row.setLongClickable(true);
            row.setEnabled(true);
            row.setOnLongClickListener(new OnLongClickListener() {
                public boolean onLongClick(View v) {
                    TableRow tablerow = (TableRow)v;
                    tablerow.setBackgroundColor(Color.BLUE);
                    resetview=tablerow;
                    TextView sample = (TextView) tablerow.getChildAt(0);
                    mSelectedElement=sample.getText().toString();
                    showDialog(2);
                    return false;
                }
            });
            row.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TableRow tablerow = (TableRow)v;
                    tablerow.setBackgroundColor(Color.BLUE);
                    resetview=tablerow;
                    TextView sample = (TextView) tablerow.getChildAt(0);
                    mSelectedElement=sample.getText().toString();
                    if(vacfile.isline(mSelectedElement)) startActivity(new Intent(getApplicationContext(), LineEditorActivity.class));
                    else startActivity(new Intent(getApplicationContext(), ElementEditorActivity.class));

                }
            });
            if(mtable!=null) mtable.addView(row);
        }
    }

    public void call_editor() {
	    Log.d(TAG,"call editor with "+vacfile.mfilename);
	    Intent intent = new Intent(Intent.ACTION_EDIT);
	    File dirdata=new File(MainActivity.Basestoragedirectory+"/"+"calcvac/");
	    dirdata.mkdir();
	    File f=new File(dirdata,vacfile.mfilename);
	    Uri uri = Uri.fromFile(f);
	    Log.d(TAG,uri.toString());
	    intent.setDataAndType(uri, "text/plain");
	    try {startActivityForResult(intent,1);}
	    catch(ActivityNotFoundException e) {
		    toaster(String.format(getResources().getString(R.string.error_editor), e.getMessage()));
		    showDialog(9);
	    }
	    catch(SecurityException e) {
		    toaster(String.format(getResources().getString(R.string.error_editor), e.getMessage()));
		    showDialog(9);
	    }
    }
    void toaster(final String message) {
        Log.d(TAG,message);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
    private int pix(int dp) {
       return (int) (dp * getResources().getDisplayMetrics().density + 0.5f);
    }

    public class MySimpleOnScaleGestureListener extends SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            final float scaleFactor = detector.getScaleFactor();
            double x=detector.getFocusX();
            Log.d(TAG,"Scale: "+scaleFactor+" x="+x);
            mtable.invalidate();
            return true;
        }
    }
}
