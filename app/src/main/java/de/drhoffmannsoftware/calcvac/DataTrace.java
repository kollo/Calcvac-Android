package de.drhoffmannsoftware.calcvac;

/* DataTrace.java (c) 2018 by Markus Hoffmann
 *
 * This file is part of Calcvac for Android, (c) by Markus Hoffmann 2002-2018
 * ==========================================================================
 * Calcvac for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


/* Datenspuren haben gemeinsame X-Basis,
 * unterschiedliche namen, farbe*/

import android.graphics.Color;
import android.util.Log;
import java.util.ArrayList;

class DataTrace {
    private final static int MAX_ANZDATA=100000;
    private static final String TAG = DataTrace.class.getSimpleName();

    String name;
    String unit;

    public double y[];
    double ymin,ymax;
    int idxmin,idxmax;
    double deltamin,deltamax;
    int color;
    int anzdata;
    public ArrayList<Link> links;

    DataTrace(int m) {
    	y=new double[m];
    	clear();
    }

    DataTrace() {
    	y=new double[MAX_ANZDATA];
    	clear();
    }
    public void clear() {
		name="";
		unit="";
		color= Color.BLUE;
		anzdata=0;
		ymin=9999;
		ymax=-9999;
        deltamin=9999;
        deltamax=-9999;
        idxmin=0;
        idxmax=0;
        links=new ArrayList<Link>();
    }

    public void add(double val) {
        if(anzdata>0) {
    	    double delta=Math.abs(val-y[anzdata-1]);
    	    if(delta>deltamax) deltamax=delta;
    	    if(delta<deltamin) deltamin=delta;
    	}
    	if(val<ymin) {ymin=val;idxmin=anzdata;}
    	if(val>ymax) {ymax=val;idxmax=anzdata;}
    	y[anzdata++]=val;
    }
    public void calc_minmax() {
	ymin=9999;
	ymax=-9999;
	deltamin=9999;
	deltamax=-9999;
	if(anzdata>0) {
	    int i;
	    double val,delta;
	    for(i=0;i<anzdata;i++) {
		val=y[i];
		if(i>0) {
		    delta=Math.abs(val-y[i-1]);
            	    if(delta>deltamax) deltamax=delta;
            	    if(delta<deltamin) deltamin=delta;
            	}
		if(val<ymin) {ymin=val;idxmin=i;}
		if(val>ymax) {ymax=val;idxmax=i;}
            }
        }
    }
    public DataTrace calc_hist() {
    	int maxanzbins=256; // TODO: maxanzbins=(int) (Math.abs(maxy1-miny1)/deltamin1)+1;
    	DataTrace result=new DataTrace(maxanzbins);
    	result.anzdata=maxanzbins;
    	Log.d(TAG,"calc_hist: ymin="+ymin+" ymax="+ymax);
    	if(anzdata>0) {
            int i,bin;
            for (i = 0; i < anzdata; i++) {
            	bin=(int)((y[i]-ymin)/(ymax-ymin)*(double)(maxanzbins-1));
            	result.y[bin]=result.y[bin]+1;
            }
            result.calc_minmax();
        }
        return result;
    }
}
