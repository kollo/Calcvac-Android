LOCAL_PATH := $(call my-dir)
export MAINDIR:= $(LOCAL_PATH)



#LAPACK, BLAS, F2C compilation
include $(CLEAR_VARS)
LOCAL_CFLAGS := -fsigned-char
include $(MAINDIR)/clapack/Android.mk
LOCAL_PATH := $(MAINDIR)
include $(CLEAR_VARS)
LOCAL_MODULE:= lapack
LOCAL_STATIC_LIBRARIES := tmglib clapack1 clapack2 clapack3 blas f2c
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)
LOCAL_EXPORT_LDLIBS := $(LOCAL_LDLIBS)
include $(BUILD_STATIC_LIBRARY)

#vacline binary for assets
include $(CLEAR_VARS)
include $(MAINDIR)/vacline/Android.mk
