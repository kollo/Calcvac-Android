
/* This file is part of VACLINE, pressure profile calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details
 */
#define FALSE 0
#define TRUE 1
#ifdef __cplusplus
extern "C" {
#endif
int wort_sep (char *t,char c,int klamb ,char *w1, char *w2);
int wort_sep2(char *t,char *c,int klamb ,char *w1, char *w2);
int wort_sepr(char *t,char c,int klamb ,char *w1, char *w2);
int wort_sepr2(char *t,char *c,int klamb ,char *w1, char *w2);
int wort_sep_e(char *t,char c,int klamb ,char *w1, char *w2);
int wort_sepr_e(char *t,char c,int klamb ,char *w1, char *w2);

int exist(char *filename);
int myeof(FILE *n);
char *longlineinput(FILE *n);


void xtrim(char *t,int f, char *w );
void xtrim2(char *t,int f, char *w );


char *searchchr(char *buf, char c);
char *searchchr2(char *buf, char c);
char *searchchr2_multi(char *buf, char *c);
void unbrak(char *t);
int getbrak( char *ss, char *s1, char *par);
int getpar( char *ss, char *s1, char *par);
char *getname(char *buf, char *name);


#ifdef __cplusplus
}
#endif
