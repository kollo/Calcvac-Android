/* This file is part of VACLINE, the pressure calculator (c) Markus Hoffmann
 * ============================================================
 * VACLINE is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "mathematics.h"

extern int verbose;

MATRIX create_matrix(int rows,int cols) {
  MATRIX ergeb;
  ergeb.rows=rows;
  ergeb.cols=cols;
  ergeb.pointer=malloc(rows*cols*sizeof(double));
  return(ergeb);
}

void clear_matrix(MATRIX *a) {
  int i;
  for(i=0;i<(a->rows)*(a->cols);i++) (a->pointer)[i]=0;
}
void free_matrix(MATRIX *a) {
  free(a->pointer);
}


/* Solves the Gleichungssystem Ax=b with Sigulaerwertzerlegung */


#ifdef ANDROID

 int dgelss_(long int *m, long int *n, long int *nrhs, 
	double *a, long int *lda, double *b, long int *ldb, double *
	s, double *rcond, long int *rank, double *work, long int *lwork,
	 long int *info);
#else
 int dgelss_(int *m, int *n, int *nrhs, 
	double *a, int *lda, double *b, int *ldb, double *
	s, double *rcond, int *rank, double *work, int *lwork,
	 int *info);
#endif
 
MATRIX solve(MATRIX a,MATRIX b) {
  /* Hier probieren wir noch einen Algoritmus mit SVD    */
#ifdef ANDROID
  long int lda=a.rows;
  long int ldb=max(a.rows,a.cols);
  long int nrhs=1;
  long int rank;
  long int info;
  long int lwork=3*min(a.cols,a.rows) + 
       max(2*min(a.cols,a.rows), max(a.cols,a.rows))+nrhs+10*a.rows;
#else
  int lda=a.rows,ldb=max(a.rows,a.cols),nrhs=1,rank,info;
  int lwork=3*min(a.cols,a.rows) + 
       max(2*min(a.cols,a.rows), max(a.cols,a.rows))+nrhs+10*a.rows;
#endif
  double rcond=1e-10;
  double work[lwork];
  b.pointer=realloc(b.pointer,sizeof(double)*ldb);
  double singulars[a.cols];
  
  
  dgelss_(&a.rows,&a.cols,&nrhs,a.pointer,&lda,b.pointer,&ldb,singulars,
    &rcond,&rank,work,&lwork,&info);
  if(info) {
    printf("ERROR: the solver has not been successful.\n");
#ifdef ANDROID
    if(info<0) printf("the %ld-th argument had an illegal value.\n",-info);
    else printf("the algorithm for computing the SVD failed to converge;\n"  
                "%ld off-diagonal elements of an intermediate "
                " bidiagonal form did not converge to zero.\n",info);
#else
    if(info<0) printf("the %d-th argument had an illegal value.\n",-info);
    else printf("the algorithm for computing the SVD failed to converge;\n"  
                "%d off-diagonal elements of an intermediate "
                " bidiagonal form did not converge to zero.\n",info);
#endif
    exit(-1);
  }

  if(verbose>0) printf("INFO: accuracy: %g%%\n",(double)rank/a.rows*100);
#ifdef ANDROID
  if(verbose>1) printf("lwork=%ld optimal: %g\n",lwork,work[0]);
#else
  if(verbose>1) printf("lwork=%d optimal: %g\n",lwork,work[0]);
#endif

  if(verbose>1) {
    int i;
    printf("First 20 singular values:\n");
    for(i=0;i<min(20,a.cols);i++) printf("%d: %g\n",i,singulars[i]);
  }
  
  if((double)rank<(double)a.cols/6) printf("WARNING: The solution will not be very accurate!\n");
  return(b);
}



