/*  Erstellt: Jan. 2009  				   **
*/
/* This file is part of VACLINE, pressure profile calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "utility.h"
#include "parser.h"
#include "vacline.h"
#include "mathematics.h"

/* Berechne den Umfang verschiedener Kammerquerschnitte. 
   a,b,c,d in m
   Ergebnis in cm^2/m  (spezifische Fläche) = 0.01*0.01m  */
static double arect(double a,double b) {return(2*(a+b)*100*100);}
static double acirc(double d)          {return(PI*d*100*100);}
static double aellipt(double ua, double ub) {
        double eprod=1, oprod=1, aa, bb, eps, sum=1;
        int i;
        if (ub > ua) {
            aa = ub;
            ub = ua;
            ua = aa;
        }
        aa = ua / 2;
        bb = ub / 2;
        eps = (aa * aa - bb * bb) / aa / aa;
        for (i = 1; i <= 10; i++) {
            oprod *= 2 * (double) i - 1;
            eprod *= 2 * (double) i;
            sum += -1 / (2 * (double) i - 1) *pow(eps, (double) i) * (oprod / eprod) * (oprod / eprod);
        }
        // Genauigkeit etwa 1e-4
        return(floor(ua * PI * sum  * 100*100));   // in cm^2/m
}
static double aschluessel(double a, double b, double c, double d) {return(2*c*100*100+aellipt(a,b));}

/* Spezifische Leitfähigkeit */
/* Leitwert einer Kreisrunden Kammer (Durchmesser a) mir Länge l*/
/* a,b,c,d in m, 
 * l       in m
 * t       in K   */
static double wcirc(double a,double l,double m,double t) {
  a*=100;
  l*=100;
  double c1=11.6*a*a*PI/4;
  double c2=17.1*a*a*a*a/sqrt(a*a+a*a)/l;
  double c3=1/(1/c1+1/c2);
  return floor(sqrt(28/m*t/300)*c3*100)/100.0;  // in l/s
}
static double wellipt(double a,double b,double l,double m,double t) {
  a*=100;
  b*=100;
  l*=100;
  double c1 = 11.6 * a * b * PI / 4;
  double c2 = 17.1 * a * a * b * b / sqrt(a * a + b * b) / l;
  double c3 = 1 / (1 / c1 + 1 / c2);
 // printf("wellipt: %g %g %g %g %g -> %g\n",a,b,l,m,t,floor(sqrt(28 / m*t/300) * c3 * 100) / 100.0);
  return floor(sqrt(28 / m*t/300) * c3 * 100) / 100.0; // in l / s
}
static double kk(double x) {return(0.0653947/(x+0.0591645)+1.0386);}

static double wrect(double a, double b, double l, double m,double t) {
  a*=100;
  b*=100;
  l*=100;
  double c1=11.6*a*b;
  double c2=30.9*kk(min(a/b, b/a));
  c2*=a*a*b*b/(a+b)/l;
  c2=17.1*a*a*b*b/sqrt(a * a + b * b) / l;
  double c3=1/(1/c1+1/c2);
  return(floor(sqrt(28/m*t/300)*c3*100)/100.0); // in l/s
}

const FUNCTION pfuncs[]= {  /* alphabetisch !!! */
 { F_ARGUMENT|F_DRET,  "!nulldummy", fabs ,0,0   ,{0}},
 { F_DQUICK|F_DRET,    "_acirc"  , acirc ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "_aellipt"  , aellipt ,2,2     ,{PL_NUMBER,PL_NUMBER}},
 { F_DQUICK|F_DRET,    "_arect"    , arect ,2,2     ,{PL_NUMBER,PL_NUMBER}},
 { F_DQUICK|F_DRET,    "_wcirc"    , wcirc ,4,4     ,{PL_NUMBER,PL_NUMBER,PL_NUMBER,PL_NUMBER}},
 { F_DQUICK|F_DRET,    "_wellipt"  , wellipt ,5,5     ,{PL_NUMBER,PL_NUMBER,PL_NUMBER,PL_NUMBER,PL_NUMBER}},
 { F_DQUICK|F_DRET,    "_wrect"    , wrect ,5,5     ,{PL_NUMBER,PL_NUMBER,PL_NUMBER,PL_NUMBER,PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ABS"       , fabs ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ACOS"      , acos ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ACOSH"      , acosh ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ASIN"      , asin ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ASINH"      , asinh ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ATAN"      , atan ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ATAN2"     , atan2 ,2,2     ,{PL_NUMBER,PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ATANH"     , atanh ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "ATN"       , atan ,1,1     ,{PL_NUMBER}},


 { F_DQUICK|F_DRET,    "CBRT"      , cbrt ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "CEIL"      , ceil ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "COS"       , cos ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "COSH"      , cosh ,1,1     ,{PL_NUMBER}},


 { F_DQUICK|F_DRET,    "EXP"       , exp ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "EXPM1"     , expm1 ,1,1     ,{PL_NUMBER}},

 { F_DQUICK|F_DRET,    "FLOOR"     , floor ,1,1     ,{PL_NUMBER}},


 { F_DQUICK|F_DRET,    "HYPOT"     , hypot ,2,2     ,{PL_NUMBER,PL_NUMBER}},



 { F_DQUICK|F_DRET,    "LN"        , log ,1,1     ,{PL_NUMBER}},

 { F_DQUICK|F_DRET,    "LOG"       , log ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "LOG10"     , log10 ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "LOG1P"     , log1p ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "LOGB"      , logb  ,1,1     ,{PL_NUMBER}},

 { F_DQUICK|F_DRET,    "MOD"     , fmod ,2,2     ,{PL_NUMBER,PL_NUMBER }},

 { F_DQUICK|F_DRET,    "SIN"       , sin ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "SINH"      , sinh ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "SQR"       , sqrt ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "SQRT"      , sqrt ,1,1     ,{PL_NUMBER}},

 { F_DQUICK|F_DRET,    "TAN"       , tan ,1,1     ,{PL_NUMBER}},
 { F_DQUICK|F_DRET,    "TANH"       , tanh ,1,1     ,{PL_NUMBER}},

};
const int anzpfuncs=sizeof(pfuncs)/sizeof(FUNCTION);

int vergleich(char *w1,char *w2) {
    double x=(parser(w1)-parser(w2));
    if(x==0) return(0);
    else if(x<0) return(-1);
   return(1);
}
static int atohex(char *n) {
  int value=0;
  while(*n) {
    value<<=4;
    if(*n>='0' && *n<='9') value+=(int)(*n-'0');
    else if(*n>='a' && *n<='f') value+=(int)(*n-'a')+10;
    else if(*n>='A' && *n<='F') value+=(int)(*n-'A')+10;
    n++;
  }
  printf("hex %s %x \n",n,value);
  return(value);
}


static int atobin(char *n) {
  int value=0;
  while(*n) {
    value<<=1;
    if(*n!='0') value++;  
    n++;
  }
  return(value);
}

double myatof(char *n) {
  if(*n=='$') return((double)atohex(++n));  
  else if(*n=='%') return((double)atobin(++n));  
  else return(atof(n));
}




double parser(char *funktion){  /* Rekursiver num. Parser */
  char *pos,*pos2;
  char s[strlen(funktion)+1],w1[strlen(funktion)+1],w2[strlen(funktion)+1];
  int vnr;

  // printf("Parser: <%s>\n",funktion); 

  /* Logische Operatoren AND OR NOT ... */

  if(searchchr2_multi(funktion,"&|")!=NULL) {
    if(wort_sepr2(s,"&&",TRUE,w1,w2)>1)     return((double)((int)parser(w1) & (int)parser(w2)));
    if(wort_sepr2(s,"||",TRUE,w1,w2)>1)     return((double)((int)parser(w1) | (int)parser(w2)));
  }
  xtrim(funktion,0,s);  /* Leerzeichen vorne und hinten entfernen */

  if(searchchr2(funktion,' ')!=NULL) {
    if(wort_sepr2(s," AND ",TRUE,w1,w2)>1)  return((double)((int)parser(w1) & (int)parser(w2)));    /* von rechts !!  */
    if(wort_sepr2(s," OR ",TRUE,w1,w2)>1)   return((double)((int)parser(w1) | (int)parser(w2)));
    if(wort_sepr2(s," NAND ",TRUE,w1,w2)>1) return((double)~((int)parser(w1) & (int)parser(w2)));
    if(wort_sepr2(s," NOR ",TRUE,w1,w2)>1)  return((double)~((int)parser(w1) | (int)parser(w2)));
    if(wort_sepr2(s," XOR ",TRUE,w1,w2)>1)  return((double)((int)parser(w1) ^ (int)parser(w2)));	
    if(wort_sepr2(s," EOR ",TRUE,w1,w2)>1)  return((double)((int)parser(w1) ^ (int)parser(w2)));	
    if(wort_sepr2(s," EQV ",TRUE,w1,w2)>1)  return((double)~((int)parser(w1) ^ (int)parser(w2)));
    if(wort_sepr2(s," IMP ",TRUE,w1,w2)>1)  return((double)(~((int)parser(w1) ^ (int)parser(w2)) | (int)parser(w2)));
    if(wort_sepr2(s," MOD ",TRUE,w1,w2)>1)  return(fmod(parser(w1),parser(w2)));
    if(wort_sepr2(s," DIV ",TRUE,w1,w2)>1) {
      int nenner=(int)parser(w2);
      if(nenner) return((double)((int)parser(w1) / nenner));
      else {
        printf("Parser error: <%s> Division durch 0\n",w2); /* Division durch 0 */
        return(0);
      }
    }
    if(wort_sepr2(s,"NOT ",TRUE,w1,w2)>1) {
      if(strlen(w1)==0) return((double)(~(int)parser(w2)));    /* von rechts !!  */
      /* Ansonsten ist NOT Teil eines Variablennamens */
    }
  }

  /* Erst Vergleichsoperatoren mit Wahrheitwert abfangen (lowlevel < Addition)  */
  if(searchchr2_multi(s,"<=>")!=NULL) {
    if(wort_sep2(s,"==",TRUE,w1,w2)>1)      return(vergleich(w1,w2)?0:-1);
    if(wort_sep2(s,"<>",TRUE,w1,w2)>1) return(vergleich(w1,w2)?-1:0);
    if(wort_sep2(s,"><",TRUE,w1,w2)>1) return(vergleich(w1,w2)?-1:0);
    if(wort_sep2(s,"<=",TRUE,w1,w2)>1) return((vergleich(w1,w2)<=0)?-1:0);
    if(wort_sep2(s,">=",TRUE,w1,w2)>1) return((vergleich(w1,w2)>=0)?-1:0);
    if(wort_sep(s,'=',TRUE,w1,w2)>1)   return(vergleich(w1,w2)?0:-1);
    if(wort_sep(s,'<',TRUE,w1,w2)>1)   return((vergleich(w1,w2)<0)?-1:0);
    if(wort_sep(s,'>',TRUE,w1,w2)>1)   return((vergleich(w1,w2)>0)?-1:0);
  }
   /* Addition/Subtraktion/Vorzeichen  */
  if(searchchr2_multi(s,"+-")!=NULL) {
    if(wort_sep_e(s,'+',TRUE,w1,w2)>1) {
      if(strlen(w1)) return(parser(w1)+parser(w2));
      else return(parser(w2));   /* war Vorzeichen + */
    }
    if(wort_sepr_e(s,'-',TRUE,w1,w2)>1) {       /* von rechts !!  */
      if(strlen(w1)) return(parser(w1)-parser(w2));
      else return(-parser(w2));   /* war Vorzeichen - */
    }
  }
  if(searchchr2_multi(s,"*/^")!=NULL) {
    if(wort_sepr(s,'*',TRUE,w1,w2)>1) {
      if(strlen(w1)) return(parser(w1)*parser(w2));
      else {
        printf("pointers not yet possible! %s\n",w2);   /* war pointer - */
        return(0);
      }
    }
    if(wort_sepr(s,'/',TRUE,w1,w2)>1) {
      if(strlen(w1)) {
        double nenner;
        nenner=parser(w2);
        if(nenner!=0.0) return(parser(w1)/nenner);    /* von rechts !!  */
        else { printf("Paser Error: <%s> Division durch 0\n",w2); return(0);  } /* Division durch 0 */
      } else { printf("Paser Syntax Error: <%s>",w2); return(0); }/* "Parser: Syntax error?! "  */
    }
    if(wort_sepr(s,'^',TRUE,w1,w2)>1) {
      if(strlen(w1)) return(pow(parser(w1),parser(w2)));    /* von rechts !!  */
      else { printf("Paser Syntax Error: <%s>",w2); return(0); } /* "Parser: Syntax error?! "  */
    }
  }
  if(*s=='(' && s[strlen(s)-1]==')')  { /* Ueberfluessige Klammern entfernen */
    s[strlen(s)-1]=0;
    return(parser(s+1));
    /* SystemFunktionen Subroutinen und Arrays */
  } else {
    pos=searchchr(s, '(');
    if(pos!=NULL) {
      pos2=s+strlen(s)-1;
      *pos++=0;

      if(*pos2!=')')  printf("ERROR: Parser Syntax Error: <%s> missing closing parenthesis!",w2); /* "Parser: Syntax error?! "  */
      else {
        *pos2=0;

        /* Benutzerdef. Funktionen */
        if(*s=='@') return(0);
	else {
	  /* Liste durchgehen */
	  int i=0,a=anzpfuncs-1,b,l=strlen(s);
          for(b=0; b<l; b++) {
            while(s[b]>(pfuncs[i].name)[b] && i<a) i++;
            while(s[b]<(pfuncs[a].name)[b] && a>i) a--;
            if(i==a) break;
          }
          if(strcmp(s,pfuncs[i].name)==0) {
	     /* printf("Funktion %s gefunden. Nr. %d\n",pfuncs[i].name,i); */
	      if((pfuncs[i].opcode&FM_TYP)==F_SIMPLE || pfuncs[i].pmax==0) {
	        return((pfuncs[i].routine)());
	      } else if((pfuncs[i].opcode&FM_TYP)==F_ARGUMENT) {
	      	return((pfuncs[i].routine)(pos));
	      } else if(pfuncs[i].pmax==1 && (pfuncs[i].opcode&FM_TYP)==F_DQUICK) {
	      	return((pfuncs[i].routine)(parser(pos)));
	      } else if(pfuncs[i].pmax==1 && (pfuncs[i].opcode&FM_TYP)==F_IQUICK) {
	      	return((pfuncs[i].routine)((int)parser(pos)));
	      } else if(pfuncs[i].pmax==2 && (pfuncs[i].opcode&FM_TYP)==F_DQUICK) {
	       	 char w1[strlen(pos)+1],w2[strlen(pos)+1];
	         int e;
		 double val1=0,val2=0;
	         if((e=wort_sep(pos,',',TRUE,w1,w2))==1) {
		   printf("ERROR: Wrong number of parameters <%s>\n",pos); /* Falsche Anzahl Parameter */
		   val1=parser(w1); 
	         } else if(e==2) {
	           val1=parser(w1); 
		   val2=parser(w2);
	         }
                return((pfuncs[i].routine)(val1,val2));
	      } else if(pfuncs[i].pmax==4 && (pfuncs[i].opcode&FM_TYP)==F_DQUICK) {
	       	 char w1[strlen(pos)+1],w2[strlen(pos)+1];
	       	 char w3[strlen(pos)+1],w4[strlen(pos)+1];
	         int e;
		 double val1=0,val2=0,val3=0,val4=0;
	         if((e=wort_sep(pos,',',TRUE,w1,w2))==1) {
		   printf("ERROR: Wrong number of parameters <%s>\n",pos); /* Falsche Anzahl Parameter */
		   val1=parser(w1); 
	         } else if(e==2) {
	           val1=parser(w1); 
		   wort_sep(w2,',',TRUE,w2,w3);
		   val2=parser(w2);
		   wort_sep(w3,',',TRUE,w3,w4);
		   val3=parser(w3);
		   val4=parser(w4);
	         }
                return((pfuncs[i].routine)(val1,val2,val3,val4));
	      } else if(pfuncs[i].pmax==5 && (pfuncs[i].opcode&FM_TYP)==F_DQUICK) {
	       	 char w1[strlen(pos)+1],w2[strlen(pos)+1];
	       	 char w3[strlen(pos)+1],w4[strlen(pos)+1],w5[strlen(pos)+1];
	         int e;
		 double val1=0,val2=0,val3=0,val4=0,val5=0;
	         if((e=wort_sep(pos,',',TRUE,w1,w2))==1) {
		   printf("ERROR: Wrong number of parameters <%s>\n",pos); /* Falsche Anzahl Parameter */
		   val1=parser(w1); 
	         } else if(e==2) {
	           val1=parser(w1); 
		   wort_sep(w2,',',TRUE,w2,w3);
		   val2=parser(w2);
		   wort_sep(w3,',',TRUE,w3,w4);
		   val3=parser(w3);
		   wort_sep(w4,',',TRUE,w4,w5);
		   val4=parser(w4);
		   val5=parser(w5);
	         }
                return((pfuncs[i].routine)(val1,val2,val3,val4,val5));
	      } else if(pfuncs[i].pmax==2 && (pfuncs[i].opcode&FM_TYP)==F_IQUICK) {
	       	 char w1[strlen(pos)+1],w2[strlen(pos)+1];
	         int e;
		 int val1=0,val2=0;
	         if((e=wort_sep(pos,',',TRUE,w1,w2))==1) {
		   printf("ERROR: Wrong number of parameters <%s>\n",pos); /* Falsche Anzahl Parameter */
		   val1=(int)parser(w1); 
	         } else if(e==2) {
	           val1=(int)parser(w1); 
		   val2=(int)parser(w2);
	         }
                return((pfuncs[i].routine)(val1,val2));
	      } else printf("Interner ERROR. Funktion nicht korrekt definiert. %s\n",s);
	   /* Nicht in der Liste ? Dann kann es noch ARRAY sein   */	
	  } else { printf("No arrays possible! %s\n",s); return(0); }  /* Feld nicht dimensioniert  */
        }
      }
    } else {  /* Also keine Klammern */
      /* Nach Symbolen suchen. Wenn kein Symbol, dann Zahl. */
      if((vnr=ips_set(s))!=-1) return(ips[vnr].value);
      else return(myatof(s));  /* Jetzt nur noch Zahlen (hex, oct etc ...)*/
    }
  }
  printf("ERROR: Parser Syntax error. <%s>\n",s); /* Syntax error */
  return(0);
}
