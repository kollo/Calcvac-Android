LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := vacline
LOCAL_SRC_FILES := vacline.c  utility.c  parser.c  mathematics.c main.c
LOCAL_CFLAGS := -fsigned-char -fPIE
LOCAL_LDFLAGS+= -pie

LOCAL_LDLIBS    := -lm 
LOCAL_STATIC_LIBRARIES  = lapack

include $(BUILD_EXECUTABLE)
