/*  Erstellt: Jan. 2009  				   **
*/
/* This file is part of VACLINE, pressure profile calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details
 */
#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr
#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
#define isign(a) ((a>=0)?1:-1)
#define max(a,b) ((a>b)?a:b)
#define min(a,b) ((a<b)?a:b)
#define TRUE 1
#define FALSE 0
#define SQR(a) ((a)*(a))
#define PI       3.141592653589793

/* Define ANDROID if this should be used with the android f2c lib*/

#define ANDROID 1

typedef struct {
#ifdef ANDROID
  long int rows;
  long int cols;
#else
  int rows;
  int cols;
#endif
  double *pointer;
} MATRIX;

#define MAT(m,a,b) ((m.pointer)[a+(b)*m.rows])


#ifdef __cplusplus
extern "C" {
#endif
MATRIX create_matrix(int rows,int cols);
void clear_matrix(MATRIX *a);
void free_matrix(MATRIX *a);
MATRIX solve(MATRIX,MATRIX);
MATRIX SVD(MATRIX a, MATRIX *w, MATRIX *v);
MATRIX backsub(MATRIX singulars, MATRIX u, MATRIX v, MATRIX b);
#ifdef __cplusplus
}
#endif
