/* This file is part of VACLINE, the pressure calculator (c) Markus Hoffmann
 * ============================================================
 * VACLINE is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 */


#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include "utility.h"



 /* Diese Funktion gibt zurueck
   0 Wenn der String leer ist
     w1$=w2$=""
   1 wenn der String t$ nur aus einem Wort
     besteht. w2$="" w1$=t$
   2 wenn der String t$ aus mehreren Worten
     besteht. w1$=erstes Wort
     w2$= Rest von t$
  ##########################################
  der String in t wird nicht veraendert,
  w1 und w2 sind Stringbuffer, die mindestens so gross wie t sein muessen
  
   Befindet sich ein Teil des Strings in 
   Anf�hrungszeichen ("), so wird dieser Teil
   nicht ber�cksichtigt.
   wird klam! mit True angegeben, wird
   auch der Inhalt von Klammern () nicht ber�cksichtigt

   Die W�rter werden als solche erkannt, wenn
   sie durch das Zeichen c getrennt sind.

   wort_sep2 hat einen string als separator
   wort_sepr und wort_sepr2 suchen rueckwaerts

   (c) Markus Hoffmann 1997

   2012: memories now may overlap and t==w2 or t==w1 is allowed.

  */
 
int wort_sep (char *t,char c,int klamb ,char *w1, char *w2)    {
  int f=0, klam=0;

  /* klamb=0 : keine Klammern werden beruecksichtigt
     klamb=1 : normale Klammern werden beruecksichtigt
     klamb=2 : eckige Klammern werden beruecksichtigt
     klamb=4 : geschweifte Klammern werden beruecksichtigt
  */

  if(!(*t)) return(*w1=*w2=0);
  while(*t && (*t!=c || f || klam>0)) {
    if(*t=='"') f=!f;
    else if(!f && (((klamb&1) && *t=='(') || ((klamb&2) && *t=='[') || ((klamb&4) && *t=='{'))) klam++;
    else if(!f && (((klamb&1) && *t==')') || ((klamb&2) && *t==']') || ((klamb&4) && *t=='}'))) klam--;
    *w1++=*t++;
  }
  if(!(*t)) {
    *w2=*w1=0;
    return(1);
  } else {
    *w1=0;
    ++t;
    while(*t) *w2++=*t++;
    *w2=0; 
    return(2);
  }
}

int wort_sepr(char *t,char c,int klamb ,char *w1, char *w2)    {
  register int i;
  int f=0, klam=0;
  
  if(strlen(t)==0) {w1[0]=w2[0]=0;return(0);}
  i=strlen(t)-1;
  while(i>=0 && (t[i]!=c || f || klam<0)) {
    if(t[i]=='"') f=!f;
    else if(t[i]=='(' && klamb && !f) klam++;
    else if(t[i]==')' && klamb && !f) klam--;
    i--;
  }
  strcpy(w1,t);
  if(i<0) {w2[0]=0;return(1);} 
  else {w1[i]=0;strcpy(w2,t+i+1);return(2);}
}


int wort_sep2(char *t,char *c,int klamb ,char *w1, char *w2)    {

  int f=0, klam=0, i=0, j=0;


  if(!*t) return(*w1=*w2=0);   /* hier gibts nix zu trennen */
  else if(strlen(t)<=strlen(c)) {
    strcpy(w1,t);*w2=0;
    return(1);
  }
  for(;;) {
/* suche erstes Vorkommen von c */
    while(t[i] && (t[i]!=c[0] || f || klam>0)) {
      if(t[i]=='"') f=!f;
    else if(!f && (((klamb&1) && t[i]=='(') || ((klamb&2) && t[i]=='[') || ((klamb&4) && t[i]=='{'))) klam++;
    else if(!f && (((klamb&1) && t[i]==')') || ((klamb&2) && t[i]==']') || ((klamb&4) && t[i]=='}'))) klam--;
      w1[j++]=t[i++];
    }

    if(t[i]==0) { /* schon am ende ? */
      *w2=0;
      w1[j]=0;
      return(1);
    } else {     /* ueberpruefe, ob auch der Rest von c vorkommt */
    
      if(strncmp(t+i,c,strlen(c))==0) {
        w1[j]=0;
        strcpy(w2,t+i+strlen(c));
        return(2);
      } else {
        if(t[i]=='"') f=!f;
        else if(t[i]=='(' && klamb && !f) klam++;
        else if(t[i]==')' && klamb && !f) klam--;
        w1[j++]=t[i++];
      }     /* ansonsten weitersuchen */
    }
  }
}

/* Spezielle Abwandlung zum erkennen von Exponentialformat */

static int is_operator(char c) {
  return(!(strchr("|&~!*/+-<>^ =",c)==NULL));
}

int wort_sep_e(char *t,char c,int klamb ,char *w1, char *w2)    {
  int f=0, klam=0,i=0,skip=0,zahl=0;

  if(!(*t)) return(w1[0]=w2[0]=0); 
  while(*t && (*t!=c || f || klam>0 || skip)) {
    skip=0;
    if(*t=='"') f=!f;
    else if(!f && (((klamb&1) && *t=='(') || ((klamb&2) && *t=='[') || ((klamb&4) && *t=='{'))) klam++;
    else if(!f && (((klamb&1) && *t==')') || ((klamb&2) && *t==']') || ((klamb&4) && *t=='}'))) klam--;
    else if((*t=='E' ||*t=='e' ) && klam==0 && !f && zahl) skip=1;
    if(i==0 || is_operator(*(t-1))) zahl=1;
    zahl=(!(strchr("-.1234567890",*t)==NULL) && zahl); 
    *w1++=*t++;
    i++;
  }
  if(!*t) {
    *w2=*w1=0;
    return(1);
  } else {
    *w1=0;
    strcpy(w2,t+1);
    return(2);
  }
}

int wort_sepr_e(char *t,char c,int klamb ,char *w1, char *w2)    {
  register int i;
  int f=0, klam=0,j,zahl=0;
  
  if(*t==0)  return(w1[0]=w2[0]=0);
  i=strlen(t)-1;
  while(i>=0) {
    if(t[i]!=c || f || klam<0) {
      if(t[i]=='"') f=!f;
    else if(!f && (((klamb&1) && t[i]=='(') || ((klamb&2) && t[i]=='[') || ((klamb&4) && t[i]=='{'))) klam++;
    else if(!f && (((klamb&1) && t[i]==')') || ((klamb&2) && t[i]==']') || ((klamb&4) && t[i]=='}'))) klam--;
      if(i==strlen(t)-1 || is_operator(t[i+1])) zahl=1;
      zahl=(!(strchr("1234567890",t[i])==NULL) && zahl); 
    } else {
      if(!zahl || i<2 || (t[i-1]!='E' && t[i-1]!='e'  )) break;
      j=i-2;
      while(j>=0 && !(strchr("1234567890.",t[j])==NULL)) j--;
      if(j>=0 && !is_operator(t[j])) break;
    }
    i--;
  }
  strcpy(w1,t);
  if(i<0) {*w2=0;return(1);}
  else {w1[i]=0;strcpy(w2,t+i+1);return(2);}
}

int wort_sepr2(char *t,char *c,int klamb ,char *w1, char *w2)    {
  register int i;
  int f=0, klam=0;


  if(!*t)  return(*w1=*w2=0);  /* hier gibts nix zu trennen */
  else if(strlen(t)<=strlen(c)) {
    strcpy(w1,t);w2[0]=0;
    return(1);
  }
  i=strlen(t)-1;
  for(;;) {
/* suche erstes Vorkommen von c */
    while(i>=0 && (t[i]!=c[strlen(c)-1] || f || klam<0)) {
      if(t[i]=='"') f=!f;
    else if(!f && (((klamb&1) && t[i]=='(') || ((klamb&2) && t[i]=='[') || ((klamb&4) && t[i]=='{'))) klam++;
    else if(!f && (((klamb&1) && t[i]==')') || ((klamb&2) && t[i]==']') || ((klamb&4) && t[i]=='}'))) klam--;
    i--;
    }
    
    if(i<0) { /* schon am ende ? */
      strcpy(w1,t);
      *w2=0;
      return(1);
    } else {     /* ueberpruefe, ob auch der Rest von c vorkommt */
    
      if(strncmp(t+i-strlen(c)+1,c,strlen(c))==0) {
        strcpy(w1,t);
	w1[i-strlen(c)+1]=0;
        strcpy(w2,t+i+1);
        return(2);
      } else {
        if(t[i]=='"') f=!f;
        else if(t[i]=='(' && klamb && !f) klam++;
        else if(t[i]==')' && klamb && !f) klam--;
        i--;
      }     /* ansonsten weitersuchen */
    }
  }
}

/* Returns the length of the open file n */

size_t lof(FILE *n) {	
  size_t laenge,position;
	
  position=ftell(n);
  if(fseek(n,0,2)==0){
    laenge=ftell(n);
    if(fseek(n,position,0)==0)  return(laenge);
  }
  return(-1);
}


/* Returns the eof condition of an open file n */

int myeof(FILE *n) {
  int c=fgetc(n);
  ungetc(c,n);
  return c==EOF;
}


int exist(char *filename) {
  struct stat fstats;
  int retc=stat(filename, &fstats);
  if(retc==-1) return(FALSE);
  return(TRUE);
}

/*
Es werden beliebig lange Zeilen eingelesen, dafuer wird der buffer mit realloc
vergroessert, wenn noetig. Zurueckgegeben wird Zeile als STRING. 
Liest bis \n oder eof oder 0. Die Laenge kann deshalb mit strlen ermittelt
werden. Zeile wird Nullterminiert
*/
#define MAXSTRLEN 256;
char *longlineinput(FILE *n) {   /* liest eine ganze Zeile aus einem ASCII-File ein */
  int c; int i=0;
  char *line;
  int l=MAXSTRLEN;
  line=malloc(l+1);
  
  while((c=fgetc(n))!=EOF) {
    if(c==(int)'\n') {
      if(i>0 && (line[i-1]=='\\' || line[i-1]=='&')) i--;
      else break;
    } else if(c==0) break;
    else {
      line[i++]=(char)c;
      if(i>l) {
        l+=MAXSTRLEN;
        line=realloc(line,l+1);
      }
    }
  }
  line[i]='\0';
  return(line);
}
/*  (c) Markus Hoffmann
  ' Teile in Anf�hrungszeichen werden nicht ver�ndert
  ' Ersetzt Tabulator durch Leerzeichen
  ' mehrfache Leerzeichen zwischen W�rtern werden entfernt
  ' wenn fl&=1 ,  gib Gro�buchstaben zur�ck
 */
 
void xtrim(char *t,int f, char *w ) {
  register int a=0,u=0,b=0;

  while(*t) {
    while(*t && (!isspace(*t) || a)) {
      if(*t=='"') a=!a;
      u=1; if(b==1) {*w++=' '; b=0;}
      if(f && !a) *w++=toupper(*t++); else *w++=*t++;
    }
    if(u && !b) b=1;
    if(*t) t++;
  } *w=0;
}


void xtrim2(char *t,int f, char *w ) {
  register int a=0,u=0,b=0,z=0,z2=0;
  const char solo[]=",;+-*/^'(~@<>=";
  const char solo2[]=";*/^')<>=";

  while(*t) {
    while(*t && (!isspace(*t) || a)) {
      if(*t=='"') a=!a;
      u=1; 
      z2=(strchr(solo2,*t)!=NULL);
      if(z2 && b) b=0;
      if(b) {*w++=' '; b=0;}
      z=(strchr(solo,*t)!=NULL);
      if(f && !a) *w++=toupper(*t++); 
      else *w++=*t++;
    }
    if(u && !b) b=1;
    if(b && z) b=0;
    if(*t) t++;
  } 
  *w=0;
}

char *searchchr(char *buf, char c) {
 int f=0;
  while(*buf) {
    if(*buf=='"') f= !f;
    if(*buf==c && !f) return(buf);
    buf++;
  }
  return(NULL);
}
char *searchchr2(char *buf, char c) { /*( Auch Klammerungen beruecksichtigen ! */ 
 int f=0,klam=0;
  while(*buf!=0) {
    if(*buf=='"') f= !f;
    else if(*buf=='('  && !f) klam++;
    if(*buf==c && !f && !(klam>0)) return(buf);
    if(*buf==')'  && !f) klam--;
    buf++;
  }
  return(NULL);
}
char *searchchr2_multi(char *buf, char *c) { 
 int f=0,klam=0;
  while(*buf!=0) {
    if(*buf=='"') f= !f;
    else if(*buf=='('  && !f) klam++;
    if((strchr(c,*buf)!=NULL) && !f && !(klam>0)) return(buf);
    if(*buf==')'  && !f) klam--;
    buf++;
  }
  return(NULL);
}

char *getname(char *buf, char *name) {
  char *p1;
  if(!(p1=strstr(buf,":"))) return(0);
  strncpy(name, buf, p1-buf); name[p1-buf] = '\0';
  xtrim2(name,0,name);
  return( p1+1);
}

static const char del[] = {',',';','\0','\n'};
#define NDEL 4
int getpar( char *ss, char *s1, char *par) {
  int klam=0;
  char *p1, *p2;        	    //finds string s1 in ss and gives back the
      			    //following symbolic or const parameter
			    //if not the return val is 0
  p1 = strstr(ss,s1);         //delimiter is ',;\0\n'
  if(p1) {
    p1 += strlen(s1);
    //after '=' sign
    for (p2=p1; ; p2++) {
      if(*p2=='(') klam++;
      else if(*p2==')') klam--;
      else if(klam==0 && strchr(del,*p2)) break;
      else if(*p2==0) break;
    }
    strncpy(par,p1,(p2-p1)); *(par+(p2-p1)) = '\0';
    return( 1); 
  } else {
    *par = '\0'; 
    return(0); 
  }
}


int getbrak( char *ss, char *s1, char *par) {
  char *p1, *p2;            //finds string s1 in ss and gives back the
      			    //following symbolic or const parameter
			    //if not the return val is 0
  p1 = strstr(ss,s1);         //delimiter is ',;\0\n'
  if (p1) {
    p1 += strlen(s1);
    if (*p1!='(') {
      printf("Bracket '(' missing!\n"); 
      exit(-1);
    }
    p1++;
    p2 = strchr(p1,')');
    if(!p2) {
      printf("Bracket ')' missing: <%s>\n",p1); 
      exit(-1);
    }
    strncpy(par,p1,(p2-p1)); 
    *(par+(p2-p1)) = '\0';
    return(1); 
  } else {
    *par = '\0'; 
    return(0); 
  }
}
void unbrak(char *t) {
  if(*t=='(') {
    while(t[1]!=')' && t[1]!=0) {
      *t=t[1]; t++;
    }
    *t=0;
  }
}

