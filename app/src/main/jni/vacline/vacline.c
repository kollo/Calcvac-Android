/* VACLINE.C 

   (c) Mike Seidel, Markus Hoffmann 


   ##     ##   ##       ####   ##     ##  ##   ##  ######
   ##     ##  ## ##    ##  ##  ##     ##  ###  ##  ##
    ##   ##   ## ##   ##       ##     ##  ###  ##  ##
    ##   ##  #######  ##       ##     ##  #### ##  ####
     ## ##   ##   ##  ##       ##     ##  ## ####  ##
     ## ##  ##     ##  ##  ##  ##     ##  ##  ###  ##
      ##    ##     ##   ####   ###### ##  ##   ##  ######
    
    
    
                       VERSION 2.01

        (C) 2000-2003 by Mike Seidel, Markus Hoffmann
        (c) 2003-2018 by Markus Hoffmann
       (mike.seidel@desy.de, markus.hoffmann@desy.de)
   
  --> Fehler behoben für 64bit version 2018 MH
*/
/* This file is part of VACLINE, pressure profile calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

/* Bemerkung:
 * Der code kann noch erheblich optimiert werden, wenn 
 * vacline herausfindet, welche Elemente gut vertraeglich zusammengefasst 
 * werden koennen ohne numerisch instabil zu werden.
 * Die kann die Matrix erheblich verkleinern.
 * Die restlichen Druecke werden dann durch einfaches Tracking 
 * berechnet.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef WINDOWS
#define EX_OK 0
#define EX_IOERR -2
#define EX_NOINPUT -1
#else
#include <sysexits.h>
#endif

#include "mathematics.h"
#include "vacline.h"
#include "utility.h"
#include "parser.h"

extern int verbose;
extern int nplot;
extern double resolution;

char *title=NULL;  /* Projekttitel */

int use;

/* some constants defining the semantics */
const char *e_names[]={"START","SECTION","MARKER","LINE","END"};

//vacuum parameters
const char *v_parms[]={"P","s","Qj"};
//parms for start
const char *start_parm[]= {"W0","S0"};
//mandatory flag, if =1 parameter must be defined
const char start_mand[] = {   0};
const char *sect_parm[] = {"L","q","Ps","d","A","W"};
const char sect_mand[]  = {  1,  0,   0,  0,  0, 0};
const char *mark_parm[] = {};
const char mark_mand[]  = {};
const char *end_parm[] = {"W0"};
const char end_mand[]  = {  0};

//number of parameters for types
const int n_parm[] = {sizeof(start_parm)/sizeof(char *),
		      sizeof(sect_parm)/sizeof(char *), 
                      0, 0, sizeof(end_parm)/sizeof(char *)};
const char *mand_list[] ={start_mand, sect_mand, 0, 0, end_mand};
const char **parm_list[] ={start_parm, sect_parm, 0, 0, end_parm};
const int n_types = sizeof(n_parm)/sizeof(int);

const char *out_parms[] = {"s","P","Q","W","q","sp","Pavg"};


double *fit_list[25]; char *fit_goals[50];
int n_fit_goals=0;
int n_fit=0;

LINK links[MAX_LINKS];
int nlinks=0;

BOUNDARY boundaries[MAX_BOUNDARY];
int nboundary=0;

double S0;
static int trafo(ELEMENT *elem,double l1, double c1, double q1, double alpha);
static void AddLink(int l1,int l2);

/* File einlesen und Zeilenweise prozessieren.*/

void Load_VacLine(char *fname) {
  FILE *dptr;
  int anzlines=0;
  char *line;
  
  if(verbose>0) printf("<-- %s [",fname);
  dptr=fopen(fname,"r");
  if(dptr==NULL) {
    printf("\nERROR: could not open file %s.\n",fname);
    exit(EX_NOINPUT);
  }
  while(!myeof(dptr)) {
    line=longlineinput(dptr);
    xtrim2(line,0,line);
    processline(line);
    free(line);
    anzlines++;
  }
  if(verbose>0) printf("] %d lines read.\n",anzlines);
  fclose(dptr);
}





SYMBOL ips[MAX_ELEMENTS*10+MAX_SYMBOLS];
int anzips=0;
IPD ipd[MAX_ELEMENTDES];
int anzipd=0;
ELEMENT ipe[MAX_ELEMENTS];
int anzipe=0;

int ips_set(char *name) {
  int i;
  if(anzips) {
    for(i=0;i<anzips;i++) {
      if(!strcmp(name,ips[i].name)) return(i);
    }
  }
  return(-1);
}
int ipd_set(char *name) {
  int i;
  if(anzipd) {
    for(i=0;i<anzipd;i++) {
      if(!strcmp(name,ipd[i].name)) return(i);
    }
  }
  return(-1);
}

int ips_Add(char *symbol,char *name) {
  int id;
  if((id=ips_set(name))!=-1) {
    printf("WARNING: symbol already defined. It will be overwritten.\n");
  }
  if(id==-1) id=anzips++;
  ips[id].name=strdup(name);
  ips[id].txt=strdup(symbol);
  ips[id].flags=0;
  return id;
}
static void SetSymbol(char *name, char *txt) { //reset a Symbols value
  int id;
  if ((id=ips_set(name))==-1) {
    printf("ERROR: Symbol %s not found!\n",name);
    exit(EX_DATAERR);
  }
  free(ips[id].txt);
  ips[id].txt=strdup(txt);
}

/* Element Beschreibung aufnehmen, aber noch nicht evaluieren.*/

int AddElemDescr(int typ,char *c1,char *buf){
  char tmp[10000];
  char b1[80], b2[80],sname[80];
  int i;
  
  int id=ipd_set(c1);  /* Schauen, ob wir den Namen schon haben...*/
  
  if(id!=-1) {
    printf("WARNING: redefinition of %s, will be overwritten.\n",c1);
    free(ipd[id].name);
    free(ipd[id].txt);
  } else id=anzipd++;
  ipd[id].name=strdup(c1);
  ipd[id].txt=strdup(buf);
  ipd[id].typ=typ;
  
  if(typ==V_LINE) return(id);  /* Dann sind wir erstmal fertig hier....*/

  /*now normal elements  */
  
  if (!getpar(buf,"TNAME=",tmp)) {
    strcpy(tmp,c1);
    ipd[id].tname=strdup(tmp);
  }
  if(getbrak(buf,"BOUNDARY",tmp)) {
#if DEBUG
    printf("BOUNDARY: ---> %s \n",tmp);
#endif
    unbrak(tmp);
    while(wort_sep(tmp,',',1,b1,tmp)) {
      if(wort_sep(b1,'=',1,b1,b2)<2) printf("ERROR: BOUNDARY Syntax error.\n");
#if DEBUG
      printf("Typ: %s, Val=%s \n",b1,b2);
#endif
      if(*b1=='P') {ipd[id].boundary_typ|=1;ipd[id].bval1=parser(b2);}
      else if(*b1=='Q') {ipd[id].boundary_typ|=2;ipd[id].bval2=parser(b2);}
      else printf("ERROR: unknown boundary type %s.\n",b1);
    }
  }
  for(i=0; i<n_parm[typ]; i++) {
    strcpy(b1, parm_list[typ][i]); strcat( b1, "=");
    strcpy(sname,c1); strcat(sname, "."); strcat(sname, parm_list[typ][i]);
    if(!getpar(buf,b1,tmp)) { 
      if( mand_list[typ][i]) {
        printf("ERROR: parameter %s is mandatory in element %s of type %s.\n",parm_list[typ][i],c1,e_names[typ]);
        exit(EX_DATAERR); 
      }
      else {
        int id2=ips_Add("0",sname);
	// printf("Symbol: <%s> set to 0, #%d\n",sname,i); 
        ipd[id].vpointer[i] = &(ips[id2].value);
      }
    } else {
      int id2=ips_Add(tmp, sname); 
      ips[id2].value = 0;
      // printf("Symbol: <%s> set to <%s>, #%d\n",sname,tmp,i); 
      ipd[id].vpointer[i] = &(ips[id2].value);
      if (strstr(buf,"FITRULE")) ips[id2].flags |= FITRULE;
    } 
  }
  ipd[id].nparms = i;
  return(id);
}


/* Vaclines nun auflösen....*/
void solve_lines() {
  int i,k,e,iflag,itmp,itmp2;
  int tlist[MAX_IN_LINE];
  char *p3;
  if(verbose>1) printf("Solve Lines:\n");
  for(i=0;i<anzipd;i++) {
    if(ipd[i].typ==V_LINE) { /* Eine Vakuum-Line aufloesen */
      char *buf=ipd[i].txt;
      char tmp[strlen(buf)];
      char tmp1[strlen(buf)];
      getbrak(buf,"LINE=",tmp);
      k=0;
      e=wort_sep(tmp,',',0,tmp1,tmp);
      while(e) {
        xtrim2(tmp1,0,tmp1);
        if(!(p3=strchr(tmp1,'*'))) {
          if(*tmp1=='-') {
	   DropC(tmp1); 
	   iflag = -1; //reverse order,   give minus sign
	  } else iflag = 1;
          if((itmp2=ipd_set(tmp1))==-1) {
	    printf("ERROR: line element not found: <%s>\n",tmp1);
	    exit(EX_DATAERR); 
	  } else {
	    tlist[k++] = iflag*itmp2; //add element to sublist
	    if(verbose>1) printf("Add element %s to line.\n",ipd[itmp2].name);
	  }
        } else { //multiple elements
          *p3='\0';
          itmp = atoi(tmp1);
          if (itmp<2) {
            printf("ERROR:  Strange multiple element number.\n");
	    exit(EX_DATAERR);
	  }
          if((itmp2=ipd_set(p3+1))==-1) {
	    printf("ERROR: line element not found: <%s>\n",tmp1);  
	    exit(EX_DATAERR);
	  }
	  int j;
          for(j=0; j<itmp; j++) tlist[k++]=itmp2;
        } 
        e=wort_sep(tmp,',',0,tmp1,tmp);
      }
      if(verbose) printf("INFO: Line %s consists of %d elements.\n",ipd[i].name,k);
      ipd[i].liste=malloc(k*sizeof(int));
      int ii;
      for(ii=0;ii<k;ii++) ipd[i].liste[ii]=tlist[ii];
      ipd[i].nparms=k;    
    }
  }
}




/* Verarbeitet eine Zeile aus dem EIngabefile*/

void processline(char *line) {
  char *p1,*p2;
  char name[50],b1[200],b2[200];
  int ne;
  
  if(*line==0) ;
  else if(*line=='!') ;
  else if(*line=='#') ;
  else {
    // printf("%s\n",line);
    if((p1=getname(line,name))) {   /* Zeilen der Art "Name: Parameter"*/
      for(ne=0; ne<n_types; ne++) {
        if((p2=strstr(line,e_names[ne]))!=0) break;
      }
      if (ne==n_types) { //must be symbol
        int id;
        if (!(p1 = strstr(p1,"="))) {
	  printf("ERROR: '=' sign missing in <%s>\n",line);
          exit(EX_DATAERR);
	}
        strcpy(b1,p1+1); 
	xtrim2(b1,0,b1);
        id=ips_Add(b1,name); //add Symbol to List
        if ((p1=strstr(line,"FITRULE")))  //add fitrule flag
          ips[id].flags &= FITRULE;
      } else { //must be element
        AddElemDescr(ne,name,line); 
      }
    } else {     // other definitions
      if(getpar(line,"INCLUDE,",b1)) Load_VacLine(b1);
      else if(getpar(line,"USE,",b1)) {//element for line to be used
	   if ((use=ipd_set(b1))==-1) {
	     printf("ERROR: unknown element for USE: <%s>\n",b1);
	     exit(EX_DATAERR);
	   }
      } else if(getpar(line,"SET,",b1)) { //set new symbol value
	    if (!(p1 = strstr(b1,"="))) {
	       printf("ERROR: '=' sign missing in <%s>\n",line);
               exit(EX_DATAERR);
	    }
            strncpy(name,b1,p1-b1); 
	    name[p1-b1]='\0';
            strcpy(b2,p1+1); 
	    xtrim2(b2,0,b2);
            SetSymbol(name,b2);
      } else if(wort_sep(line,',',1,b1,b2)) {
	   if(strcmp(b1,"TITLE")==0) {
	     title = malloc(strlen(b2)+1);
	     strcpy(title,b2);
	   } else if (strcmp(b1,"LINK")==0) {
	     int l1=-1,l2=-1;
             if(wort_sep(b2,',',1,b1,b2)<2) {
	       printf("LINK: Syntax error.\n");
	       exit(EX_DATAERR);
	     }
	     if((l1=ipd_set(b1))==-1) {
	       printf("ERROR: unknown element for LINK: <%s>\n",b1);
               exit(EX_DATAERR);
	     }
	     if((l2=ipd_set(b2))==-1) {
	       printf("ERROR: unknown element for LINK: <%s>\n",b2);
	       exit(EX_DATAERR);
	     }
	     /* Hier ist jetzt noch nicht bekannt, wo genau die verlinkten Elemente 
	     nachher positioniert sind. */
             if(verbose>1) printf("LINK: %s(#%d) <--> %s(#%d)\n",b1,l1,b2,l2);
	     AddLink(l1,l2);
	   }
      }
      if(line==strstr(line,"VTWISS"))  //start parms: VTWISS in first chars
	    AddElemDescr(V_START,"START",line); 
      /* Kommandos fuer das Fitten einlesen.*/
      if(getbrak(line,"FITLIST=",b1)) { //list of elements to fit
        int id;
        *(b1+strlen(b1)+1) = '\0'; 
	*(b1+strlen(b1)) = ',';
        p1 = b1;
        for (n_fit = 0; n_fit<25; n_fit++) {
          p2 = strchr(p1,','); 
          if (!p2) break; //end reached
          *p2 = '\0';
          if((id=ips_set(p1))==-1) {
            printf("ERROR: symbol to fit '%s' not found!\n",p1);
            exit(EX_DATAERR); 
	  } else {
            ips[id].flags &= FIT;
            fit_list[n_fit] = &(ips->value);
	  }
          p1=p2+1;
        }
      }
      if((p1=strstr(line,"FIT,"))) {  //add value for fitgoal
         p1 += strlen("FIT,");
         fit_goals[n_fit_goals] = strdup(p1);
         n_fit_goals++;
      }      
    }
  }
}


void Eval_Symbols(int flag) {
  int i;
  if(flag==0) {          //eval all symbols        
    for(i=0;i<anzips;i++) {    
      ips[i].value = parser(ips[i].txt);  
    }
  } else { //eval only symbs with flag (faster)
    for(i=0;i<anzips;i++) {    
      if(ips[i].flags&flag) ips[i].value = parser(ips[i].txt);  
    }
  }
}
/*gemeint ist hier: Verbinde das Ende des ersten Elements (l1) mit dem ANfang
  des zweiten (l2). Wobei bei zusammengesetzten Elementen rekursiv aufgelöst werden muss.
  Es kann auch mehrere Vorkommen desselben Elements geben. Hier nehmen wir dann jeweils das erste.
  
  */

static void AddLink(int l1,int l2) {
  links[nlinks].l1=l1;
  links[nlinks].l2=l2;
  nlinks++;
}



void print_ed_info(int idx) {
  int j;
  printf("Name=%s\n",ipd[idx].name);
  printf("typ=%d\n",ipd[idx].typ);
  printf("tName=%s\n",ipd[idx].tname);
  printf("txt=%s\n",ipd[idx].txt);
  printf("nparms=%d\n",ipd[idx].nparms);
  if(ipd[idx].typ==V_LINE && ipd[idx].nparms>0) {
    printf("Besteht aus: ");
    for(j=0;j<ipd[idx].nparms;j++) {
      printf("%d ",ipd[idx].liste[j]);
    }
    printf("\n");
  }
}





/* Gibt das "twissfile" aus:
   Liste der Elemente mit ihren Parametern und Startdruck/Flow
 */


void write_twiss(char *fname) {
  if(verbose) printf("--> %s [",fname);
  int i;
  
  FILE *fp=fopen(fname,"w");
  fputs("# Ausgabe von vacline V." VERSION " (c) " DATE " by " AUTHOR "\n",fp);
  fputs("#\tpos.\tlength\tpressure\tflow\t\tconductance\to-rate\t\tpspeed\t\tp_avg\t\tarea\t\tint.area\n",fp);
  fprintf(fp,"# \t[m]\t[m]\t[mbar]\t\t[mbar l/s]\t[l m/s]\t\t[mbar l/s m]\t[l s/m]\t\t[mbar]\t\t[cm^2]\t\t[cm^2]\n");
  fprintf(fp,"# ===================================================================================================================================================\n");
  for(i=0;i<anzipe;i++) { 
    fprintf(fp,"%s\t%.3f\t%.3f\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\n",ipe[i].name,
	   ipe[i].z,ipe[i].l,ipe[i].P,ipe[i].Q,ipe[i].W,ipe[i].q,ipe[i].sp,ipe[i].Pavg,ipe[i].A,ipe[i].Ai);
    if(verbose>1) printf(".");
  }
  if(nlinks) {
    fputs("# Links: \n",fp);
    fprintf(fp,"# number start pos end pos flow \n");
    for(i=0;i<nlinks;i++) {
       links[i].pressure=ipe[links[i].l1e].P;
       fprintf(fp,"#* %d %s %g %s %g %g %g\n",i,ipd[links[i].l1].name,ipe[links[i].l1e].z+ipe[links[i].l1e].l,
       ipd[links[i].l2].name,ipe[links[i].l2e].z,
       links[i].flow,links[i].pressure);
    }
  }
  // TODO Boundaries rausschreiben....
  
  fclose(fp);
  if(verbose) printf("] (%d+%d)\n",anzipe,i);
}


// Berechne Druck und Gasfluss an beliebiger Stelle (auch innerhalb Element)

void CalcP(double s, double *p, double *q, double *c, double *o, double *ss, double *pavg, double *A) {
  double ds=0, p0, p1, ps, l0, q0,q1; 
  int dum;
  int id;
  ELEMENT el;

  for(id=0; id<anzipe; id++) {   // Suche das zu s gehoerige Element
    if((ds=s-ipe[id].z)>=0 && ds<ipe[id].l) break;
  }
  if(id==anzipe) {
    *q=*p=0;
    return;
  }
  *pavg=ipe[id].Pavg;
  *A=ipe[id].Ai;
  *ss=ipe[id].sp;
  *o=ipe[id].q;
  *c=ipe[id].W;
  if(id==0) {  /* Das erste Element ist immer ein Marker, also l=0*/
    *p=ipe[id].P;
    *q=ipe[id].Q;
    return;
  } else {
    p0 = ipe[id-1].P; /*Druck am Anfang des Elements*/
    p1 = ipe[id].P;   /*Druck am Ende des Elements*/
    q0 = ipe[id-1].Q; /*Gasfluss am Anfang des Elements*/
    q1 = ipe[id].Q;   /*Gasfluss am Ende des Elements*/
  }
  
  l0 = ipe[id].l; 
  dum=trafo(&el,ds,ipe[id].W,ipe[id].q,sqrt(ipe[id].sp/ipe[id].W));  //erzeuge Matrix
  dum=ipe[id].fl;
  if(dum==0) {
    *p=el.mm00*p0 +el.mm01*q0 +el.dd0;
    *q=el.mm10*p0 +el.mm11*q0 +el.dd1;
  } else { //special treatment for too long sections
    double alpha=sqrt(ipe[id].sp/ipe[id].W);
  //  double w0=ipe[id].W;
    ps=ipe[id].q/ipe[id].sp;
    *p=(p0-ps)*exp(-alpha*ds)+(p1-ps)*exp(-alpha*(l0-ds))+ps;
    *q=q0*exp(-alpha*ds)+q1*exp(-alpha*(l0-ds));
//printf("##%d## %g %g l=%g s=%g z=%g ",id,ps,alpha,l0,ds,ipe[id].z);
   // *p=(p0-ps)*exp(alpha*ds)+(p1-ps)*exp(-alpha*(l0+ds))+ps;
   // *q=alpha*w0*(p0-ps)*exp(alpha*ds)-alpha*w0*(ps-p1)*exp(-alpha*(l0+ds));
  }
}




/* Schreibt die .dat Datei raus mit Druckverlauf und Gas-Stroemen.*/

void write_pressure_data(char *fname) {
  FILE *fp; 
  int i=0;
  double s,s1,s2;
  double p,f,c,o,ss,pavg,A;
  if(verbose) printf("--> %s [",fname);

  s1 = S0;
  s2 = ipe[anzipe-1].z+ipe[anzipe-1].l;

  fp = fopen(fname,"w");
  fputs("% Output of vacline V." VERSION " (c) " DATE " by " AUTHOR "\n",fp);
  fputs("% position\tpressure\tflow\t\tconductance\to-rate\t\tpspeed\t\tp_avg\t\tarea\n",fp);
  fprintf(fp,"%% [m]\t\t[mbar]\t\t[mbar l/s]\t[l m/s]\t\t[mbar l/s m]\t[l s/m]\t\t[mbar]\t\t[cm^2]\n");
  fprintf(fp,"%%===================================================================================================================================\n");

  double step=max(resolution,(s2-s1)/(double )(nplot-1));

  for(i=0;i<=(int)((s2-s1)/step); i++) {
    s=step*(double)i+s1;
    if(s>=s2) break;
    CalcP(s,&p,&f,&c,&o,&ss,&pavg,&A); 
    fprintf(fp,"%f %e %e %e %e %e %e %g\n",s,p,f,c,o,ss,pavg,A);
    if(verbose>1) printf(".");
  }
  /* Hier noch die Links und zugehörigen Gasflüsse rausschreiben.*/
  if(nlinks) {
    fputs("# Links: \n",fp);
    fprintf(fp,"%% number start pos end pos flow \n");
    for(i=0;i<nlinks;i++) {
       links[i].pressure=ipe[links[i].l1e].P;
       fprintf(fp,"#* %d %s %g %s %g %g %g\n",i,ipd[links[i].l1].name,ipe[links[i].l1e].z+ipe[links[i].l1e].l,
       ipd[links[i].l2].name,ipe[links[i].l2e].z,
       links[i].flow,links[i].pressure);
    }
  }
  fclose(fp);

  if(verbose) printf("] (%d)\n",i);
}
double f_frac(double b) {return(b-((double)((int)b)));}
void writematrix(MATRIX m1,MATRIX v1) {
  int i,j;
  int m=m1.cols;
  int n=v1.rows;
  printf("lines=%d, rows=%d\n",n,m);
  if(m>20) m=20;
  if(n>20) n=20;
  for(i=0;i<n;i++) {
    printf("(");
    for(j=0;j<m;j++) {
      if(f_frac(MAT(m1,i,j))==0 && fabs(MAT(m1,i,j))<100) printf("%2d ",(int)MAT(m1,i,j));
      else if(fabs(MAT(m1,i,j)-MAT(v1,i,0))<0.1*fabs(MAT(m1,i,j))) printf(" v ");
      else if(MAT(m1,i,j)>0) printf("+* ");
      else if(MAT(m1,i,j)<0) printf("-* ");
      else  printf(" 0 ");
    }
    printf("|");
    if(f_frac(MAT(v1,i,0))==0 && fabs(MAT(v1,i,0))<100) printf("%2d ",(int)MAT(v1,i,0));
    else if(MAT(v1,i,0)>0) printf("+* ");
    else if(MAT(v1,i,0)<0) printf("-* ");
    else  printf(" 0 ");
    printf(")\n");
  }
}




void writematrix2(MATRIX m1,MATRIX v1) {
  int i,j;
  int m=m1.cols;
  int n=v1.rows;
  printf("lines=%d, rows=%d\n",n,m);
  for(i=0;i<n;i++) {
    printf("(");
    for(j=0;j<m;j++) {
      printf("%2g ",MAT(m1,i,j));
    }
    printf("|");
    printf("%2g ",MAT(v1,i,0));
    printf(")\n");
  }
}



int spalte_zu_element(int spalte) {
  int i=0,id;
  for(id=0; id<anzipe; id++) {
    if(ipd[ipe[id].indx].typ==V_SECTION) i++;
    if(2*i==spalte+2 || 2*i+1==spalte+2) return(id);
  }
  return(-1);
}



/*Aufbau der Matrix: 

*/



void CalcVacuum() {
  int i,id;
  MATRIX m1,v1,vac;
  int j,k,n,m,bon,l1=-1,l2=-1,l1k=-1,l2k=-1,anzzeilen,anzspalten,lon=0; 
  double Pavg;

  double px_s,px_P,px_W,px_Q,px_q,px_sp,px_Pavg=0,px_Ai=0;

  double p0,p1,ps,q0,c0,l0,alpha,Ai,a1;
  int lincount;
  
  
  int bound_links;  /* Zeilenzahl fuer LINK-Bedingungen*/
  
  
  
// Dimensionierung der Matrix bestimmen:
//  Fuer jedes Element, eine Zeile P, eine Zeile Q
//  Fuer n Elemente n+2 Spalten.
//  Fuer jede Randbedingung eine Zeile 
//  Fuer jeden link eine Zeile und eine Spalte
//  Die Matrix muss hinterher Quadratisch sein (evtl. kann man ueberbestimmte 
//  Gleichungen mit einer SVD Methode loesen)


/* Bestimme anzahl der Elemente und ANzahl der Randbedingungen  */

  for(i=0,bon=0,m=0; i<anzipe; i++) {
    m+=2*(ipd[ipe[i].indx].typ==V_SECTION);   // Marker, Start etc. ignorieren
    bon+=(ipd[ipe[i].indx].boundary_typ&1);
    bon+=((ipd[ipe[i].indx].boundary_typ&2)>0);
  }
  n=m;          
  m+=2+nlinks;  
  
  /*m ist voraussichtliche Zeilenzahl der Matrix (kann nur noch abweichen, wenn 
  bei den Links pathologische dabei sinf (link mit sich selbst))
    n ist zweimal die Anzahl der echten Elemente
    bon ist Anzahl der zusaetzlichen randbedingungen
    nlinks ist anzahl der Links
    2 Anfangsbedingungen kommen eh hinzu
    */
  
  if(verbose) printf("INFO: %d elements, %d boundaries, %d links --> %d variables.\n",n/2,bon,nlinks,m);
  if(bon+nlinks+n>m) printf("WARNING: Too many boundaries. Should be: %d\n",m-n-nlinks);
  if(bon+nlinks+n<m) printf("WARNING: Too few boundaries. Should be: %d\n",m-n-nlinks);

  anzzeilen=bon+nlinks+n;
  anzspalten=nlinks+n+2;
  if(verbose>1) printf("Matrix M hat %d Zeilen und %d Spalten.\n",anzzeilen,anzspalten);

  
  //reserve vectors and ma
//  vac = Mx<double>(anzspalten); 
  v1 = create_matrix(anzzeilen,1);
  m1 = create_matrix(anzzeilen,anzspalten);
  clear_matrix(&m1);
  clear_matrix(&v1);//clean up
  lincount=0;
  

  // Links transformieren:
  
  // Fuer jeden Link kommt eine Unbekannte hinzu (Der Transfer-fluss)
  // Entsprechend kommt eine Gleichung hinzu P_l1=P_l2
  // Der Transferfluss muss allerdings in den Transformationsgleichungen
  // beruecksichtigt werden.

  

  if(nlinks) {
    for(i=0;i<nlinks;i++) {
      /* Richtige Positionen in der Matrix für die Eintraege finden*/
      k=j=0;
      for (id=0; id<anzipe; id++) {
        if(id==links[i].l1e) {l1=j;l1k=k;}
        if(id+1==links[i].l2e) {l2=j;l2k=k;}
        if(ipd[ipe[id].indx].typ==V_SECTION) {j++;k+=2;}
	if(ipd[ipe[id].indx].boundary_typ&1) k++;
 	if((ipd[ipe[id].indx].boundary_typ&2)>0) k++;
      }
      if(l1<0 || l2<0) printf("ERROR: something is wrong!\n");
      
      if(l1!=l2) {  // falls nicht mit sich selbst gelinkt
        /* Bedingung fuer den Druck an beiden Stellen */
        MAT(m1,lincount,l1*2)=1;
        MAT(m1,lincount,l2*2)=-1;
	MAT(v1,lincount,0)=0;
	// printf("for LINK/P #%d: Eintrag in Spalten %d und %d (%s,%s)\n",i,l1*2,l2*2,ipe[spalte_zu_element(2*l1)].name,ipe[spalte_zu_element(2*l2)].name);
        lincount++;
      } else printf("ERROR: Link element %s with itself.\n",ipe[l1].name);
      /* Bedingung fuer den Fluss an beiden Stellen */
      // modified flow condition
      if(l1k<0 || l2k<0) printf("ERROR: something is wrong!\n");

      if(l1k>=n) printf("WARNING: LINK mit Endelement noch nicht moeglich !\n");
      else {
        MAT(m1,nlinks+l1k+1,anzspalten-nlinks+i) = -1; 
	//printf("for LINK/Q #%d: Eintrag in Zeile %d, Spalte %d \n",i,nlinks+l1k+1,anzspalten-nlinks+i);
      }
      if(l2k>=n) printf("WARNING: LINK mit Endelement noch nicht moeglich !\n");
      else {
        MAT(m1,nlinks+l2k+1,anzspalten-nlinks+i) = 1;
	//printf("for LINK/Q #%d: Eintrag in Zeile %d, Spalte %d \n",i,nlinks+l2k+1,anzspalten-nlinks+i);
	
      }
    }
  }
  
  bound_links=lincount;
  
//  printf("bound_links=%d\n",bound_links);
  
  for (id=0, i=0; id<anzipe; id++) {
 //   printf("id=%d, i=%d, elem=%d\n",id,i,spalte_zu_element(i));
    if (ipd[ipe[id].indx].boundary_typ) {
      if(verbose)  
       printf("INFO: boundary condition found at element #%d (%s) : %d\n",id,ipe[id].name,
       ipd[ipe[id].indx].boundary_typ);
      /* Für jede Boundary condition schreibe eine extra Zeile.
         0 .... 0 1 0 ... 0 | bval 
	 
	 */
      if(ipd[ipe[id].indx].boundary_typ&B_P) {
        // printf("for Boundary/P: Eintrag in Spalte %d\n",i);
        MAT(m1,lincount,i)=1; 
	MAT(v1,lincount,0)=ipd[ipe[id].indx].bval1;
	lincount++;
      }
      if((ipd[ipe[id].indx].boundary_typ&B_Q)>0) {
        /*Kollisionen finden mit LINK-Boundaries*/
	if(bound_links>0) {
	  int cnt;
          for(cnt=0;cnt<bound_links;cnt++) {
	    /* Wir suchen hier zuwar die P-Bounderies der Links, aber wissen, dass es auch eine
	       Q Bedingung dazu gibt. */
	    if(MAT(m1,cnt,i)!=0) printf("ERROR: Double Q-Boundary collision with LINK #%d\n",cnt);
	  }
        }
        // printf("for Boundary/Q: Eintrag in Spalte %d End of %s\n",i+1,ipe[spalte_zu_element(i+1)].name);
        MAT(m1,lincount,i+1)=1; 
	MAT(v1,lincount,0)=ipd[ipe[id].indx].bval2;
	lincount++;
      }
    }
    if(ipd[ipe[id].indx].typ==V_SECTION) {
      if (ipe[id].fl==0) {
        MAT(m1,lincount,i+0)   = -ipe[id].mm00;
        MAT(m1,lincount,i+1)   = -ipe[id].mm01;
	MAT(m1,lincount,i+2)   = 1;
	MAT(m1,lincount,i+3)   = 0;
	
        MAT(m1,lincount+1,i+0) = -ipe[id].mm10;
	MAT(m1,lincount+1,i+1) = -ipe[id].mm11;
	MAT(m1,lincount+1,i+2) = 0;
	MAT(m1,lincount+1,i+3) = 1;
        MAT(v1,lincount,0)   = ipe[id].dd0; 
	MAT(v1,lincount+1,0) = ipe[id].dd1; 
     } else { /* this for too long sections, where the ends dont feel each other */
        MAT(m1,lincount,i+0)   = ipe[id].mm00;
        MAT(m1,lincount,i+1)   = -ipe[id].mm01;
	MAT(m1,lincount,i+2)   = 0;
	MAT(m1,lincount,i+3)   = 0;
        MAT(m1,lincount+1,i+2) = ipe[id].mm00;
	MAT(m1,lincount+1,i+3) = ipe[id].mm01;
	MAT(m1,lincount+1,i+0) = 0;
	MAT(m1,lincount+1,i+1) = 0;
        MAT(v1,lincount,0)  =ipe[id].dd0; 
	MAT(v1,lincount+1,0)=ipe[id].dd0;
	lon++;
      }
      lincount+=2;
      i+= 2;
    }
  }
  if(verbose>1)  writematrix(m1,v1);
//  writematrix2(m1,v1);

  if(verbose) if(lon>0) printf("INFO: found %d long elements.\n",lon);


// Hier ist die Matrix zusammengebaut.
// Jetzt muss das Gleichungssystem geloest werden.

#if DEBUG
if(anzspalten==anzzeilen) {
  vac = gaussj(m1,v1); //solution vector contains altern. Pi,Qi and transfer-Flows for links
} 
#endif

  vac=solve(m1,v1);

  
  if(verbose>1) {
    for(i=0;i<min(20,vac.rows);i++) printf("VAC:%d: %g\n",i,(vac.pointer)[i]);
  }
  if(nlinks) {
    for(i=0;i<nlinks;i++) {
      links[i].flow=MAT(vac,m-nlinks+i,0);
      links[i].pressure=0;
      if(verbose>1) printf("INFO: Flow through link %d: %s<-->%s %g\n",i,
      ipd[links[i].l1].name,ipd[links[i].l2].name,
      links[i].flow);
    }
  }

  for (id=0,i=1; id<anzipe; id++) 
    if (ipd[ipe[id].indx].typ == V_SECTION) break;       //search first pipe


// Anfangswerte:
  px_s=S0;   // start position
  px_P= MAT(vac,0,0); // P am Anfang der Line
  px_Q=MAT(vac,1,0); // Q am Anfang der Line
  px_W=ipe[id].W;
  px_q=ipe[id].q; 
  px_sp=ipe[id].sp;



// tracke Positionen etc fuer die anderen Elemente durch

  for (id=0,i=1,Pavg=0,Ai=0.; id<anzipe; id++) {
    if(ipd[ipe[id].indx].typ == V_SECTION) {
      
      px_s+= ipe[id].l; // Endposition d. Elements
      p0 = px_P = MAT(vac,2*i,0);    //  Druck am Anfang d. Elements
      px_Q= MAT(vac,2*i+1,0);       //  gas flow  am Anfang
      c0 = px_W = ipe[id].W;  // spez. Leitfaehigkeit
      q0 = px_q = ipe[id].q;  // spez. outgasrate
      px_sp = ipe[id].sp;     // pump speed

      //now average pressure fuer element
      alpha = sqrt(ipe[id].sp/ipe[id].W); 
      l0 = ipe[id].l;
      if (alpha==0) //section without  pump
        px_Pavg= p0-q0/6./c0*l0*l0+px_Q/2./c0*l0;
      else { //section with pump
        if (fabs(alpha*l0)<13) { //computational accuracy ok.
	  px_Pavg= (p0-q0/c0/alpha/alpha)*sinh(alpha*l0);
	  px_Pavg+= px_Q/c0/alpha*(cosh(alpha*l0)-1);
	  px_Pavg+= q0*l0/c0/alpha;
	  px_Pavg/= alpha*l0;
        } else {
	  ps = ipe[id].q/ipe[id].sp;
	  p1 = ipe[id-1].P; //get press. from other end
	  px_Pavg= ps + (p0+p1-2*ps)/alpha/l0*(1.-exp(-alpha*l0));
        }
      }
      // Flaeche bestimmen
      if((a1=ipe[id].A)==0) a1=PI*ipe[id].d*1.e4; //in cm^2/m
      Ai+=ipe[id].l*a1; //sum area
      px_Ai= Ai;
      //cout<<ipe->parp(0)<<endl;
      Pavg += px_Pavg*l0;
      i++; 
    } else {   // Marker und so...
       // die letzten Werte werden uebertragen. s.u.
    }
    // Durchschnittsdruck bestimmen
    if(ipd[ipe[id].indx].typ == V_END) { //total average pressure at endelement
      px_Pavg= Pavg/(px_s-S0);
    }
    ipe[id].Pavg=px_Pavg; //Setze die Parameter fuer das Element
    ipe[id].Ai=px_Ai; 
    ipe[id].P=px_P; 
    ipe[id].Q=px_Q; 
  }
  Pavg /= px_s-S0; //total average pressure

  if(verbose)  printf("INFO: total average pressure: %g mbar\n",Pavg);  
}

/* Fragt ab, ob name schon vorhanden ist und gibt dann 
   index zurück, sonst -1 
 */
int ipe_set(char *name) {
  int i;
  if(anzipe) {
    for(i=0;i<anzipe;i++) {
      if(!strcmp(name,ipe[i].name)) return(i);
    }
  }
  return(-1);
}



// Erzeugt due Matrixeintraege mm und dd fuer das Element der
// laenge l1 Return flag, if accuracy is oK

static int trafo(ELEMENT *elem,double x, double c, double q, double alpha) {
  int fl=0;
  if (alpha==0.) {   /* Keine Pumpe*/
    elem->mm00=elem->mm11=1;
    elem->mm01=-x/c;   /*   sin(x)/x=1 */ 
    elem->mm10=0;
    elem->dd0=-x*x*q/2/c;
    elem->dd1=q*x;
  } else {
    if (fabs(alpha*x)<13) { //computational accuracy ok.
      double s,co;
      s=sinh(alpha*x);
      co=elem->mm11=elem->mm00=cosh(alpha*x); 
      elem->mm01=-s/c/alpha;
      elem->mm10=-c*alpha*s;
      elem->dd0=-q/c/alpha/alpha*(co-1);
      elem->dd1=q/alpha*s;
    } else {
      fl = 1; //set flag
      elem->mm00=1; 
      elem->mm01=1/c/alpha;
      elem->dd1=elem->mm10=elem->mm11=0;
      elem->dd0=q/c/alpha/alpha; 
    }
  }
  return(fl);
}



int ipe_Add(char *name, int flag,int idd) {
  int id=-1;
  double c1,l1, s1, q1, a1,d;
  if(id==-1) id=anzipe++;
  ipe[id].name=strdup(name);
  ipe[id].indx=idd;
  if(id==0)  ipe[id].z=S0;  /*Anfangsposition des Elements */
  else ipe[id].z=ipe[id-1].z+ipe[id-1].l;

  switch(ipd[idd].typ) {
  case V_START:
   ipe[id].z=*(ipd[idd].vpointer[1]);
  case V_LINE:
  case V_MARKER:
  case V_END:
    ipe[id].l=0;
    ipe[id].A=0;
    ipe[id].d=0;
    break;
  case V_SECTION:
    c1=*(ipd[idd].vpointer[5]);    //conductance
    a1=*(ipd[idd].vpointer[4]);    //inner surface
    d=*(ipd[idd].vpointer[3]);    //diameter
    s1=*(ipd[idd].vpointer[2]); /* pumping speed */
    q1=*(ipd[idd].vpointer[1]); /* specific outgasing */
    l1=*(ipd[idd].vpointer[0]); /* Length */
    
    if(c1==0.)             //if not explic., compute from diam.
      c1=0.123*pow(d*100.,3.); //spec conductance m l/s
    if (c1<1e-10) printf("WARNING: Conductance too small! W=%g at element %s\n",c1,name);
    //outgassing, a1 surface per m [cm^2/m]
    if (a1==0) a1=PI*d*1.e4; //in cm^2/m
    if (a1<1e-10) printf("WARNING: Surface unreasonably small! A=%g at element %s\n",a1,name);
    q1 = q1*a1;   

    ipe[id].W=c1;
    ipe[id].q=q1;
    ipe[id].sp=s1;
    ipe[id].d=d;
    ipe[id].A=a1;
    
    
    ipe[id].fl=trafo(&(ipe[id]),l1,ipe[id].W,ipe[id].q,sqrt(s1/c1));
    
  default:
    ipe[id].l=*(ipd[idd].vpointer[0]);
  }    
  return id;
}



/* Fügt eine (zusammengesetzte) Struktur zur beamline hinzu.
 */

  #define VAR(n) (*(ipd->list[(n)]))
void AddElem(int n_elem) {
  int id;
  int i; 
  
  int iflag=isign(n_elem); 
  n_elem=abs(n_elem); //if negative -> reverse line
  id=n_elem;
#if 0
  printf("Add Element: %s \n",ipd[id].name);
#endif


/* Ueberprüfe, ob das Element teil eines Links ist, und wenn ja, 
merke die Start- und End-Indizies für spaeter.*/

int startipe=anzipe;


  if(ipd[id].typ==V_START) {  //start element
    ipe_Add(ipd[id].name,1,id);//multiple elements poss.
  } else if(ipd[id].typ!=V_LINE) {
    ipe_Add(ipd[id].name,1,id);//multiple elements poss.
  } else { //subline
     // Uebernehme den Makronamen als Marker
     ipe_Add(ipd[id].name,1,id);//multiple elements poss.
    
     if (iflag==-1)  //reverse order
       for (i=ipd[id].nparms-1; i>=0; i--) 
          AddElem(-ipd[id].liste[i]); //recursive calls, reverse also sublines!
    else            //normal order
       for (i=0; i<ipd[id].nparms; i++) 
          AddElem(ipd[id].liste[i]); //recursive calls
  } 


  for(i=0;i<nlinks;i++) {
    if(links[i].l2==id) {
      if(verbose>1) printf("Link #%d Ausgang %d bei Anfang von Element %d (%s)\n",i,id,startipe,ipe[startipe].name);
      links[i].l2e=startipe;
    }
  }



  for(i=0;i<nlinks;i++) {
    if(links[i].l1==id) {
      if(verbose>1) printf("Link #%d Eingang %d bei Ende von Element %d (%s)\n",i,id,anzipe-1,ipe[anzipe-1].name);
      links[i].l1e=anzipe-1;
    }
  }

}

void Build_Vacuum() {
  int i;
 
// if (ipd.Na()==ipd.Nm()) {
//   cerr<<"BuildVacuum::No Startelement!"<<endl; exit(-1);}
// start_p = ipd->list;
  anzipe=0;    //empty line
  AddElem(use);      //add Beamline (expandieren)
  //search start-element
  for (i=0; i<anzipe; i++) {
    if (ipd[ipe[i].indx].typ==V_START) {
      S0 = *(ipd[ipe[i].indx].vpointer[1]); //start pos
      break;
    }
  }
  if (i>=anzipe) {
    printf("WARNING: No Startelement given! "); 
    printf("Setting Startposition to 0!\n"); 
    S0=0;
  }
  if(verbose) printf("INFO: Startposition set to %g m.\n",S0);
    
  CalcVacuum();       //calculate from list of elements
}






void List_Elements() {
  int i,j;
  printf("\n%d Elements:\n============\n",anzipe);
  for(i=0;i<anzipe;i++) {
    printf("%s \t#%d z=%gm :",ipe[i].name,ipe[i].indx,ipe[i].z);
    if(ipd[ipe[i].indx].typ==V_START) {
      printf(" START ");
      
      if(ipd[ipe[i].indx].nparms) {
        for(j=0;j<ipd[ipe[i].indx].nparms;j++) {
          printf("%g ",*(ipd[ipe[i].indx].vpointer[j]));
        }
      }
      printf(" [%d]\n",ipd[ipe[i].indx].nparms);
    } else if(ipd[ipe[i].indx].typ==V_LINE) {
      printf(" LINE ");
      if(ipd[ipe[i].indx].nparms) {
        for(j=0;j<ipd[ipe[i].indx].nparms;j++) {
          printf("%s ",ipd[ipd[ipe[i].indx].liste[j]].name);
    
        }
      }
      printf(" [%d]\n",ipd[ipe[i].indx].nparms);
    } else {
      if(ipd[ipe[i].indx].nparms) {
        for(j=0;j<ipd[ipe[i].indx].nparms;j++) {
          printf("%g ",*(ipd[ipe[i].indx].vpointer[j]));
        }
      }
      printf(" [%d] |",ipd[ipe[i].indx].nparms);
      printf(" fl=%d, l=%g, PQ=(%g,%g),"
             " Wq=(%g,%g), Ad=(%g,%g), sp=%g, Pavg=%g, Ai=%g, "
	     "m=(%g,%g,%g,%g), d=(%g,%g)\n",ipe[i].fl,ipe[i].l,ipe[i].P,ipe[i].Q,
	     ipe[i].W,ipe[i].q,ipe[i].A,ipe[i].d,ipe[i].sp,ipe[i].Pavg, 
	     ipe[i].Ai, ipe[i].mm00, ipe[i].mm01, ipe[i].mm10, ipe[i].mm11,
	     ipe[i].dd0, ipe[i].dd1);
    }
  }
}

void List_Symbols() {
  int i;
  printf("\n%d Symbols:\n===========\n",anzips);
  for(i=0;i<anzips;i++) {
    printf("%s\t:= %s\t= %g\t, $%02x\n",ips[i].name,ips[i].txt,ips[i].value,ips[i].flags);
  }
}



