
/* This file is part of VACLINE, pressure profile calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details
 */
#define FM_TYP       0x00f00
#define F_SIMPLE     0x00000  /* Befehl ohne Parameter */
#define F_ARGUMENT   0x00100  /* Befehl mit Parameter im P_CODE.argument */
#define F_PLISTE     0x00200
#define F_DQUICK     0x00400
#define F_IQUICK     0x00500
#define F_SQUICK     0x00600
#define F_AQUICK     0x00700
#define F_IGNORE     0x00800

#define F_INVALID    0x01000
#define F_IRET       0x02000
#define F_DRET       0x00000


#define PL_EVAL     0
#define PL_LEER     1
#define PL_OPTIONAL 1

#define PL_TYP     0x3c
#define PL_INT     4
#define PL_FLOAT   8
#define PL_NUMBER  (PL_INT|PL_FLOAT)


typedef struct {
  long opcode;
  char name[20];
  double (*routine)();
  signed char pmin;        /* Mindestanzahl an Parametern */
  signed char pmax;        /* Maximal moegliche Anzahl (-1) = beliebig */
  short pliste[12];  /* Liste der Kommandoparametertypen mit pmin Eintraegen */
} FUNCTION;



double parser(char *funktion);
