/* VACLINE.H (c) Mike Seidel, Markus Hoffmann  */

/* This file is part of VACLINE, pressure profile calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details
 */

#define VERSION "2.01"
#define DATE "2002-2018"
#define AUTHOR "Mike Seidel & Markus Hoffmann"

#define MAX_SYMBOLS 4096 //max symbols
#define MAX_ELEMENTDES 1024 //max element descriptors
#define MAX_ELEMENTS 4096 //max elements
#define MAX_LINKS 100 /* Max number of links */
#define MAX_BOUNDARY 100 /* Max number of extra boundaries */

//#define APREC   1e-25 //precision for FitA
//#define PPREC   1e-15 //precision for FitP
#define FITRULE  4   //rule to be carried out
#define FIT      2   //symbol to be varied for fit
#define MAX_IN_LINE 2048 //max elements in subline
#define NPAR    8 //s, P, Q, W, ql, sp, Pavg, Ai
#define NPLOT   3072


#define DropC( str) strcpy((str),((str)+1))
#define IncC( str, cc) memmove(((str)+1),(str),strlen(str)+1), *(str)=(cc)
#define EndS( str) (strchr((str),0))

typedef struct {
  int l1;       /* start element descriptor*/
  int l2;       /* other end element descriptor*/
  int l1e;      /* start element */
  int l2e;      /* other end element*/
  double flow;  /* flow through link */
  double pressure;  /* pressure in link */
} LINK;
#define B_P 1
#define B_Q 2

typedef struct {
  int typ;
  double val1;
  double val2;
} BOUNDARY;


/* Prototypes */

void Load_VacLine(char *);
void Eval_Symbols(int); 
void Build_Vacuum(); 
void FitP();

void write_pressure_data(char *);
void write_twiss(char *);
void List_Elements();
void List_Symbols();
void solve_lines();

void processline(char *line);

char *getname(char *buf, char *name);
int getpar( char *ss, char *s1, char *par);
int ips_set(char *name);
typedef struct {
  char *name;
  char *txt;
  int flags;
  double value;
} SYMBOL;
/*Line or element descriptor*/
typedef struct {
  int typ;
  char *name;
  char *tname;
  char *txt;
  int *liste;
  double *vpointer[100];
  int nparms;
  int boundary_typ;
  double bval1;
  double bval2;
} IPD;
typedef struct {
  int typ;
  char *name;
  int indx;   /* Index of descriptor */
  int fl;
  double z;  /*  longitudinal Position of Element [m]*/
  double l;  /*  Length of Element [m]*/
  double P;  /*  Pressure at end of Element [mbar]*/
  double Q;  /*  Gas flow at end of Element [mbar l/s]*/
  double W;  /*  Specific conductance*/
  double A;  /*  Inner surface */
  double q;  /*  Outgasing rate */
  double d;    /*  Diameter */
  double sp;   /*  Pumping Speed */
  double Pavg; /* Average Pressure */
  double Ai;
  double mm00;
  double mm01;
  double mm10;
  double mm11;
  double dd0;
  double dd1;
} ELEMENT;
extern SYMBOL ips[];
extern ELEMENT ipe[];

/* Element typen */

#define V_START 0
#define V_SECTION 1
#define V_MARKER 2
#define V_LINE 3
#define V_END 4
