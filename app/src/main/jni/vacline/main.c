/* MAIN.C 

   (c) Markus Hoffmann 2002-2011 
*/

/* This file is part of VACLINE, pressure profile calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details
 */


//baseline calculation for design vacuum system
#include <stdio.h>
#include <stdlib.h>
#ifdef WINDOWS
#define EX_OK 0
#define EX_IOERR -2
#define EX_NOINPUT -1
#else
#include <sysexits.h>
#endif
#include <string.h>

#include "vacline.h"
#include "utility.h"

#define DEBUG 0

int verbose=1;      /* Verbose level */
const char vacline_name[]="vacline";
const char version[]=VERSION;           /* Programmversion           */
int loadfile=FALSE;
char ifilename[256]="input.vac";
char ofilename[256]="output.dat";
char twissfilename[256]="output.twiss";
int dotwiss=0;
int dooutput=0;
double resolution=0.001; /* minimal resolution 1 mm */
int nplot=NPLOT;         /* maximal number of output points*/

void intro(){
  puts  ("*********************************************************");
  printf("*           %10s                V.%5s           *\n",vacline_name, version);
  puts  ("*             by Mike Seidel 2002                       *\n"
         "*             by Markus Hoffmann 2002-2011              *\n"
         "* Copyright (c) 2002-2011 --                              *\n"
	 "*                DESY Deutsches Elektronen Synchrotron, *\n" 
         "*   Ein Forschungszentrum der Helmholtz-Gemeinschaft    *\n"
         "*********************************************************\n"); 
}
 
void usage(){
  puts("\n Usage:\n ------ \n");
  printf(" %s [-h] [<filename>] --- process vacuum line [%s]\n\n",vacline_name,ifilename);
  puts("-v                  --- be more verbose\n"
       "-q                  --- be more quiet");
  printf("-r <number>           --- specify resolution [m]. default: %g m\n",resolution);
  printf("--resolution <number> --- specify resolution [m]. default: %g m\n",resolution);
  printf("-n <number>           --- specify maximum number of output points. default: %d\n",nplot);
  printf("--nplot <number>      --- specify maximum number of output points. default: %d\n",nplot);
  
  printf("-o <filename>       --- place output into file       [%s]\n",ofilename);
  printf("--twiss <filename>  --- place twiss output into file [%s]\n",twissfilename);
  puts("-h --help           --- Usage");
}


void kommandozeile(int anzahl, char *argumente[]) {
  int count,quitflag=0;


  /* Kommandozeile bearbeiten   */
  for(count=1;count<anzahl;count++) {
    if (strcmp(argumente[count],"-h")==FALSE || 
        strcmp(argumente[count],"--help")==FALSE) {
      intro();
      usage();
      quitflag=1;   
    } 
    else if (strcmp(argumente[count],"-r")==FALSE || 
        strcmp(argumente[count],"--resolution")==FALSE) {
	count++;
	resolution=atof(argumente[count]);
    }
    else if (strcmp(argumente[count],"-n")==FALSE || 
        strcmp(argumente[count],"--nplot")==FALSE) {
	count++;
	nplot=atof(argumente[count]);
    }
     else if (strcmp(argumente[count],"-v")==FALSE) verbose++;
     else if (strcmp(argumente[count],"-q")==FALSE) verbose--;
     else if (strcmp(argumente[count],"-o")==FALSE) {
       count++;
       dooutput=1;
       strcpy(ofilename,argumente[count]);
     } else if (strcmp(argumente[count],"--version")==FALSE) {
       intro();
       puts("This program is scientific software. The used methods and formulas are\n" 
            "published in DESY-HERA-03-23: Markus Hoffmann, \n'Vakuum-Simulationsrechnung " 
            "fuer HERA'.\n\n"
	    "This program is open source and licensed under the GPLv2."
	    "This program is distributed in the hope that it will be useful, "
	    "but WITHOUT ANY WARRANTY; without even the implied warranty of "
    	    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.");
     } else if (strcmp(argumente[count],"--twiss")==FALSE) {
       count++;
       dotwiss=1;
       strcpy(twissfilename,argumente[count]);
     } else {
      if(!loadfile) {
        loadfile=TRUE;
        strcpy(ifilename,argumente[count]); 
      }
    }
   }
   if(quitflag) exit(EX_OK);
}



int main(int anzahl, char *argumente[]) {
  char dummy[256];
  if(anzahl<2) {    /* Kommandomodus */
    intro();
  } else {
    kommandozeile(anzahl, argumente);    /* Kommandozeile bearbeiten */
    if(loadfile) {
      if(exist(ifilename)) {
        if(verbose) printf("PASS 0\n");
        Load_VacLine(ifilename);
        if(verbose) printf("PASS 1\n");
        Eval_Symbols(0);
	solve_lines();
        if(verbose) printf("PASS 2\n");
	Build_Vacuum(); //v1.FitP();
        if(!dooutput) {
	  wort_sepr(ifilename,'.',0,ofilename,dummy);
          strcat(ofilename,".dat");
        }
        write_pressure_data(ofilename);
	if(dotwiss) write_twiss(twissfilename); 
        
	if(verbose>1) {
	  List_Elements();
          if(verbose>2) List_Symbols();
	}
      } else {
        printf("ERROR: file %s not found !\n",ifilename);
	return(EX_NOINPUT);
      }
    }
  } 
  return(EX_OK);
}
