Guide to contributing to CALCVAC for Android
=============================================

CALCVAC and VACLINE have been rudimentary command-line programs for 
Linux for a long time. Only Vacuum specialists could use them. 


The port to Android (with quite a lot of improvements) now makes it more 
easy to construct and design a vacuum beamline e.g. for accelerators.

Al though the algorithm is still the same as on the ancient versions from 
2002/2003 the user interface has much changed. Because an App for this 
purpose has not been invented before, the user interface is new as well.

The authors have implemented function which seem useful to them in 
the all day business. However, other users might feel differently on 
their needs. 

Suggestions for improvements are therefor very welcome. Even the exchange 
of experience of usage could be very valuable.

The new version of CALCVAC/VACLINE is still file compatible (.inf and .vac. 
files, as well as the resulting .dat files with the calculated data). 
I would like to keep this as a feature, however, this file based approach might
cause some trouble interfacing with the tap-and-swish user interface of a 
modern Smart phone or tablet.

Not everything runs smoothly yet.

You can:
* report bugs and unexpected behavior. --> open issue
* donate/share your vacuum beamline files for testing.
* discuss new features  --> open issue
* improve the user manual and online-help.
* suggest nice icons and graphics or even create them.
* translate the app to other languages.
* If you can: port it to apple IOS/iphone.


## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
license GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 


## Contributing with a Pull Request

The best way to contribute to this project is by making a pull-request:

1. Login with your Gitlab account or create one now
2. [Fork](https://gitlab.com/kollo/Calcvac-Android.git) the Calcvac for Android repository. Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the `contrib` directory if you're not sure where your contribution might fit.
5. Edit `doc/ACKNOWLEGEMENTS` and add your own name to the list of contributors under the section with the current year. Use your name, or a gitlab ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a pull-request against the Calcvac repository.



## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a pull-request, then you can file an Issue. Filing an Issue will help us
see the problem and fix it.

Create a [new Issue](https://gitlab.com/kollo/Calcvac-Android/issues/new?issue) now!



## Thanks

We are very grateful for your support. With your help, this implementation
will be a great project. 

