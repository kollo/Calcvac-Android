CALCVAC/VACLINE for Android
===========================

Calculate logitudinal one-dimensional pressure profiles in vacuum pipes.


Copyright (c) 2002-2021 -- DESY Deutsches Elektronen Synchrotron, 
                 Ein Forschungszentrum der Helmholtz-Gemeinschaft

Authors:
     Markus Hoffmann (2002-2021),
     Mike Seidel -2002

<img alt="Logo" src="fastlane/metadata/android/en-US/images/icon.png" width="120" />

Description:
------------

The programs calcvac/vacline can calculate longitudinal one-dimensional pressure
profiles in vacuum pipes.  The pipes can consist of different sections of
different shape and material.  Outgasing of several different gas species can be
simulated. Also it is possible to install vacuum pumps  of different types. 
Also cryo effects (cold pipes and cryogenic pumps) are simulated. Finally the pipes 
can be linked together to form a network.

This program is scientific software.  The  used  methods  and  formulas  are 
published  in  [DESY-HERA-03-23](http://wof-cluster.desy.de/sites2009/site_msk/content/localfsExplorer_read?currentPath=/afs/desy.de/group/msk/www/html/Publikationen/Reports/DESY-HERA-03-23.pdf): Markus Hoffmann, "Vakuum-Simulationsrechnung
für HERA" (german).

### Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/packages/de.drhoffmannsoftware.calcvac)


### Screenshots

<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/5.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/6.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/7.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/8.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/9.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/a.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/b.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/c.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/d.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/e.png" width="30%">
</div>

### Important Note:

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; Version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


Acknowledgements
================

Thanks to all people, who helped me to realize this package.

VACLINE uses functionality of the LAPACK library. 
LAPACK (Linear Algebra Package) is a standard software library for 
numerical linear algebra. 

For details, see: http://www.netlib.org/lapack/


Further notes
=============

The sources for vacline, calcvac (as used on the command-line on Linux and
WINDOWS operating systems) and the CALCVAC/VACLINE user manual are 
maintained in the [CALCVAC main repository](https://codeberg.org/kollo/calcvac), 
called "calcvac" and hosted on codeberg.org. 
They are just cloned into this repository.

There do also exist (rather old) versions of CALCVAC/VACLINE for Linux and 
WINDOWS.


Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.


